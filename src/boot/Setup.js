import React, { Component } from "react";
import { StyleProvider, Root, StatusBar } from "native-base";

import App from "../App";
import getTheme from "../theme/components";
import variables from "../theme/variables/commonColor";
import { connect } from "react-redux";
import { getNotificationList } from "../actions";




class Setup extends Component {
    constructor(props){
        super(props)
        this.props.getNotificationList(true)
    }
   
    render() {
        return (
            <StyleProvider style={getTheme(variables)}>
                <Root>
                    <App />
                </Root>
            </StyleProvider>
        );
    }
}

const mapStateToProps = (state) => ({
  
})

const mapDispatchToProps = {
  getNotificationList
}

export default connect(mapStateToProps, mapDispatchToProps)(Setup)
