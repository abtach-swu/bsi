export const VALIDEMAIL='Please provide valid email'
export const VALIDPASSWORD ='Please provide password'
export const VALIDCONFIRMPASSWORD='Confirm Password do not match'


//Profile
export const VALIDATION_EMPTY_NAME ='Please provide name'

//Change Password
export const VALIDATION_OLD_PWD ='Please provide old password'
export const VALIDATION_NEW_PWD ='Please provide new password'
export const VALIDATION_CONFIRM_PWD ='Please provide confirm password'
export const VALIDATION_CONFIRM_PWD_NOT_MATCH ='Confirm password does not match'

