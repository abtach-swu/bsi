import axios from "axios";
import { getUserToken } from '../UserPreference';


class HttpServiceManager {

  static myInstance = null;
  static axiosInstance = null;

  static getInstance() {
    if (HttpServiceManager.myInstance == null) {
      HttpServiceManager.myInstance = new HttpServiceManager();
    }
    return this.myInstance;
  }

  static initialize = (baseURL, authHeader) => {

    HttpServiceManager.getInstance().axiosInstance = axios.create({
      baseURL: baseURL,
      timeout: 60000,
      headers: authHeader
    });
    // HttpServiceManager.getInstance().axiosInstance.defaults.headers.common['Content-Type'] = 'multipart/form-data';

    HttpServiceManager.getInstance().axiosInstance.interceptors.request.use(
      function (config) {
        return new Promise((resolve, reject) => {
          getUserToken().then((value) => {
            if (value !== undefined && value !== null) {
              // config.data = {}
              config.headers["user-token"] = value
              resolve(config);
            } else {
              config.headers["user-token"] = ''
              resolve(config);
            }
          })
        });
      },
      function (error) {
        return Promise.reject(error);
      }
    );
  }

  request = (requestName, parameters, method) => {
    //showLoader(true);
    const data = method === "GET" ? "" : parameters;
    if (HttpServiceManager.getInstance().axiosInstance !== null) {
      return new Promise((resolve, reject) => {
        let reqParam = {
          method: method,
          url: requestName,
          data: data,
          params: method === "POST" ? "" : parameters
        }
        console.log("reqParam", reqParam)
        HttpServiceManager.getInstance().axiosInstance.request(reqParam)
          .then((response) => {
            console.log("response", response.data)
            if (response.data.code === 200 || response.data.code === 204) {
              resolve(response.data);
            } else {
              reject('Unknown error');
            }
          }).catch((error) => {
            reject(HttpServiceManager.checkError(error));
          });
      })
    } else {
      console.warn('HttpServiceManager method "initialize" is not called, call it in App.js componentDidMount');
    }
  }


  //Image request

  requestWithImage = (requestName, parameters, method) => {
    //showLoader(true);
    if (HttpServiceManager.getInstance().axiosInstance !== null) {
      return new Promise((resolve, reject) => {
        let reqParam = {
          method: method,
          url: requestName,
          data: parameters,
          params: ""
        }
        console.log("reqParam", reqParam)
        HttpServiceManager.getInstance().axiosInstance.request(reqParam)
          .then((response) => {
            console.log("response", response.data)
            if (response.data.code === 200 || response.data.code === 204) {
              resolve(response.data);
            } else {
              reject('Unknown error');
            }
          }).catch((error) => {
            reject(HttpServiceManager.checkError(error));
          });
      })
    } else {
      console.warn('HttpServiceManager method "initialize" is not called, call it in App.js componentDidMount');
    }
  }


  static checkError = (error) => {
    if (error.response === undefined) {
      return error.message
    } else if (error.response.status === 500) {
      return "Html cannot be parsed"
    } else if (error.response.status === 503) {
      return error.message
    } else if (error.response.status === 401) {
      return error.message

    } else if (error.response.status === 403) {
      return error.message
    } else if (error.response.status === 404) {
      //console.log(error.response.data.data)

      const values = Object.keys(error.response.data.data).map(key => {
        return error.response.data.data[key];
      })
      return (values);



      //var values = Object.keys(error.response.data.data[0]).map((key) => {
      //  return error.response.data.data[0][key];
      //});
      // console.log('\n• ' + values.join('{"\n•"}'))
      //return ('• ' + values.join('\n• '));
    } else {
      return error.message
    }
  }
}
export default HttpServiceManager;
