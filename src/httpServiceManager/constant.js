const constant = {
    baseUrl: "http://360cubes.com/bsi_staging/public/api",
    imageBaseUrl: "http://360cubes.com/bsi_staging/public/",
    // baseUrl : "http://bsiapp.com/public/api",
    // imageBaseUrl : "http://bsiapp.com/public",
    // baseUrl: "http://192.34.60.217/bsi-web/public/api",
    // imageBaseUrl: "http://192.34.60.217/bsi-web/public/",
    login: 'user/login',
    forgotPassword: 'user/forgot/password',
    userDetail: "user/detail",
    mytask: 'task/list',
    userList: 'user/list',
    inventoryList: 'inventory/list',
    plantList: 'plant/list',
    unitList: 'unit/list',
    floorList: 'floor/list',
    portList: 'port/list',

    taskAdd: 'task/add',
    taskDetail: 'task/detail/',
    userLogout: 'user/logout',

    updateProfile: 'user/update',
    changePassword: 'user/change/password',

    report_request: 'task/get-report',
    taskUpdate: 'task/update/',

    notificationList: 'notification/list',
    notificationUpdate: 'notification/update',
    faq: 'faq',

    task_mark_as_complete: 'task/mark-as-complete',

    //Messages
    message: 'message'

}

export default constant