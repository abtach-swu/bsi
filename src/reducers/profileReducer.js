import {
    IS_LOADING,
    PROFILE_SUCCESS,
    PROFILE_ERROR,
    NAME_CHANGED_PROFILE,
    IMAGE_CHANGED_PROFILE,
    CLEAR_ERROR_REDUX,
    CHANGE_PASSWORD_ERROR,
    CHANGE_PASSWORD_SUCCESS,
} from "../actions/types";



const initialState = {
    error: '',
    user: null,
    loading: false,
    message: '',
    name:'',
    image:''
}

export default (state = initialState, action) => {
    switch (action.type) {
        case IS_LOADING:
            return { ...state, loading: true, error: '', user: null, message: '' }
        case PROFILE_SUCCESS:
            return { ...state, user: action.payload.data, message: action.payload.message, error: '', loading: false }
        case PROFILE_ERROR:
            return { ...state, error: action.payload, message: '',user: null, loading: false }

        case NAME_CHANGED_PROFILE:
            return { ...state, error: '', name: action.payload,user: null, message: '', loading: false }
        case IMAGE_CHANGED_PROFILE:
            return { ...state, error: '', image: action.payload,user: null, message: '', loading: false }
        


        case CHANGE_PASSWORD_SUCCESS:
            return { ...state, message: action.payload, error: '', loading: false }
        case CHANGE_PASSWORD_ERROR:    
            return { ...state, error: action.payload, message: '', loading: false }

        case CLEAR_ERROR_REDUX:
            return {...state,error:'',message:'',loading:false}
        default:
            return state
    }
}