import { combineReducers } from 'redux';
import authReducer from './authReducer';
import addTaskReducer from './addTaskReducer';
import mytaskReducer from './mytaskReducer';
import InventoryReducer from './InventoryReducer';
import profileReducer from './profileReducer';
import MessageReducer from './MessageReducer';
import NotificationReducer from './NotificationReducer';




export default combineReducers({
    authReducer ,
    addTaskReducer ,
    mytaskReducer,
    InventoryReducer,
    profileReducer,
    MessageReducer,
    NotificationReducer

});