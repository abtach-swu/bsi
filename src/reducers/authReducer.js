import {
    EMAIL_CHANGED_LOGIN,
    PASSWORD_CHANGED_LOGIN,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAIL,
    IS_LOGIN_LOADING,
    RESET_PASSWORD_SUCCESS,
    RESET_PASSWORD_FAIL

} from '../actions/types';

const initialState = {
    email: '',
    password: '',
    user: null,
    error: '',
    loading: false,
    userToken: '',
    message: '',
    isConnected : true
}

export default (state = initialState, action) => {
    switch (action.type) {
        case EMAIL_CHANGED_LOGIN:
            return { ...state, email: action.payload, error: '', user: null , message : ''}
        case PASSWORD_CHANGED_LOGIN:
            return { ...state, password: action.payload, error: '', user: null, message : '' }
        case LOGIN_USER_SUCCESS:
            return { ...state, ...initialState, user: action.payload, loading: false ,email:'',password:'', message : ''}
        case LOGIN_USER_FAIL:
            return { ...state, error: action.payload, loading: false, password: '' , message : '' }
        case IS_LOGIN_LOADING:
            return { ...state, loading: true, error: '',  message : '' }
        case RESET_PASSWORD_SUCCESS:
            return { ...state, ...initialState, message: action.payload, loading: false }
        case RESET_PASSWORD_FAIL:
            return { ...state, error: action.payload, loading: false, password: '', message : '' }
        case "NETWORK_INFO":
            return { ...state, isConnected: action.payload , error : '', loading: false , message : ''}
        case "ON_LOGOUT":
            return { ...state, loading : true}
        case "ON_USER_LOGOUT_SUCCESS":
            return { ...state, ...initialState, message : "LOGOUT"}
        case "ON_USER_LOGOUT_FAIL":
            return { ...state,error: action.payload , message : "" , loading  : false}
        default:
            return state
    }
}