import { EMAIL_CHANGED_LOGIN } from "../actions/types";

const initialState = {
  message: "",
  error: "",
  notificationList: [],
  user: null,
  loading: false,
  detail : null,
  faqs : [],
  notificationCount : []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case "NOTIFICATION":
      return { ...state,
        loading : true ,  
        detail : null,
        error : '',
        message : ''
      };
    case "NOTIFICATION_SUCCESS":
      return {
        ...state,
       loading : false,
       notificationList : action.payload,
       notificationCount : action.flag ? action.payload.filter(item => item.is_read === 0) : [] ,
       detail : null
      };
    case "NOTIFICATION_FAIL":
      return {
        ...state,
        loading : false,
        error : action.payload,
        detail : null
      };
      case "TASK_DETAIL":
      return { 
        ...state, 
        loading : true,  
        detail : null
      };
      case 'GET_TASK_DETAIL_SUCCESS':
      return {
        ...state , 
        detail : action.payload,
        loading : false,
        error  :'',
        message : ''
      }

      case 'GET_TASK_DETAIL_FAIL':
      return {
        ...state , 
        error : action.payload,
        message : '',
        loading : false,
        detail : null
      }
      case 'NOTIFICATION_READ_SECCESS':
      return {
        ...state,
        message: action.payload,
        error : ""
      };
    case 'NOTIFICATION_READ_FAIL':
      return {
        ...state,
        error: action.payload,
        message : ""
      };
    // 
      case "FAQ":
      return { 
        ...state, 
        loading : true,  
      };
      case 'FAQS_GET_SECCESS':
      return {
        ...state , 
        faqs : action.payload.map(item => { return { ...item , isDisplay : false}}),
        loading : false,
        error  :'',
        message : ''
      }

      case 'FAQS_GET_FAIL':
      return {
        ...state , 
        error : action.payload,
        faqs : [],
        message : '',
        loading : false,
        detail : null
      }

    case "ON_USER_LOGOUT_SUCCESS":
      return { 
        ...state, 
        ...initialState 
    };
    case "CLEAR_REDUX_STATE":
      return { 
        ...state, 
        detail : null,
        loading : false,
    };
    default:
      return state;
  }
};
