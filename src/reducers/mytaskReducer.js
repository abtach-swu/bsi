
import {
    IS_LOADING,
    IS_REFRESHING,
    MY_TASK_FAIL,
    MY_TASK_SUCCESS,
    MY_TASK_LOAD_MORE
} from "../actions/types";


const initialState = {
    error: '',
    responseData: [],
    pandingTask : [],
    today : [],
    upcoming : [],
    completed : [],
    taskToEdit : {},
    loading: false,
    refreshing: false,
    message: '',
    currentPage: null,
    nextPage: null,
    detail : null
}

export default (state = initialState, action) => {
    switch (action.type) {
      case IS_REFRESHING:
        return { ...state, refreshing: true, loading: false, error: "", responseData: [] , detail : null};
      case MY_TASK_LOAD_MORE:
        return {
          ...state,
          responseData: [...state.responseData, ...action.payload],
          currentPage: action.payload.meta.current_page,
          nextPage: action.payload.meta.last_page,
          message: "",
          error: "",
          refreshing: false,
          loading: false,
          detail : null
        };

      case 'TASK_EDIT':
      return {
        ...state,
        taskToEdit : action.payload
      }

      case MY_TASK_SUCCESS:
        return {
          ...state,
          responseData: action.payload,
          today : ( action.filter_type === 'today')? action.payload : state.today,
          upcoming : ( action.filter_type === 'upcoming')? action.payload : state.upcoming,
          completed : ( action.status === 'completed')? action.payload : state.completed,
          message: "",
          error: "",
          refreshing: false,
          loading: false,
          detail : null
        };

      case MY_TASK_FAIL:
        return {
          ...state,
          // error: action.payload,
          error: '',
          message: "",
          refreshing: false,
          loading: false,
          detail : null
        };
      
      case 'OFFLINE_MARK_TASK_COMPLETE':
      return {
        ...state ,
        pandingTask : [...state.pandingTask, action.payload],
        today : state.today.filter(item => item.id !== action.payload.task_id),
        upcoming : state.upcoming.filter(item => item.id !== action.payload.task_id),
        // completed : state.today.filter(item => item.id === action.payload.task_id),
        // completed : state.upcoming.filter(item => item.id === action.payload.task_id),
        message  : "Will update when online",
        error : "",
        detail : null
      }
      case 'GET_TASK_DETAIL_SUCCESS':
      return {
        ...state , 
        detail : action.payload,
        error  :'',
        message : ''
      }
      case 'ON_MARK_TASK_COMPLETE_SUCCESS':
      return {
        ...state,
        detail : null,
        error: "",
      };

      case 'GET_TASK_DETAIL_FAIL':
      return {
        ...state , 
        error : action.payload,
        message : '',
        detail : null
      }

      case 'CLEAR_PANDING_TASK':
      return {
        ...state ,
        pandingTask : [],
        detail : null,
        message  : "",
        error : ""
      }
      case "CLEAR_REDUX_STATE":
      return { 
        ...state, 
         detail : null
    };
      case "ON_USER_LOGOUT_SUCCESS":
      return { ...state, ...initialState}
      
      default:
        return state;
    }
}