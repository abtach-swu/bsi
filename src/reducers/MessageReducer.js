import {
  USER_LIST_SUCCESS,
  USER_LIST_FAIL,
  MESSEGE_RETRIEVED_SUCCESS,
  MESSEGE_RETRIEVED_FAIL,
  MESSAGE_SEND_SECCESS,
  MESSAGE_SEND_FAIL,
  MESSAGE_READ_SECCESS,
  MESSAGE_READ_FAIL,
} from "../actions/types";

const initial_state = {
  userList: [],
  messageData : [],
  message: "",
  error: "",
  loading : false
};

export default (state = initial_state, action) => {
  switch (action.type) {
    case USER_LIST_SUCCESS:
      return {
        ...state,
        userList: action.payload,
        message : "",
        error : ""
      };
    case USER_LIST_FAIL:
      return {
        ...state,
        error: action.payload,
        message : "",
      };
    case MESSEGE_RETRIEVED_SUCCESS:
      return {
        ...state,
        messageData: action.payload,
        message : "",
        error : ""
      };
    case MESSEGE_RETRIEVED_FAIL:
      return {
        ...state,
        error: action.payload,
        message : "",
      };
    case 'MESSAGE_SEND':
      return {
        ...state,
        loading : true
      };
    case MESSAGE_SEND_SECCESS:
      return {
        ...state,
        message: action.payload,
        error : "",
        loading : false
      };
    case MESSAGE_SEND_FAIL:
      return {
        ...state,
        error: action.payload,
        error : "",
        loading : false
      };
    case MESSAGE_READ_SECCESS:
      return {
        ...state,
        message: action.payload,
        error : ""
      };
    case MESSAGE_READ_FAIL:
      return {
        ...state,
        error: action.payload,
        message : ""
      };
    case 'CLEAR_REDUX_MESSAGE_STATE':
      return {
        ...state,
        ...initial_state
      };
    default:
      return state;
  }
};
