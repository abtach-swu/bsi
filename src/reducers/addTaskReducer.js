import {
  IS_LOADING,
  MY_USER_LIST_SUCCESS,
  MY_USER_LIST_FAIL,
  INVENTORY_LIST_SUCCESS,
  INVENTORY_LIST_FAIL,
  PLANT_LIST_SUCCESS,
  PLANT_LIST_FAIL,
  UNIT_LIST_SUCCESS,
  UNIT_LIST_FAIL,
  FLOOR_LIST_SUCCESS,
  FLOOR_LIST_FAIL,
  PORT_LIST_SUCCESS,
  PORT_LIST_FAIL,
  ADD_TASK_SUCCESS,
  ADD_TASK_FAIL,
  INVENTORY_CHANGE_NAME,
  INVENTORY_CHANGE_QUANTITY,
  INVENTORY_CHANGE_INCREMENT,
  ON_SELECT_TARGET,
  ON_SELECT_PLANT,
  ON_SELECT_UNIT,
  ON_SELECT_FLOOR,
  ON_SELECT_PORT,
  SELECTED_INVENTORY_ARRAY,
  ON_CHANGE_DATE,
  ON_CHANGE_TIME,
  ON_SAVE_PORT
} from "../actions/types";

import moment from "moment";

const initialState = {
  crewResponseData: [],
  inventoryResponseData: [],
  selectedInventory: [],
  plantResponseData: [],
  unitResponseData: [],
  floorResponseData: [],
  portResponseData: [],
  portSelectionDone: [],
  newFloorSelection: [],
  isModalVisible: false,
  editable: false,
  loading: false,
  task_id: "",
  description: "",
  job_number: "",
  message: "",
  error: "",

  selectedTarget: "Select Person",
  selectedPlant: "Plant",
  selectedUnit: "Unit",
  selectedFloor: "Floor",
  selectedPort: "Port",
  date:  moment(new Date()).format("DD-MM-YYYY"),
  time: new Date().toLocaleTimeString()
};

export default (state = initialState, action) => {
  switch (action.type) {
    case IS_LOADING:
      return {
        ...state,
        loading: true,
        error: "",
        message: "",
      };
    case MY_USER_LIST_SUCCESS:
      return {
        ...state,
        error: "",
        crewResponseData: action.payload,
        message: "",
        loading: false
      };
    case MY_USER_LIST_FAIL:
      return { ...state, error: action.payload, message: "", loading: false };

    case "TASK_EDIT":
      return {
        ...state,
        editable: true,
        loading: false,
        selectedTarget: action.payload.target_id,
        selectedInventory: action.payload.inventory,
        portSelectionDone: action.payload.port,
        selectedPlant: action.payload.location.plant_id,
        selectedUnit: action.payload.location.unit_id,
        date: moment(new Date(action.payload.schedule_date)).format("DD-MM-YYYY"),
        time: action.payload.schedule_time,
        job_number: action.payload.job_number,
        description: action.payload.description,
        task_id: action.payload.id,
        error: "",
        message: ""
      };

    case ON_SELECT_TARGET:
      return {
        ...state,
        error: "",
        selectedTarget: action.payload,
        message: "",
        loading: false
      };
    case ON_SELECT_PLANT:
      return {
        ...state,
        error: "",
        selectedPlant: action.payload,
        unitResponseData: (action.payload == '-1') ? [] : state.unitResponseData,
        message: "",
        loading: false
      };
    case ON_SELECT_UNIT:
      return {
        ...state,
        error: "",
        selectedUnit: action.payload,
        floorResponseData: (action.payload == '-1') ? [] : state.floorResponseData,
        portSelectionDone: [],
        message: "",
        loading: false
      };
    case ON_SELECT_FLOOR:
      return {
        ...state,
        error: "",
        selectedFloor: action.payload,
        message: "",
        loading: false
      };

    case "ON_FLOOR_LIST_COMP":
      return {
        ...state,
        error: "",
        newFloorSelection: state.portSelectionDone,
        message: "",
        loading: false
      };

    case "ON_FLOOR_DISSELECT":
      return {
        ...state,
        error: "",
        newFloorSelection: state.newFloorSelection.filter(item => item.floor_id !== action.payload),
        // disselectedFloor : (state.disselectedFloor.length !== 0) ? [...state.disselectedFloor.filter(item => item.floor_id !== action.payload), state.newFloorSelection.find(item => item.floor_id === action.payload) ]  : state.newFloorSelection.filter(item => item.floor_id === action.payload),
        message: "",
        loading: false
      };

    case "ON_FLOOR_SELECTION_DONE":
      return {
        ...state,
        error: "",
        floorResponseData: action.payload,
        portSelectionDone: state.newFloorSelection,
        message: "",
        loading: false
      };

    case ON_SELECT_PORT:
      return {
        ...state,
        error: "",
        selectedPort: action.payload,
        message: "",
        loading: false,
        isModalVisible: false
      };
    case ON_CHANGE_DATE:
      return {
        ...state,
        error: "",
        date: action.payload,
        time : moment(action.payload).isSame(new Date().toLocaleDateString(), 'day') ? new Date().toLocaleTimeString() : state.time,
        message: "",
        loading: false
      };
    case ON_CHANGE_TIME:
      return {
        ...state,
        error: "",
        time: action.payload,
        message: "",
        loading: false
      };
    case "MODALVISIBLE":
      return { ...state, isModalVisible: !state.isModalVisible };
    case INVENTORY_LIST_SUCCESS:
        state.selectedInventory.forEach(element =>  
          action.payload = action.payload.map(item => element.inventory_id === item.id ?{...item, inventory_name : element.name , inventory_quantity : element.total_qty, is_selected: true , inventory_used_qty : element.used_qty} : item)
        )
      return {
        ...state,
        error: "",
        inventoryResponseData : action.payload,
        message: "",
        loading: false
      };
    case INVENTORY_LIST_FAIL:
      return { ...state, error: action.payload, message: "", loading: false };
    case SELECTED_INVENTORY_ARRAY:
      return {
        ...state,
        selectedInventory: action.payload,
        inventoryResponseData: action.inventoryList,
        message: "",
        loading: false
      };

    case INVENTORY_CHANGE_NAME:
      return {
        ...state,
        inventoryResponseData: state.inventoryResponseData.map(item =>
            item.id === action.id
              ? { ...item, inventory_name: action.payload }
              : item
          ),
        error: "",
        message: ""
      };

    case INVENTORY_CHANGE_QUANTITY:
      return {
        ...state,
        inventoryResponseData: state.inventoryResponseData.map(item =>
            item.id === action.id
              ? {
                ...item,
                inventory_quantity:
                  isNaN(parseInt(action.payload)) === true
                    ? 0
                    : parseInt(action.payload)
              }
              : item
          ),
        error: "",
        message: ""
      };
    // return {
    //   ...state,
    //   selectedInventory: (state.editable)? state.selectedInventory.map(item =>
    //     item.id === action.id
    //       ? {
    //           ...item,
    //           total_qty:
    //             isNaN(parseInt(action.payload)) === true
    //               ? 0
    //               : parseInt(action.payload)
    //         }
    //       : item
    //   ) 
    //   : 
    //   state.selectedInventory.map(item =>
    //     item.id === action.id
    //       ? {
    //           ...item,
    //           inventory_quantity:
    //             isNaN(parseInt(action.payload)) === true
    //               ? 0
    //               : parseInt(action.payload)
    //         }
    //       : item
    //   ),
    //   error: "",
    //   message: ""
    // };
    case INVENTORY_CHANGE_INCREMENT:
      return {
        ...state,
        inventoryResponseData: state.inventoryResponseData.map(item =>
            item.id === action.id
              ? {
                ...item,
                inventory_quantity: parseInt(
                  item.inventory_quantity + action.payload
                )
              }
              : item
          ),
        error: "",
        message: ""
      };
    case PLANT_LIST_SUCCESS:
      return {
        ...state,
        error: "",
        plantResponseData: action.payload,
        unitResponseData: [],
        message: "",
        loading: false
      };
    case PLANT_LIST_FAIL:
      return { ...state, error: action.payload, message: "", loading: false };

    case UNIT_LIST_SUCCESS:
      return {
        ...state,
        error: "",
        unitResponseData: action.payload,
        floorResponseData: [],
        message: "",
        loading: false
      };
    case UNIT_LIST_FAIL:
      return { ...state, error: action.payload, message: "", loading: false };

    case FLOOR_LIST_SUCCESS:
      return {
        ...state,
        error: "",
        floorResponseData: action.payload,
        portResponseData: [],
        message: "",
        loading: false
      };
    case FLOOR_LIST_FAIL:
      return { ...state, error: action.payload, message: "", loading: false };

    case ON_SAVE_PORT:
      return {
        ...state,
        newFloorSelection: [...state.newFloorSelection, action.payload],
        // portSelectionDone: (state.portSelectionDone.length !== 0) ? [...state.portSelectionDone.filter( item => item.floor_id !== action.payload.floor_id) , action.payload]  : [...state.portSelectionDone, action.payload]
      };

    case PORT_LIST_SUCCESS:
      return {
        ...state,
        error: "",
        portResponseData: action.payload,
        message: "",
        loading: false
      };
    case PORT_LIST_FAIL:
      return { ...state, error: action.payload, message: "" };

    case ADD_TASK_SUCCESS:
      return {
        ...state,
        ...initialState,
        message: action.payload
      };
    case "NEW_TASK_ADD":
      return {
        ...state,
        ...initialState,
      };
    case ADD_TASK_FAIL:
      return { ...state, error: action.payload, message: "", loading: false };
    default:
      return state;
  }
};
