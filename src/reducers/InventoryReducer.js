import {
  ON_MARK_TASK_COMPLETE_SUCCESS,
  ON_MARK_TASK_COMPLETE_FAIL,
  ON_SAVED_INVENTORY,
  SELECTED_PORT_INVENTORY_SAVED,
  ON_SAVED_INVENTORY_PORT_REPORT,
  INVENTORY_CHANGE_USED_INCREMENT
} from "../actions/types";

const initialState = {

  task_port: {},
  task_detail: {},
  task_location: {},
  port_data: [],
  task_inventory: [],
  selected_ports: [],
  slctedPortInventory: [],
  error: "",
  message: "",
  floor_id: "",
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SELECTED_PORT_INVENTORY_SAVED:
      return {
        ...state,
        task_location: action.location,
        task_detail: action.payload,
        task_inventory: action.payload.inventory,
        task_port: action.task_port,
        selected_ports: (state.selected_ports.length !== 0) ? state.selected_ports.find(item => item.floor_id === action.floor_id) !== undefined ? state.selected_ports : [...state.selected_ports, action.selectedPortData] : [...state.selected_ports, action.selectedPortData],
        floor_id: action.floor_id
      };
    case ON_MARK_TASK_COMPLETE_SUCCESS:
      return {
        ...state,
        error: "",
        message: action.payload
      };
    case ON_MARK_TASK_COMPLETE_FAIL:
      return {
        ...state,
        error: action.payload,
        message: ""
      };
    case ON_SAVED_INVENTORY:
      return {
        ...state,
        selected_ports: state.selected_ports.map(item => item.floor_id === action.floor_id ? { ...item, data: item.data.map(itm => itm.port_id === action.port_id ? { ...itm, inventory: action.payload, is_filled: 1 } : itm) } : item),
        error: "",
        message: ""
      };
    case ON_SAVED_INVENTORY_PORT_REPORT:
      return {
        ...state,
        port_data: (state.port_data.length !== 0) ? state.port_data.find(item => item.floor_id === action.payload.floor_id) !== undefined ? state.port_data.map(item => item.floor_id === action.payload.floor_id ? action.payload : item) : [...state.port_data, action.payload] : [...state.port_data, action.payload]
      };
    case INVENTORY_CHANGE_USED_INCREMENT:
      return {
        ...state,
        selectedInventory: state.selectedInventory.map(item =>
          item.id === action.id
            ? {
              ...item,
              inventory_quantity: parseInt(
                item.inventory_quantity + action.payload
              )
            }
            : item
        ),
        error: "",
        message: ""
      };

    case "CLEAR_REDUX_STATE":
      return {
        ...state,
        ...initialState
      };

    default:
      return state;
  }
};
