import React, { Component } from "react";
import { Image, TouchableOpacity, Text, View } from "react-native";
import Login from "./components/Login";
import { images } from "../src/assets";
import {
  createStackNavigator,
  createAppContainer,
  createDrawerNavigator,
  DrawerActions
} from "react-navigation";
import ForgotPassword from "./components/ForgotPassword";
import SupervisorTasksTab from "./components/SupervisorTasksTab";
import SupervisorHome from "./components/SupervisorHome";
import HomeScreen from "./components/HomeScreen";
import CrewTaskTab from "./components/CrewTasksTab";
import EditProfile from "./components/EditProfile";
import ChangePassword from "./components/ChangePassword";
import colorsUtil from "./utilities/colorsUtil";
import SideBar from "../src/components/SideBar";
import MyProfile from "./components/MyProfile";
import FAQS from "./components/FAQs";
import TaskAdd from "./components/TaskAdd";
import TaskDetail from "./components/TaskDetail";
import Authentication from "./components/Authentication";
import PortMap from "./components/PortMap";
import BlastingReports from "./components/BlastingReports";
import SegmentScreen from "./components/SegmentScreen";
import Messages from "./components/Messages";
import CreateMessage from "./components/CreateMessage";
import InventoryList from "./components/InventoryList";
import FloorList from "./components/FloorList";
import MessageDetail from "./components/MessageDetail";
import Notifications from "./components/Notifications";


//drawerNavigation
const drawerScreenState = createStackNavigator(
  {
    SegmentScreen: {
      screen: SegmentScreen,
      navigationOptions: ({navigation}) => ({
        title: "My Tasks",
        headerRight: (
          <View style={{marginRight: 18 , flexDirection : "row" }}>
          {/* <TouchableOpacity
            onPress={() => alert("Search")}
          >
            <Image
              style={{ width: 20, height: 20 , marginRight: 15 }}
              source={images.search}
            />
          </TouchableOpacity> */}

          <TouchableOpacity
            onPress={() => {
              navigation.navigate("Notifications");
              navigation.setParams({count:0})
              }
            }
          >
            <Image
              style={{ width: 20, height: 24}}
              source={images.notification}
              resizeMode={'contain'}
            />
           
            {navigation.state.params === undefined ? null : navigation.state.params.count === 0 ? null :
                  <Text style={{color : 'white', fontSize : 12, position: 'absolute', top: 0, right: -4,height: 15, width: 15, textAlign: "center", backgroundColor: 'red', borderRadius: 8 ,overflow:'hidden' }}>
                  {navigation.state.params === undefined ? null : navigation.state.params.count}
                  </Text>
            }
                        
          </TouchableOpacity>
          </View>

        )
      })
    },
    Messages: {
      screen: Messages,
      navigationOptions: () => ({
        title: "Message"
      })
    },
    blastingReport: {
      screen: BlastingReports,
      navigationOptions: () => ({
        title: "Inventory Report"
      })
    },
    myProfile: {
      screen: MyProfile,
      navigationOptions: () => ({
        title: "My Profile"
      })
    },
    fAQS: {
      screen: FAQS,
      navigationOptions: () => ({
        title: "Help & FAQ'S"
      })
    }
  },
  {
    headerMode: "float",
    gesturesEnabled: false,
    initialRouteName:'SegmentScreen',
    headerLayoutPreset: "center",
    defaultNavigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: colorsUtil.toolbarBgColor,
        shadowColor: "transparent",
        elevation: 0,
        borderBottomWidth: 0
      },
      headerTintColor: colorsUtil.toolbarTextColor,
      headerTitleStyle: {
        fontWeight: "bold"
      },

      headerLeft: (
        <TouchableOpacity
          onPress={() => navigation.dispatch(DrawerActions.toggleDrawer())}
        >
          <Image
            style={{ width: 25, height: 18, marginLeft: 15 }}
            source={images.drawer_icon}
          />
        </TouchableOpacity>
      )
    })
  }
);

const DrawerState = createDrawerNavigator(
  {
    drawerScreenStack: { screen: drawerScreenState }
  },
  {
    contentComponent: SideBar,
    drawerWidth: 300,
    drawerPosition: "left",
    drawerOpenRoute: "DrawerOpen",
    drawerCloseRoute: "DrawerClose",
    drawerToggleRoute: "DrawerToggle",
    screenMode: "screen"
  }
);

//Login Stack Navigation
const LoginStack = createStackNavigator(
  {
    loginScreen: {
      screen: Login,
      navigationOptions: () => ({
        header: null
      })
    },
    forgotPasswordScreen: {
      screen: ForgotPassword,
      navigationOptions: () => ({
        title: "Forgot Password"
      })
    }
  },
  {
    headerMode: "float",
    defaultNavigationOptions: ({ navigation }) => ({
      headerStyle: {
        backgroundColor: colorsUtil.toolbarBgColor
      },
      headerTintColor: colorsUtil.toolbarTextColor,
      headerTitleStyle: {
        fontWeight: "bold"
      }
    })
  }
);


//Primary Stack
const AppNavigator = createStackNavigator(
  {
    authentication: {
      screen: Authentication,
      navigationOptions: () => ({
        header: null
      })
    },
    loginStack: {
      screen: LoginStack,
      navigationOptions: () => ({
        header: null
      })
    },
    drawerState: {
      screen: DrawerState,
      navigationOptions: () => ({
        header: null
      })
    },
    HomeScreen: {
      screen: HomeScreen
    },
    supervisorHome: {
      screen: SupervisorHome
    },
    crewTaskTab: {
      screen: CrewTaskTab,
      navigationOptions: () => ({
        header: null
      })
    },
    supervisorTasksTab: {
      screen: SupervisorTasksTab,
      navigationOptions: () => ({
        header: null
      })
    },
    taskAdd: {
      screen: TaskAdd,
      navigationOptions: () => ({
        title: "Add Task"
      })
    },
    Notifications: {
      screen: Notifications,
      navigationOptions: () => ({
        title: "Notifications"
      })
    },
    taskDetail: {
      screen: TaskDetail
    },
    PortMap: {
      screen: PortMap,
      navigationOptions: () => ({
        title: "OBS Ports"
      })
    },
    CreateMessage: {
      screen: CreateMessage,
      navigationOptions: () => ({
        title: "Create Message"
      })
    },
    MessageDetail: {
      screen: MessageDetail,
      navigationOptions: () => ({
        title: "Message Detail"
      })
    },
    InventoryList: {
      screen: InventoryList
    },
    FloorList: {
      screen: FloorList
    },
    viewProfile: {
      screen: MyProfile
    },
    editProfile: {
      screen: EditProfile
    },
    changePassword: {
      screen: ChangePassword,
      navigationOptions: () => ({
        title: "Change Password"
      })
    }
  },
  {
    initialRouteName: "authentication",
    /* The header config from HomeScreen is now here */
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: colorsUtil.toolbarBgColor
      },
      headerLayoutPreset: "center",
      headerTintColor: colorsUtil.toolbarTextColor,
      headerTitleStyle: {
        fontWeight: "bold"
      }
    }
  }
);
export default createAppContainer(AppNavigator);
