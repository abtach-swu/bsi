import DefaultPreference from 'react-native-default-preference';

export const setUser = (user) => {
  DefaultPreference.
    set('user', JSON.stringify(user)).
    then(() => {
    });
}

export const getUserData = () => {

  return new Promise((resolve, reject) => {
    DefaultPreference.
      get('user').
      then((value) => {
        if (value !== undefined && value !== null) {
          resolve(JSON.parse(value))
        }
        resolve(value)
      });
  });
}
export const setUserToken = (token) => {
  DefaultPreference.
    set('token', token).
    then(() => {
    });
}

export const getUserToken = () => {

  return new Promise((resolve, reject) => {
    DefaultPreference.
      get('token').
      then((value) => {
        resolve(value)
      });
  });
}
export const clearAll = () => {
  DefaultPreference.clearAll().then(() => {
  }).catch((error) => {
    console.log(error);
  });
}
