import firebase, { Notification, RemoteMessage } from "react-native-firebase";

class FirebaseNotification {
    fcmToken = () => {
        firebase
            .messaging()
            .getToken()
            .then(fcmToken => {
                if (fcmToken) {
                    // user has a device token
                    console.log("FCM Token  : ", fcmToken);
                } else {
                    // user doesn't have a device token yet
                    console.log("user doesn't have a device token yet");
                }
            })
            .catch(error => {
                console.log("Error : ", error);
            });
    };

    _token = async () => {
        const fcmToken = await firebase.messaging().getToken();
        if (fcmToken) {
            // user has a device token
            console.log("FCM Token  : ", fcmToken);
            return fcmToken;
        } else {
            // user doesn't have a device token yet
            console.log("user doesn't have a device token yet");
            // return "0000";
        }
    };

    notificationPermission = () => {

        return new Promise((resolve, reject) => {
            firebase.messaging().hasPermission().then((bool) => {
                if (bool) {
                    resolve(bool)
                } else {
                    firebase.messaging().requestPermission().then(() => {
                        firebase.messaging().hasPermission().then((bool) => {
                            if (bool) {
                                resolve(bool)
                            }
                        }).catch(() => {
                            reject(false)
                        })
                    }).catch(() => {
                        reject(false)
                    })
                }

            }).catch((error) => {
                reject(false)
            })
        })
    }
    requestPremission = () => {

    }
    _fcmToken = async () => {
        return await firebase.messaging().getToken();
    };
    _notificationDisplay = async () => {
        return await firebase.notifications().onNotificationDisplayed();
    };

    // notificationListener = () => {
    //   firebase.notifications().onNotification(notification => {
    //     console.log("notificationListener : ", notification);
    //     // Process your notification as required
    //   });
    // };
    _messageListener = async () => {
        const onMessgae = await firebase
            .messaging()
            .onMessage((message: RemoteMessage) => {
                // Process your message as required
                console.log("_messageListener ***** ", message);
                // const notificationData = message._data;
                // if(message && message._data){
                // this.props.navigation.navigate("otherProfile", {profile:user});
                // }
            });

        return await firebase.messaging().onMessage();
    };

    _notificationListener = async () => {
        const onNotification = await firebase.notifications().onNotification();
        console.log("_notificationListener utility  : ", onNotification);
        return await firebase.notifications().onNotification();
    };
}
export default new FirebaseNotification();
