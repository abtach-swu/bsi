
import { Toast } from 'native-base';

export const Toasts = {
    showToast: (message, type="danger" ,duration = 2500) => {
      Toast.show({
        text: message,
        duration,
        position: 'bottom',
        textStyle: { textAlign: 'center' },
        type: type
      });
    },
  };
  