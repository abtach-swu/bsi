export default colors = {


    windowBackgroundColor: "#fff",
    buttonColorPurpule: "#530c7e",


    textColorWhite: "#fff",
    textColorPurpule: "#530c7e",

    borderColor: "#f1f1f1",
    borderRoundedColor: "#f1f1f1",
    borderGrayColor: "#a2a2a2",

    darkGray: "#808080",
    gray: "#ccc",
    white: "#fff",
    greenColor: "#00c94a",
    transparentColor: "transparent",
    black: "#000",
    red: "red",
    blue: "#0275be",
    greenColor: "green",
    purple: "#530c7e",
    yellow: "yellow",
    ornage: "orange",

    //Toolbar
    toolbarBgColor: "#530c7e",
    toolbarTextColor: "#fff",

    //Tab Colors
    tabBackgroundColor: "#fff",
    tabUnSelectedTextColor: "#CCC",
    tabSelectedTextColor: "#530c7e",

    tabBorderColor: "#530c7e",
    tabTopBorderColor: "#530c7e",


    //Report
    report_header_background_light_pink: "#ebc9ff",

    segmentBackgroundColor: "#530c7e",
    segmentActiveBackgroundColor: "#fff",
    segmentTextColorNormal: "#fff",
    segmentTextColorActive: "#530c7e"

}
