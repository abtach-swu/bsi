import {
  Alert,
  Dimensions,
  Share
} from 'react-native';

const {
  width,
  height
} = Dimensions.get('window');

export const heightRatio = (orignalHeight) => {
  h = (orignalHeight / 2.69);
  return (h * height) / 736;
}
export const widthRatio = (orignalWidth) => {
  w = (orignalWidth / 2.69);
  return (w * width) / 414;
}
export const validateEmail = (text) => {
  let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (reg.test(text) === false) {
    return false;
  }
  return true;
}


// +nnnnnnnnnn
// +nnn-nnn-nn
// nnnnnnnnnnn
// nnnn-nnnn-n
// (nnn)nnnnnn
// +(nnn)nnnnn

export const validateNumber = (text) => {
  let reg = /s*(?:\+?(\d{1,3}))?([-. (]*(\d{3})[-. )]*)?((\d{3})[-. ]*(\d{2,4})(?:[-.x ]*(\d+))?)/;
  if (reg.test(text) === false) {
    return false;
  }
  return true;
}



export const checkError = (error) => {

  if(error.response === undefined){
      return error.message
  }else if(error.response.status === 500){
      return "Html cannot be parsed"
  }else if(error.response.status === 503){
      return error.message
  }else if(error.response.status === 403){
      return error.message
  }else if(error.response.status === 404) {
      console.log(Object.keys(error.response.data.data[0]))
      var values = Object.keys(error.response.data.data[0]).map((key) => {
          return error.response.data.data[0][key];
      });
      console.log(values.join('{"\n"}'))
      return (values.join('\n'));
  }else{
      return error.message
  }
}

// export const checkError = (error) => {
//   if (error.response === undefined) {
//     return error.message
//   } else if (error.response.status === 500) {
//     return "Html cannot be parsed"
//   } else if (error.response.status === 503) {
//     return error.message
//   } else if (error.response.status === 403) {
//     return error.message
//   } else if (error.response.status === 404) {
//     console.log(Object.keys(error.response.data.data[0]))
//     var values = Object.keys(error.response.data.data[0]).map((key) => {
//       return error.response.data.data[0][key];
//     });
//     console.log('\n• ' + values.join('{"\n•"}'))
//     return ('• ' + values.join('\n• '));
//   } else {
//     return error.message
//   }
// }
export const onShareButtonPressed = (item) => {
  Share.share({
    // message: item.description,
    // title: item.title
    message: item.description,
    title: item.title
    // message: 'BAM: we\'re helping your business with awesome React Native apps',
    // url: 'http://bam.tech',
    // title: 'Wow, did you see that?'
  }, {
    // Android only:
    // dialogTitle: 'Share BAM goodness',

    dialogTitle: item.title,
    // iOS only:
    excludedActivityTypes: [
      'com.apple.UIKit.activity.PostToTwitter'
    ]
  })

}

export const Alerts = (title, description) => {
  Alert.alert(
    title,
    description,
    [{
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      },
      {
        text: 'OK',
        onPress: () => console.log('OK Pressed')
      },
    ], {
      cancelable: false
    }
  )
}