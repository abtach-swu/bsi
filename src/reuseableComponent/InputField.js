import React from 'react';
import { Item, Label, Input } from 'native-base';
import colors from '../utilities/colorsUtil';
const InputField = (Props) => {
    return (

        <Item stackedLabel last>
            {/* <Label style={{ color: colors.black }}>{Props.label}</Label> */}
            <Input placeholder={Props.placeholder} style={{ color: colors.black,fontSize:18 }}
                placeholderTextColor={colors.black}
                secureTextEntry={Props.secureTextEntry}
                onChangeText={Props.onChangeText}></Input>
        </Item>
    );
}
export { InputField };