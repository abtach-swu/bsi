import React, { Component } from "react"
import { View, Text } from "react-native"
import { ProgressCircle } from 'react-native-svg-charts'
import colorsUtil from "../utilities/colorsUtil";
const ReportProgressCircle = (props) => {
    return (
        <View style={{ justifyContent: 'center' }}>
            <ProgressCircle
                style={{ height: props.height ===undefined? 85:props.height }}
                progress={props.progress}
                progressColor={props.progressColor}
                strokeWidth={10}
            />
            <View style={{
                position: 'absolute',
                alignSelf: 'center',
                justifyContent: 'center'
            }}>
                <Text style={{color:colorsUtil.white}}>{props.labelPercent}</Text>
            </View>
            

        </View>
    )
}
export { ReportProgressCircle }