import React, { Component } from 'react';
import { View, Text, ImageBackground , Image, StyleSheet, TextInput , Animated, TouchableOpacity} from 'react-native';

class FloatingLabelInput extends Component {

    //inactiveColor
    //activeColor
    //onChangeText()
    //labelBackgroundColor
    //textInputStyle
    //labelStyle
    
    state = {
        isFocused: false,
    };
    _animatedIsFocused = new Animated.Value(this.props.value === '' ? 0 : 1);

    componentWillMount() {
    }
    
    handleFocus = () => this.setState({
        isFocused: true
    });
    handleBlur = () => this.setState({
        isFocused: false
    });
    componentDidUpdate() {
        Animated.timing(this._animatedIsFocused, {
            toValue: (this.state.isFocused || this.props.value !== '') ? 1 : 0,
            duration: 200,
        }).start();
    }
    labelStyle = {
        position: 'absolute',
        left: 10,
        top: this._animatedIsFocused.interpolate({
            inputRange: [0, 1],
            outputRange: [13, -9],
        }),
        fontSize: this._animatedIsFocused.interpolate({
            inputRange: [0, 1],
            outputRange: [18, 14],
        }),
        color: this._animatedIsFocused.interpolate({
            inputRange: [0, 1],
            outputRange: [this.props.inactiveColor || '#aaa', this.props.activeColor || '#000'],
        }),
        backgroundColor: this.props.labelBackgroundColor || '#fff',
        paddingLeft: 5,
        paddingRight: 5,
        alignSelf: 'center',
    };

    borderColorStyle = {
        borderColor: this._animatedIsFocused.interpolate({
            inputRange: [0, 1],
            outputRange: [this.props.inactiveColor  || '#aaa', this.props.activeColor ||  '#000'],
        }),
    }
    colorStyle = {
        color: this._animatedIsFocused.interpolate({
            inputRange: [0, 1],
            outputRange: [this.props.inactiveColor || '#aaa', this.props.activeColor || '#000'],
        }),
    }
    tintColorStyle = {
        tintColor: this._animatedIsFocused.interpolate({
            inputRange: [0, 1],
            outputRange: [this.props.inactiveColor || '#aaa', this.props.activeColor || '#000'],
        }),
    }
    render() {
        return (
            <Animated.View style={[this.borderColorStyle, {borderWidth: 1,borderRadius: 4,flexDirection: 'row',},this.props.style]}>
                <Animated.Text style={[this.props.labelStyle,this.labelStyle]}>
                    {this.props.label}
                </Animated.Text>
                <TextInput
                    style={[styles.txtInputStyle,this.props.textInputStyle,{color:this.props.activeColor}]}
                    onFocus={this.handleFocus}
                    onBlur={this.handleBlur}
                    editable = {this.props.editable}
                    secureTextEntry = {this.props.secureTextEntry || false}
                    onChangeText={this.props.onChangeText}
                    blurOnSubmit
                    multiline={this.props.multiline && true}
                    value={this.props.value}
                    keyboardType={this.props.keyboardType}
                    />
                 
                <TouchableOpacity onPress={this.props.iconPressed} style={{alignItems: 'center',justifyContent:'center',marginRight:15}} >
                    {this.props.iconImg && <Animated.Image resizeMode='contain' source={this.props.iconImg} style={[this.tintColorStyle,{width:24,height: 24}]}/> || this.props.iconText && <Animated.Text style={this.colorStyle}> {this.props.iconText}</Animated.Text>}
                </TouchableOpacity>
            </Animated.View>
        );
    }
}
  export { FloatingLabelInput }

  const styles = StyleSheet.create({
    txtInputStyle:{
        minHeight: 50 ,
        fontSize: 20,
        color:'#000', 
        paddingLeft: 15,
        paddingTop: 5,
        paddingBottom: 5,
        paddingRight: 15,
        alignSelf: 'stretch',
        flex:1
    }
  });