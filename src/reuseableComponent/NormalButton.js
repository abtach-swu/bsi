import React from "react";
import { Button,  } from "native-base"
import { Text, StyleSheet, Image } from "react-native";
import colorsUtil from "../utilities/colorsUtil";


const NormalButton = (props) => {
    return (
            <Button block style={[styles.buttonStyle,{backgroundColor:props.btnBgColor}, props.btnSyle]} onPress={props.onClick}>
            {props.showImage ? 
            <Image source={props.btnImage}
            style={{ width: 20, height: 20 , marginRight  : 10 }} />
            :
            null
            }
                <Text numberOfLines={1} style={[styles.textStyle,{color:props.textColor}]}>{props.text}</Text>
            </Button>
    )
}
const styles = StyleSheet.create({
    buttonStyle: {
        borderRadius : 5,
        backgroundColor: colorsUtil.buttonColorPurpule,
    
    },
    textStyle: {
        color: colorsUtil.textColorWhite, 
        fontSize : 18,

    }
})



export { NormalButton };