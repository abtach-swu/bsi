import React, { Component } from "react"
import { Text, View, StyleSheet } from "react-native"
import colorsUtil from "../utilities/colorsUtil"

const ReportHeader = (props) => {

    dataCrew = ["Crew", "MNS Cap", "Dynamic", "Primacord"]
    dataPort = ["Port", "MNS Cap", "Dynamic", "Primacord"]


    return (

        <View style={styles.headerStyle}>
            <Text style={styles.textStyle}>
                {props.type == "crew" ? dataCrew[0] : dataPort[0]}
            </Text>
            <Text style={styles.textStyle}>
                {dataCrew[1]}
            </Text>
            <Text style={styles.textStyle}>
                {dataCrew[2]}
            </Text>
            <Text style={styles.textStyle}>
                {dataCrew[3]}
            </Text>
        </View>
    )

}
const styles = StyleSheet.create({

    test: {

    },
    headerStyle: {
        flexDirection: "row",
        marginTop: 8,
        padding: 8,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: colorsUtil.purple,
        backgroundColor: colorsUtil.report_header_background_light_pink,
    },
    textStyle: {
        flex: 1,
        color: colorsUtil.purple,
        textAlign: "center"
    }
});

export { ReportHeader }