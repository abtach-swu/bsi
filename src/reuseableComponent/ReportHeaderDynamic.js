import React, { Component } from "react"
import { Text, View, StyleSheet } from "react-native"
import colorsUtil from "../utilities/colorsUtil"

const ReportHeaderDynamic = (props) => {

    return (

        <View style={styles.headerStyle}>
            {props.dataList.map(item =>
                <Text style={styles.textStyle}>
                    {item}
                </Text>
            )}
        </View>
    )

}
const styles = StyleSheet.create({

    test: {

    },
    headerStyle: {
        flexDirection: "row",
        marginTop: 8,
        padding: 8,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: colorsUtil.purple,
        backgroundColor: colorsUtil.report_header_background_light_pink,
    },
    textStyle: {
        flex: 1,
        color: colorsUtil.purple,
        textAlign: "center"
    }
});

export { ReportHeaderDynamic }