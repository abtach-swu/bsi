import React from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";

const MessageCardItem = props => {
  return (
    <View style={{ padding: 5 }}>
      <TouchableOpacity
        onPress={props._onPressed}
        style={{
          flexDirection: "row",
          alignItems: "center"
        }}
      >   
          <View style={props._shadowStyle}> 
            <Image source={props._imageSource} style={props._imageStyle} />
          </View>
          <View
            style={{
              marginLeft: 10,
              flexDirection: "row",
              justifyContent: "space-between",
              flex: 1
            }}
          >
            <View style={{ flex: 2 }}>
              <Text style={props._titleTextStyle}>{props._titleText}</Text>
              <Text style={props._messageTextStyle} numberOfLines={1}>
                {props._MessageText}
              </Text>
            </View>
            <View
              style={{ justifyContent: "space-between", alignItems: "center" }}
            >
              <Text style={styles.timeStyle}>{props._created_at}</Text>
              {!props._isHide ? (
                <View style={styles.statusIcon} hide={true} />
              ) : null}
            </View>
          </View>
      </TouchableOpacity>
    </View>
  );
};
const styles = {
  timeStyle: {
    color: "grey",
    fontSize: 10
  },
  statusIcon: {
    borderRadius: 100,
    backgroundColor: "purple",
    width: 10,
    height: 10
  },
};
export { MessageCardItem };
