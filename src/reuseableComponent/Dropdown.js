import React from 'react';
import { StyleSheet, Platform } from 'react-native';
import { Item, Picker, Icon, View} from 'native-base';

const Dropdowns = (props) => {

    let serviceItems = props.listItems.map((s, i) => {
        
        return <Picker.Item key={s.id} value={s.id} label={props.type == 'user' ? s.name : s.title}/>

    });

    return (
        <Item style={styles.inputText} >

            <Picker
                enabled={props.enabled}
                mode="dropdown"
                iosHeader={props.placeholder}
                textStyle={(props.textstyle)?{width:(Platform.OS === 'ios') ? '83%' : '100%'}:null}
                // iosIcon={<Icon name="arrow-down" />}
                style={{ borderRadius: 20, width: (Platform.OS === 'ios') ? '83%' : '100%' }}
                placeholder={props.placeholder}
                headerBackButtonText="Back"
                placeholderStyle={{ color: "#bfbfbf" }}
                placeholderIconColor="#007aff"
                itemTextStyle={{ textAlign: 'center' }}
                selectedValue={props.selectedValue}
                onValueChange={props.onValueChange}
            >
                {/* {(Platform.OS !== 'ios') ? 
                <Picker.Item label={props.placeholder} value="-1" />
                :
                <View></View>
            }             */}
            <Picker.Item label={props.placeholder} value="-1" />
            {serviceItems}
            </Picker>
        </Item >


    )
}

const styles = StyleSheet.create({
    inputText: {
        height: 50,
        borderRadius: 10,
        width:(Platform.OS === 'ios') ? '100%' : '100%',

    },pickerText:{
      width:(Platform.OS === 'ios') ? '90%' : '100%',
    }

})
export { Dropdowns };