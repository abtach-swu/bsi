import React, { Component } from "react"
import { View, Text } from "react-native"
import colorsUtil from "../utilities/colorsUtil";
import { ReportProgressCircle } from "./ReportProgressCircle";
import { ListItem, Left } from "native-base";
const ReportProgressCircleItem = (props) => {
    return (


        <View style={{ margin: 10 }}>
            <Text style={{ color: colorsUtil.textColorWhite, fontSize: 16 }}>{props.label}</Text>
            <ReportProgressCircle
                progressColor={props.progressColor}
                progress={props.progress}
                labelPercent={props.labelPercent}
            />
        </View>
    )
}
export { ReportProgressCircleItem }