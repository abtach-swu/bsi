import React from "react";
import { Button, ListItem } from "native-base"
import { Text, StyleSheet } from "react-native";
import colorsUtil from "../utilities/colorsUtil";


const NormalButtonBorderLess = props => {
    return (
            <Button block style={styles.buttonStyle}>
                <Text style={{color:props.color}}>{props.text}</Text>
            </Button>
    )
}
const styles = StyleSheet.create({
    buttonStyle: {
        backgroundColor: colorsUtil.buttonColorPurpule,
    },
})



export { NormalButtonBorderLess };