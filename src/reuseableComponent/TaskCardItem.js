import React, { Component } from "react";
import { View, Image, StyleSheet, TouchableOpacity } from "react-native";
import { Card, CardItem, Left, Thumbnail, Body, Text, Right } from "native-base";
import { images } from "../assets";
import colorsUtil from "../utilities/colorsUtil";
const TaskCardItem = (props) => {

    return (
        <TouchableOpacity onPress={props.onItemSelected}>
            <Card style={styles.cardStyle}>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', height: 30 }}>
                    <Image 
                    source={ props.isCheck === 0 ? images.status_task_gray_image : images.status_task_right_image } 
                    style={{width:30 , height:30}}
                   />
                    
                    <Text note style={{ marginTop : 10 , marginRight : 10}}>{props.time}</Text>
                </View>

                <CardItem style={{flex  :1}}>
                    <Left style={{ flex  :3}}>
                        <View style={styles.shadowStyle}>
                        <Image large source={{ uri: props.thumbnail }} style={styles.profileImageStyle}/>
                        </View>
                        <Body>
                            <Text style={{ fontSize: 15 }}>{props.title}</Text>
                            <View style={{ flexDirection: "row", alignItems: 'center' }}>
                                <Image small source={images.location} style={styles.icon_style} />
                                <Text note style={styles.textStyle}>{props.sub_subtitle}</Text>
                            </View>
                            <View style={{ flexDirection: "row", alignItems: 'center'}}>
                                <Image small source={images.job_number} style={styles.icon_style} />
                                <Text note style={styles.textStyle}>{props.subtitle} </Text>
                            </View>

                        </Body>
                    </Left>
                    <Right style={{ flex  :1 }}>
                        <Image 
                            source={ props.isCheck === 0 ? images.done_disable : images.done_icon }
                            style={{width : 20 , height : 20  }} />
                    </Right>
                </CardItem>
                    <View style={{borderWidth : 0.4 , flex : 1, borderColor : colorsUtil.gray , marginLeft : 20 , marginRight : 20}}></View>
                <CardItem>
                    <View style={{ flexDirection: "row" }}>
                        <Text style={{ flex: 1 }} note numberOfLines={2}>{props.detailDescription} </Text>
                        <Image source={images.arrow_next_icon} style={styles.arrow_image_style} />
                    </View>
                </CardItem>
            </Card>
        </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
    cardStyle: {
        borderRadius: 8,
        margin : 10
    },
    profileImageStyle: {
        borderColor: colorsUtil.borderColor,
        borderWidth: 2,
        width: 60,
        height: 60,
        borderRadius: 30,
  },
  shadowStyle : {
    backgroundColor : 'white',
    elevation: 1,
    shadowOpacity: 0.3,
    shadowRadius: 4,
    shadowOffset: {
      height: 0,
      width: 0
    },
    borderRadius: 30,
  },
    icon_style: {
        width: 10,
        height: 10,
        resizeMode: "contain"
    },
    arrow_image_style: {
        width: 18,
        height: 14,
        // marginTop : 5,
        resizeMode: "contain"
    },
    textStyle: {
        marginLeft: 4,
        fontSize : 12
    }
})

export { TaskCardItem }
