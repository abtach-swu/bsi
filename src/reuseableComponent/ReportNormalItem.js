import React, { Component } from "react"
import { Text, View, StyleSheet } from "react-native"
import colorsUtil from "../utilities/colorsUtil"

const ReportNormalItem = (props) => {

    //dataCrew = ["Jhon Smith", "20", "30", "10"]
    return (

        // <View style={{justifyContent:"center"}}></View>
        <View style={styles.headerStyle}>
            <Text style={styles.textStyle}>
                {props.title}
            </Text>
            <Text style={styles.textStyle}>
                {props.itemOne}
            </Text>
            <Text style={styles.textStyle}>
                {props.itemTwo}
            </Text>
            <Text style={styles.textStyle}>
                {props.itemThree}
            </Text>
        </View>
    )

}
const styles = StyleSheet.create({

    test: {

    },
    headerStyle: {
        flex: 1,
        flexDirection: "row",
        borderBottomWidth: 1,
        borderColor: colorsUtil.borderGrayColor,
    },
    textStyle: {
        flex: 1,
        padding: 8,
        borderRightWidth: 1,
        borderColor: colorsUtil.borderGrayColor,
        // color: colorsUtil.purple,
        textAlign: "center"
    }
});

export { ReportNormalItem }