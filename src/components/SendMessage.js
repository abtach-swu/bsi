import React, { Component } from "react";
import {
  View,
  Text,
  FlatList,
  ActivityIndicator,
  TouchableOpacity,
  Image
} from "react-native";
import { Container } from "native-base";
import { connect } from "react-redux";
import { MessageCardItem } from "../reuseableComponent";
import { getMessages } from "../actions";
import { images } from "../assets";
import moment from "moment";
import Modal from "react-native-modal";
import constant from "../HttpServiceManager/constant";
import colorsUtil from "../utilities/colorsUtil";



class SendMessage extends Component {
  state = {
    messageData: [],
    isLoading: true,
    isRead: false,
    isModalVisible: false,
    item: {},
    
  };
  componentDidMount = () => {
    this.props.getMessages("sent");
  };
  componentWillReceiveProps(nextProps) {

    if(nextProps.error !== ""){
      alert(nextProps.error)
      this.setState({
        isLoading : false,
        messageData : []
      })
    }
    else {
      this.setState({
        messageData: nextProps.messagesData,
        isLoading: false
      });
    }
  }
  _toggleModal = () => {
    this.setState({
      isModalVisible: !this.state.isModalVisible
    });
  };

  _onItemPressed = item => {
    item.is_read = 1;
    this.setState(
      {
        isRead: !this.state.isRead,
        item
      },
      () => {
        this._toggleModal();
      }
    );
  };

  render() {
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text> Loading...</Text>
        <ActivityIndicator size="large" color="purple" />
      </View>
    ) : (
      <Container>
        <FlatList
          contentContainerStyle={{ padding: 10 }}
          data={this.state.messageData}
          extraData={this.state}
          renderItem={({ item }) => {
            return (
              <View>
                <MessageCardItem
                  _isHide={true}
                  _shadowStyle={styles.shadowStyle}
                  _imageSource={{ uri:constant.imageBaseUrl + item.receiver.image_url}}
                  _imageStyle={styles.imageStyle}
                  _titleTextStyle={{ fontWeight: "bold", marginBottom: 5, color : "black" }}
                  _messageTextStyle={{ color: "grey" }}
                  _titleText={`${item.receiver.first_name} ${item.receiver.last_name}`}
                  _MessageText={item.message}
                  _created_at={moment(item.created_at).format("DD-MMM")}
                  _onPressed={() => this._onItemPressed(item)}
                />
              </View>
            );
          }}
          ListEmptyComponent={() => {
            return <Text style={{alignSelf : 'center'}}> Empty !</Text>;
          }}
          ItemSeparatorComponent={() => {
            return <View style={styles.bottomLine} />;
          }}
          onRefresh={() => this.props.getMessages("sent")}
          refreshing={this.state.isLoading}
          // initialNumToRender={3}
          keyExtractor={(item, index) => `${item.id}`}
        />
        <Modal isVisible={this.state.isModalVisible} onBackdropPress={this._toggleModal}>
          <View style={styles.modalContent}>
            <View
              style={{
                justifyContent: "space-between",
                flexDirection: "row",
                backgroundColor: colorsUtil.purple,
                padding: 5,
              }}
            >
              <Text
                style={{
                  fontSize: 20,
                  color : 'white'
                }}
              >
                {this.state.isModalVisible
                  ? `${this.state.item.receiver.first_name} ${
                      this.state.item.receiver.last_name
                    }`
                  : ""}
              </Text>
              <TouchableOpacity onPress={this._toggleModal}>
                <Image
                  source={images.cancel_grey}
                  style={{ width: 15, height: 15 }}
                />
              </TouchableOpacity>
            </View>
            <Text
              style={{
                backgroundColor: "white",
                padding: 10,
                color : 'black'
              }}
            >
              {this.state.item.message}
            </Text>
          </View>
        </Modal>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    messagesData: state.MessageReducer.messageData,
    error: state.MessageReducer.error,
    
  };
};

const mapDispatchToProps = {
  getMessages
};

const styles = {
  imageStyle: {
    width: 40,
    height: 40,
    borderRadius: 20,
  },
  shadowStyle : {
    marginRight : 10,
    borderRadius: 30,
    backgroundColor : 'white',
    elevation: 1,
    shadowOpacity: 0.3,
    shadowRadius: 4,
    shadowOffset: {
      height: 0,
      width: 0
    }
  },
  bottomLine: {
    borderBottomWidth: 1,
    borderBottomColor: "#BFBFBF",
    marginLeft: 60,
    marginRight: 5,
    marginTop: 5
  },
  modalContent: {
    backgroundColor: "white",
    padding: 10,
    borderRadius: 4,
    backgroundColor: "rgba(52, 52, 52, 0.0)"
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SendMessage);
