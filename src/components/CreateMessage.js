import React, { Component } from "react";
import { connect } from "react-redux";
import {
  TextInput,
  TouchableOpacity,
  FlatList,
  View,
  Text,
  ActivityIndicator,
  KeyboardAvoidingView,
  ScrollView,
  Keyboard,
  TouchableWithoutFeedback
} from "react-native";
import { NormalButton } from "../reuseableComponent";
import colorsUtil from "../utilities/colorsUtil";
import { getAllUserList, _onSendMessage } from "../actions";
import { Toasts } from "../utilities/showToast";
import Icon from "react-native-vector-icons/FontAwesome";


  

class CreateMessage extends Component {
  state = {
    userList: [],
    isLoading: true,
    message: "",
    selectedItems: [],
    receiver_id : [],
    isSelected : false
  };
  data = [
    {name : 'abc' , id :1},
    {name : 'abc' , id :2},
    {name : 'abc' , id :3},
    {name : 'abc' , id :4},
    {name : 'abc' , id :5},
    {name : 'abc' , id :6},
    {name : 'abc' , id :7},
    {name : 'abc' , id :8},
    {name : 'abc' , id :9},
    {name : 'abc' , id :10},
    {name : 'abc' , id :11},
    {name : 'abc' , id :12},
    {name : 'abc' , id :13},
    {name : 'abc' , id :14},
    {name : 'abc' , id :15},
  ]


  componentDidMount = () => {
    this.props.getAllUserList();
  };

  onSendMessage = () => {
    let receiver_id = this.state.userList.filter(it => it.is_selected === true).map(itm => itm.id)
    if (receiver_id.length === 0) {
      Toasts.showToast("Please select a receiver");
    }
    else if (this.state.message.trim() === "") {
      Toasts.showToast("Can't send empty message");
    } 
    else {
      this.props._onSendMessage(receiver_id.toString(), this.state.message);
    }
  };

  
  componentWillReceiveProps(nextProps) {
    if (nextProps.userList.length > 0) {
      nextProps.userList.map(item => {
        item.is_selected  =false
      })
      this.setState({
        userList: nextProps.userList,
        isLoading: false
      });
    }
  }

  _onChangeHandler = text => {
    this.setState({
      message: text
    });
  };

  componentDidUpdate() {
    if (this.props.error != "") {
      Toasts.showToast(this.props.error);
    }
    if (this.props.message != "") {
      Toasts.showToast(this.props.message, "success");
      this.props.navigation.pop();
      this.props.navigation.state.params.onReload();
    }
  }

  _onPress = (item) => {
   item.is_selected = !item.is_selected;
    this.setState({
        is_selected: !this.state.is_selected
    })
}

  render() {
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text> Loading...</Text>
        <ActivityIndicator size="large" color="purple" />
      </View>
    ) : (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
          <View style={{flex : 1}}>
            <Text style={styles.titleStyle}>Send to: </Text>
            <FlatList
                  data={this.state.userList}
                  // data={this.data}
                  extraData={this.state}
                  keyExtractor={item => `${item.id}`}
                  renderItem={({ item }) => {
                    return (
                      <TouchableOpacity
                        style={styles.listComponent}
                        onPress={() => {
                         this._onPress(item)
                        }}
                      >
                        <Text
                          style={{
                            fontWeight: "bold",
                            color: item.is_selected ? "#696969" : "#000000"
                          }}
                        >
                          {item.name}
                        </Text>
                        <Icon
                          name={ item.is_selected ? "check" : null}
                          size={15}
                          color="#696969"
                        />
                      </TouchableOpacity>
                    );
                  }}
                />
          </View>
          

          <View style={{flex : 2}}>
            <Text style={styles.titleStyle}>Message</Text>

            <TextInput
              style={{
                height: 80,
                borderColor: "gray",
                borderWidth: 1,
                borderRadius: 10,
                padding: 10,
                textAlignVertical: "top"
              }}
              onChangeText={text => this._onChangeHandler(text)}
              multiline={true}
              value={this.state.message}
              placeholder="Message..."
            />

            <View>
              {!this.props.loading ?
              <NormalButton
              text="Send"
              textColor="#fff"
              onClick={() => this.onSendMessage()}
              btnSyle={{ marginTop: 10 }}
              />
            :
            <View style={{justifyContent: "center", alignItems: "center" , flexDirection : 'row'}}>
              <ActivityIndicator size="small" color="grey" />
              <Text> Sending...</Text>
            </View>
            }
            </View>
            </View>
      </KeyboardAvoidingView>
      </TouchableWithoutFeedback>
    );
  }
}


const mapStateToProps = state => {
  return {
    userList: state.MessageReducer.userList,
    message: state.MessageReducer.message,
    loading: state.MessageReducer.loading,
    error: state.MessageReducer.error,
  };
};

const mapDispatchToProps = {
  getAllUserList,
  _onSendMessage
};

const styles = {
  container: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
  },
  titleStyle: {
    marginTop: 10,
    marginBottom: 10,
    fontSize: 20,
    fontWeight: "bold"
  },
  borderedStyle: {
    borderColor: colorsUtil.borderRoundedColor,
    borderWidth: 10,
    borderRadius: 20
  },
  listComponent: {
    borderRadius: 10,
    marginBottom: 5,
    backgroundColor: "#f2f2f2",
    padding: 13,
    justifyContent: "space-between",
    flexDirection: "row"
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateMessage);
