import React, { Component } from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import { Container, Content, Thumbnail } from "native-base";
import { NormalButton, FloatingLabelInput } from "../reuseableComponent";

import colorsUtil from "../utilities/colorsUtil";
import { getUserData, setUser } from "../UserPreference";
import constant from "../HttpServiceManager/constant";
import ImagePicker from "react-native-image-picker";
import { Toasts } from "../utilities/showToast";
import { connect } from "react-redux";
import { SpinnerView } from "../utilities/common";
import { updateProfile, onChangeName, onChangePicture } from "../actions";
import { VALIDATION_EMPTY_NAME } from "../constants/Constants";

class EditProfile extends Component {
  static navigationOptions = {
    title: "Edit Profile"
  };

  state = {
    userData: {}
  };

  _onChangeText(text, type) {
    this.setState({
      [type]: text
    });
  }
  saveChanges = () => {
    const { name, image } = this.props;
    if (name === "") {
      Toasts.showToast(VALIDATION_EMPTY_NAME);
      return;
    }

    this.props.updateProfile(name, image, true);
  };
  changePassword = () => {
    this.props.navigation.navigate("changePassword");
  };

  componentDidMount() {
    getUserData().then(user => {
      this.props.onChangePicture(constant.imageBaseUrl + user.image_url);
      this.props.onChangeName(user.name);
      this.setState({
        userData: user
      });
    });
  }
  componentDidUpdate() {
    if (this.props.error !== "") {
      Toasts.showToast(this.props.error);
    }
    if (this.props.user !== null) {
      setUser(this.props.user);
    }
    if (this.props.message !== "") {
      Toasts.showToast(this.props.message, "success");
      this.props.navigation.pop();
      this.props.navigation.state.params.userUpdated();
    }

  }

  selectImage = () => {
    const options = {
      title: "Select Image",
      quality: 0.1,
      noData: true
    };
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
      } else if (response.error) {
        Toasts.showToast(response.error);
      } else if (response.customButton) {
      } else {
        //const source = { uri: response.uri };

        this.props.onChangePicture(response.uri);
        //   this.setState({
        //     imageSelect:response,
        //     image:response.uri
        //   })
        //this.setState({image:response.uri})
      }
    });
  };

  render() {
    return (
      <Container>
        <Content contentContainerStyle={styles.topView}>
          <TouchableOpacity onPress={() => this.selectImage()}>
            <Thumbnail
              large
              style={styles.profileImageStyle}
              source={{ uri: this.props.image }}
            />
          </TouchableOpacity>

          {/* <Form style={{width:280,marginTop : 60}}> */}

          <FloatingLabelInput
            style={[styles.commonContent, { marginTop: 50 }]}
            label="Name"
            value={this.props.name}
            onChangeText={text => this.props.onChangeName(text)}
            inactiveColor={colorsUtil.gray}
            activeColor={colorsUtil.tabSelectedTextColor}
            labelBackgroundColor={colorsUtil.white}
            labelStyle={{ color: colorsUtil.textColorPurpule }}
          />
          <FloatingLabelInput
            style={styles.commonContent}
            value={this.state.userData.email}
            editable={false}
            inactiveColor={colorsUtil.gray}
            activeColor={colorsUtil.gray}
            labelBackgroundColor={"transparent"}
            labelStyle={{ color: colorsUtil.gray }}
          />
          <NormalButton
            btnSyle={styles.whiteButtonWithBorder}
            textColor={colorsUtil.purple}
            text="Change Password"
            onClick={() => this.changePassword()}
          />

          <NormalButton
            btnSyle={styles.commonContent}
            text="Save"
            textColor={colorsUtil.white}
            onClick={() => this.saveChanges()}
          />
        </Content>
        {this.props.loading && <SpinnerView />}
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  topView: {
    justifyContent: "center",
    flex: 1,
    alignItems: "center",
    padding: 16
  },
  textPurple: {
    marginTop: 24,
    color: colorsUtil.textColorPurpule
  },
  commonContent: {
    marginLeft: 30,
    marginRight: 30,
    marginTop: 15,
    height: 50
  },
  whiteButtonWithBorder: {
    marginLeft: 30,
    marginRight: 30,
    marginTop: 15,
    height: 50,
    backgroundColor: "white",
    borderColor: "purple",
    borderWidth: 1
  },
  profileImageStyle: {
    borderColor: colorsUtil.borderColor,
    borderWidth: 5,
    height: 120,
    width: 120,
    borderRadius: 60
  }
});

const mapStateToProp = state => {
  return {
    name: state.profileReducer.name,
    image: state.profileReducer.image,
    user: state.profileReducer.user,
    error: state.profileReducer.error,
    loading: state.profileReducer.loading,
    message: state.profileReducer.message
  };
};
export default connect(
  mapStateToProp,
  {
    onChangeName,
    onChangePicture,
    updateProfile
  }
)(EditProfile);
