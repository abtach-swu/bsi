import React, { Component } from "react";

import { StyleSheet, Text, View, TouchableOpacity, Platform } from "react-native";
import {
  Container,
  Content,
  Form,
  Thumbnail
} from "native-base";
import { images } from "../assets";
import colorsUtil from "../utilities/colorsUtil";
import {
  NormalButton,
  FloatingLabelInput
} from "../reuseableComponent";
import { isEmailValid, isValidPassword } from "../utilities/validations";
import { emailChanged, passwordChanged, loginUser } from "../actions";
import { connect } from "react-redux";
import { Toasts } from "../utilities/showToast";
import { VALIDEMAIL, VALIDPASSWORD } from "../constants/Constants";
import { SpinnerView } from "../utilities/common";
import { StackActions, NavigationActions } from "react-navigation";
import firebase from 'react-native-firebase';


class Login extends Component {
  state = {
    email: "",
    password: "",
    user_type: "",
    isLogin: false,
    isHide: true,
    fcmToken: '',
    device_type: '',
  };

  componentDidMount() {
    firebase.messaging().getToken()
      .then(fcmToken => {
        if (fcmToken) {
          // user has a device token
          this.setState({
            fcmToken
          })
        } else {
          // user doesn't have a device token yet
        }
      });
  }

  loginRequest = () => {
    let device_type = Platform.OS
    //Handling Validation
    const { email, password } = this.props
    if (!isEmailValid(email)) {
      Toasts.showToast(VALIDEMAIL)
    }
    else if (!isValidPassword(password)) {
      Toasts.showToast(VALIDPASSWORD)
    } else {
      this.props.loginUser({ email, password, device_token: this.state.fcmToken, device_type }, true)
    }
  };

  forgotPassword = () => {
    this.props.navigation.navigate("forgotPasswordScreen");
  };

  componentDidUpdate() {
    if (this.props.error !== '') {
      Toasts.showToast(this.props.error)
    }
    else if (this.props.user !== null) {
      this._navigateTo("drawerState");
    }
  }

  _navigateTo = routs => {
    const resetAction = StackActions.reset({
      index: 0,
      key: null,
      actions: [NavigationActions.navigate({ routeName: routs })]
    });
    this.props.navigation.dispatch(resetAction);
  };
  render() {
    return (
      <Container>
        <Content contentContainerStyle={styles.topView}>
          <Thumbnail
            source={images.app_logo}
            style={{ marginBottom: 30, width: 150, height: 150 }}
          />
          <Form style={{ width: 280 }}>
            <FloatingLabelInput
              style={{ height: 50 }}
              label="Email"
              value={this.props.email}
              onChangeText={text => this.props.emailChanged(text)}
              inactiveColor={colorsUtil.gray}
              activeColor={colorsUtil.tabSelectedTextColor}
              labelBackgroundColor={colorsUtil.white}
              labelStyle={{ color: colorsUtil.textColorPurpule }}
            />
            <FloatingLabelInput
              style={{ marginTop: 20, height: 50 }}
              label="Password"
              secureTextEntry={this.state.isHide}
              value={this.props.password}
              onChangeText={text => this.props.passwordChanged(text)}
              inactiveColor={colorsUtil.gray}
              activeColor={colorsUtil.tabSelectedTextColor}
              labelBackgroundColor={colorsUtil.white}
              labelStyle={{ color: colorsUtil.textColorPurpule }}
              iconImg={images.icon_eye}
              iconPressed={() => this.setState({ isHide: !this.state.isHide })}
            />
          </Form>

          <View style={{ alignItems: "center" }}>
            <NormalButton
              btnSyle={{ width: 280, height: 50, marginTop: 25 }}
              text="Login"
              textColor={colorsUtil.white}
              onClick={() => this.loginRequest()}
            />
          </View>

          <View style={{ alignItems: "center" }}>
            <TouchableOpacity onPress={() => this.forgotPassword()}>
              <Text
                style={styles.forgotPasswordText}
              >
                Forgot Password ?
            </Text>

            </TouchableOpacity>
          </View>
        </Content>
        {this.props.loading && <SpinnerView />}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  topView: {
    justifyContent: "center",
    flex: 1,
    alignItems: "center",
    padding: 16
  },

  forgotPasswordText: {
    fontSize: 17,
    color: colorsUtil.textColorPurpule,
    marginTop: 30
  }
});

const mapStateToProp = state => {
  return {
    email: state.authReducer.email,
    password: state.authReducer.password,
    error: state.authReducer.error,
    loading: state.authReducer.loading,
    user: state.authReducer.user
  };
};
export default connect(
  mapStateToProp,
  {
    emailChanged,
    passwordChanged,
    loginUser
  }
)(Login);
