import React, { Component } from "react";
import {
  View,
  Text,
  FlatList,
  ActivityIndicator,
  Platform,
  NativeModules
} from "react-native";
import { Container } from "native-base";
import { connect } from "react-redux";
import { MessageCardItem } from "../reuseableComponent";
import { getNotificationList, _getTaskDetail, _onNotificationRead, myTaskApiRequest } from "../actions";
import moment from "moment";
import constant from "../HttpServiceManager/constant";
import colorsUtil from "../utilities/colorsUtil";

// Firebase BadgeCount 
import firebase from "react-native-firebase";

const ShortcutBadger = NativeModules.ShortcutBadger;

class Notifications extends Component {
  state = {
    image: constant.imageBaseUrl,
    userName: '',
    target_id: 0
  }
  componentDidMount = () => {
    this.props.getNotificationList(false)
    Platform.OS === 'ios' ?
      firebase.notifications().setBadge(0) :
      ShortcutBadger.removeCount();
  }


  _onItemPressed = item => {
    this.setState({
      target_id: item.target_id
    })
    if (item.is_read === 0) {
      this.props._onNotificationRead(item.id)
      item.is_read = 1;
    }
    if (item.identifier === "add_task") {
      this.props._getTaskDetail(item.task_id)
    }
    else if (item.identifier === "mark_as_complete") {
      this.props._getTaskDetail(item.task_id)

    }
    else if (item.identifier === "update_task") {
      this.props._getTaskDetail(item.task_id)
    }
    else if (item.identifier === "add_message") {
      this.props.navigation.navigate('Messages')
    }
  };

  onReload = () => {
    this.props.myTaskApiRequest(1, 'today', 'pending', '', this.state.target_id, true);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isConnected) {
      if (nextProps.detail !== null) {
        this.props.navigation.navigate("taskDetail",
          {
            detail: nextProps.detail,
            reload: this.onReload
          });
      }
    }
    else {
      Alert.alert('Something went wrong', 'Kindly check if the device is connected to stable cellular data plan or WiFi.');
    }
  }

  render() {
    return this.props.loading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="purple" />
        <Text> Loading...</Text>
      </View>
    ) : (
        <Container>
          <FlatList
            contentContainerStyle={{ padding: 10 }}
            data={this.props.notificationList}
            extraData={this.state || this.props}
            renderItem={({ item }) => {
              // console.log("{moment.utc(item.created_at).fromNow()}", moment.utc(item.created_at, "MM-DD-YYYY h:m:s").fromNow());
              return (
                <View style={{ paddingBottom: 10, paddingTop: 10 }}>
                  <MessageCardItem
                    _isHide={item.is_read === 0 ? false : true}
                    _shadowStyle={styles.shadowStyle}
                    _imageSource={{ uri: this.state.image + item.user.image_url }}
                    _imageStyle={styles.imageStyle}
                    _titleTextStyle={{ fontSize: 18, marginBottom: 5, color: 'black' }}
                    _messageTextStyle={{ color: "grey" }}
                    _titleText={item.title}
                    _MessageText={item.description}
                    _created_at={moment.utc(item.created_at, "MM-DD-YYYY h:m:s").fromNow()}
                    _onPressed={() => this._onItemPressed(item)}
                  />
                </View>
              );
            }}
            ListEmptyComponent={() => {
              return <Text style={{ alignSelf: 'center' }}> No record found</Text>;
            }}
            ItemSeparatorComponent={() => {
              return <View style={styles.bottomLine} />;
            }}
            onRefresh={() => this.props.getNotificationList(false)}
            refreshing={this.props.loading}
            keyExtractor={(item, index) => `${item.id}`}
          />
        </Container>
      );
  }
}

const mapStateToProps = state => {
  return {

    loading: state.NotificationReducer.loading,
    notificationList: state.NotificationReducer.notificationList,
    error: state.NotificationReducer.error,
    message: state.NotificationReducer.message,
    detail: state.NotificationReducer.detail,
    isConnected: state.authReducer.isConnected,

  };
};

const mapDispatchToProps = {
  getNotificationList,
  _getTaskDetail,
  _onNotificationRead,
  myTaskApiRequest
};

const styles = {
  imageStyle: {
    borderColor: colorsUtil.white,
    borderWidth: 2,
    width: 60,
    height: 60,
    borderRadius: 30
  },
  shadowStyle: {
    marginRight: 10,
    borderRadius: 30,
    backgroundColor: 'white',
    elevation: 1,
    shadowOpacity: 0.3,
    shadowRadius: 4,
    shadowOffset: {
      height: 0,
      width: 0
    }
  },
  bottomLine: {
    borderBottomWidth: 1,
    borderBottomColor: "#BFBFBF",
    marginLeft: 5,
    marginRight: 5,
    marginTop: 5
  },
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Notifications);
