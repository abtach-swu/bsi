import React, { Component } from "react";
import { Container, Tab, Tabs } from "native-base"
import { getUserData } from "../UserPreference";
import CrewTasksTab from "./CrewTasksTab";
import { myTaskApiRequest } from "../actions";
import { connect } from "react-redux";

class HomeScreen extends Component {
    state = {
        selectedTab: "today",
        signinUser: null,
        isConnected: false
    }

    componentDidMount() {
        getUserData().then(user => {
            this.setState({
                signinUser: user
            })
            if (user !== null) {
                this.props.myTaskApiRequest(1, 'today', 'pending', '', this.state.signinUser.id, true);
            }
        });
    }

    onReload = () => {
        this.props.myTaskApiRequest(1, this.state.selectedTab.toLowerCase(), 'pending', '', this.state.signinUser.id, true);
    }

    onTabSelected = (heading) => {
        this.setState({
            selectedTab: heading
        })
        if (heading.toLowerCase() === 'done') {
            this.props.myTaskApiRequest(1, '', 'completed', '', this.state.signinUser.id, true);
        } else {
            this.props.myTaskApiRequest(1, heading.toLowerCase(), 'pending', '', this.state.signinUser.id, true);

        }
    }

    render() {
        return this.state.signinUser !== null ? (
            <Container>
                <Tabs
                    onChangeTab={({ ref }) => {
                        this.onTabSelected(ref.props.heading);
                    }
                    }
                >
                    <Tab heading="Today">
                        <CrewTasksTab navigation={this.props.navigation}
                            reload={this.onReload}
                            filterType={'today'}
                            statusType={'pending'}
                            target_id={this.state.signinUser.id}
                        />
                    </Tab>
                    <Tab heading="Upcoming">
                        <CrewTasksTab navigation={this.props.navigation}
                            reload={this.onReload}
                            filterType={'upcoming'}
                            statusType={'pending'}
                            target_id={this.state.signinUser.id}
                        />
                    </Tab>
                    <Tab heading="Done">
                        <CrewTasksTab navigation={this.props.navigation}
                            reload={this.onReload}
                            statusType={'completed'}
                            filterType={''}
                            target_id={this.state.signinUser.id}
                        />
                    </Tab>
                </Tabs>
            </Container>
        )
            :
            null
    }
}

const mapStateToProp = state => {
    return {
        error: state.mytaskReducer.error,
        loading: state.mytaskReducer.loading,
        data: state.mytaskReducer.responseData,
        refreshing: state.mytaskReducer.refreshing,
        nextPage: state.mytaskReducer.nextPage,
        currentPage: state.mytaskReducer.currentPage
    };
};
export default connect(
    mapStateToProp,
    {
        myTaskApiRequest,
    }
)(HomeScreen);



