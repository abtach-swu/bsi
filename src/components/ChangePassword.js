import React, { Component } from "react"
import { View, StyleSheet } from "react-native"
import { Container, Content, Text, Thumbnail, Form, Button } from "native-base";
import { NormalButton, FloatingLabelInput } from "../reuseableComponent";

import colorsUtil from "../utilities/colorsUtil";
import { images } from "../assets";

import { Toasts } from "../utilities/showToast";
import { connect } from "react-redux";
import { SpinnerView } from "../utilities/common";
import { changePasswordRequest, clearErrors } from "../actions"
import {
    VALIDATION_OLD_PWD,
    VALIDATION_NEW_PWD,
    VALIDATION_CONFIRM_PWD,
    VALIDATION_CONFIRM_PWD_NOT_MATCH
} from "../constants/Constants";
class ChangePassword extends Component {

    static navigationOption = {
        title: "Change Password"
    }
    state = {
        oldPassword: '',
        newPassword: '',
        confirmPassword: '',
        isCurrenHide: true,
        isNewHide: true,
        isConfirmHide: true,
    }

    _onChangeText(text, type) {
        this.props.clearErrors()
        this.setState({
            [type]: text
        })
    }
    saveChanges = () => {

        const { oldPassword, newPassword, confirmPassword } = this.state
        if (oldPassword === '') {
            Toasts.showToast(VALIDATION_OLD_PWD)
        }
        else if (newPassword === '') {
            Toasts.showToast(VALIDATION_NEW_PWD)
        }
        else if (confirmPassword === '') {
            Toasts.showToast(VALIDATION_CONFIRM_PWD)
        } else if (confirmPassword !== newPassword) {
            Toasts.showToast(VALIDATION_CONFIRM_PWD_NOT_MATCH)
        }
        else {
            this.props.changePasswordRequest(oldPassword, newPassword, true)
        }
    }
    componentDidUpdate() {

        if (this.props.error !== '') {
            Toasts.showToast(this.props.error)
        }
        if (this.props.message !== '') {
            Toasts.showToast(this.props.message, "success");
        }
    }

    securePasswordHandler = () => {

        // this.setState({

        // })
    }

    render() {
        return (
            <Container>
                <Content contentContainerStyle={styles.topView}>

                    {/* <Form style={{marginTop : 60}}> */}

                    <FloatingLabelInput
                        style={styles.commonContent}
                        label="Old Password"
                        value={this.state.oldPassword}
                        secureTextEntry={this.state.isCurrenHide}
                        onChangeText={(text) => this._onChangeText(text, 'oldPassword')}
                        inactiveColor={colorsUtil.gray}
                        activeColor={colorsUtil.tabSelectedTextColor}
                        labelBackgroundColor={colorsUtil.white}
                        labelStyle={{ color: colorsUtil.textColorPurpule }}
                        iconImg={images.icon_eye}
                        type='currentpass'
                        name='currentpass'
                        iconPressed={() => {
                            this.setState({
                                isCurrenHide: !this.state.isCurrenHide,
                                isNewHide: true,
                                isConfirmHide: true
                            })
                        }}
                    />
                    <FloatingLabelInput
                        style={styles.commonContent}
                        label="New Password"
                        value={this.state.newPassword}
                        secureTextEntry={this.state.isNewHide}
                        onChangeText={(text) => this._onChangeText(text, 'newPassword')}
                        inactiveColor={colorsUtil.gray}
                        activeColor={colorsUtil.tabSelectedTextColor}
                        labelBackgroundColor={colorsUtil.white}
                        labelStyle={{ color: colorsUtil.textColorPurpule }}
                        iconImg={images.icon_eye}
                        iconPressed={() => {
                            this.setState({
                                isCurrenHide: true,
                                isNewHide: !this.state.isNewHide,
                                isConfirmHide: true
                            })
                        }}
                    />
                    <FloatingLabelInput
                        style={styles.commonContent}
                        label="Confirm Password"
                        value={this.state.confirmPassword}
                        secureTextEntry={this.state.isConfirmHide}
                        onChangeText={(text) => this._onChangeText(text, 'confirmPassword')}
                        inactiveColor={colorsUtil.gray}
                        activeColor={colorsUtil.tabSelectedTextColor}
                        labelBackgroundColor={colorsUtil.white}
                        labelStyle={{ color: colorsUtil.textColorPurpule }}
                        iconImg={images.icon_eye}
                        iconPressed={() => {
                            this.setState({
                                isCurrenHide: true,
                                isNewHide: true,
                                isConfirmHide: !this.state.isConfirmHide
                            })
                        }}
                    />

                    {/* </Form> */}

                    <NormalButton
                        btnSyle={styles.commonContent}
                        text="Save" textColor={colorsUtil.white} onClick={() => this.saveChanges()}
                    />

                </Content>
                {this.props.loading && <SpinnerView />}
            </Container>
        )
    }
}
const styles = StyleSheet.create({
    topView: {
        justifyContent: 'center',
        flex: 1,
        alignItems: "center",
        padding: 16
    },
    textPurple: {
        marginTop: 24,
        color: colorsUtil.textColorPurpule
    },
    commonContent: {
        marginLeft: 30,
        marginRight: 30,
        marginTop: 15,
        height: 50
    },
    whiteButtonWithBorder: {
        marginLeft: 30,
        marginRight: 30,
        marginTop: 15,
        height: 50,
        backgroundColor: "white", borderColor: 'purple', borderWidth: 1
    },
    profileImageStyle: {
        borderColor: colorsUtil.borderColor,
        borderWidth: 5,
        //width : 90,
        //height : 90,
        // marginBottom : 40
    }
})
const mapStateToProp = state => {
    return {
        error: state.profileReducer.error,
        loading: state.profileReducer.loading,
        message: state.profileReducer.message,
    };
};
export default connect(
    mapStateToProp,
    {
        clearErrors,
        changePasswordRequest
    }
)(ChangePassword);