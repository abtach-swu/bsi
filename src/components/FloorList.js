import React, { Component } from "react";
import {
  Text,
  View,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
  // Platform,
  // Image
} from "react-native";
import { Toasts } from "../utilities/showToast";
import Icon from "react-native-vector-icons/FontAwesome";
import {
  onSelectFloor,
  getPortApiRequest,
  onFloorDisselect,
  _onFloorSelectionDone,
  onFloorListComp
} from "../actions";
import { connect } from "react-redux";
import { images } from "../assets";


let _this = null;

class FloorList extends Component {
  state = {
    floorList: [],
    is_selected: false,
    loading: true,
  };
  static navigationOptions = ({ navigation }) => {
    return {
      title: "Floors",
      headerRight: (
        <TouchableOpacity onPress={() => _this.onDone()}>
          <Text style={{ color: "white", marginRight: 10, fontSize: 15 }}>
            Done
          </Text>
        </TouchableOpacity>
      ),

    };
  };

  componentDidMount = () => {
    _this = this;
    this.props.onFloorListComp()
    const cloneFloorList = JSON.parse(JSON.stringify(this.props.floorData))
    this.setState({
      floorList: cloneFloorList,
      loading: false
    })
  };


  onDone = () => {
    let selectedFloorsTitle = this.state.floorList
      .filter(item => item.is_selected === true)
      .map(itm => itm.title);
    if (selectedFloorsTitle.length > 0) {
      Toasts.showToast(`Floor added`, "success");
      // Toasts.showToast(`${selectedFloorsTitle.length} floor added`, "success");
      this.props._onFloorSelectionDone(this.state.floorList)
      this.props.navigation.state.params.getSelectedFloor(
        selectedFloorsTitle.toString(),
      );
      this.props.navigation.pop();
    } else {
      Toasts.showToast("Please select at least one floor");
    }
  }


  onItemSelect = item => {
    if (!item.is_selected) {
      let id = item.id;
      this.props.onSelectFloor(id);
      this.props.getPortApiRequest(false, id);
      this.props.navigation.navigate("PortMap", {
        isSelectPort: true,
        item,
        itemSelected: this._itemSelected
      });
    } else {
      let id = item.id;
      this.props.onFloorDisselect(id);
      item.is_selected = !item.is_selected;
      this.setState({
        is_selected: !this.state.is_selected
      });
    }
  };

  _itemSelected = (item) => {
    item.is_selected = !item.is_selected;
    this.setState({
      is_selected: !this.state.is_selected
    });
  };

  render() {
    return this.state.loading ? (
      <View
        style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
      >
        <Text> Loading...</Text>
        <ActivityIndicator size="large" color="purple" />
      </View>
    ) : (
        <View style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
          <FlatList
            data={this.state.floorList}
            contentContainerStyle={{ marginTop: 10, paddingBottom: 20 }}
            ListEmptyComponent={() => (
              <Text style={{ alignSelf: "center" }}>No data found </Text>
            )}
            extraData={this.state}
            keyExtractor={item => `${item.id}`}
            renderItem={({ item }) => {
              return (
                <TouchableOpacity
                  style={styles.listComponent}
                  onPress={() => {
                    this.onItemSelect(item);
                  }}
                >
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: item.is_selected ? "#696969" : "#000000"
                    }}
                  >
                    {item.title}
                  </Text>
                  <Icon
                    name={item.is_selected ? "check" : null}
                    size={15}
                    color="#696969"
                  />
                </TouchableOpacity>
              );
            }}
          />
        </View>
      );
  }
}
const mapStateToProp = state => {
  return {
    floorData: state.addTaskReducer.floorResponseData
  };
};

const styles = StyleSheet.create({
  titleStyle: {
    marginTop: 16,
    marginBottom: 10,
    fontSize: 20,
    fontWeight: "bold",
    marginLeft: 10
  },
  listComponent: {
    borderRadius: 10,
    marginRight: 12,
    marginLeft: 12,
    marginBottom: 5,
    backgroundColor: "white",
    padding: 15,
    justifyContent: "space-between",
    flexDirection: "row"
  }
});

export default connect(
  mapStateToProp,
  {
    getPortApiRequest,
    onSelectFloor,
    onFloorDisselect,
    _onFloorSelectionDone,
    onFloorListComp
  }
)(FloorList);
