import React, { Component } from "react";
import { StyleSheet, FlatList, TouchableOpacity, Image } from "react-native";
import {
  Container,
  Text,
  Content,
  View,
  Item,
  Input,
  Label
} from "native-base";
import { images } from "../assets";
import DatePicker from "react-native-datepicker";
import { Toasts } from "../utilities/showToast";
import { heightRatio } from "../utilities/utility";
import Icon from "react-native-vector-icons/FontAwesome";
import moment from 'moment';

import {
  NormalButton,
  FloatingLabelInput,
  Dropdowns
} from "../reuseableComponent";
import colorsUtil from "../utilities/colorsUtil";
import {
  addTask,
  getUserApiRequest,
  onChangeInventoryName,
  onChangeInventoryQuantity,
  getInventoryApiRequest,
  getPlantApiRequest,
  getUnitApiRequest,
  getFloorApiRequest,
  getPortApiRequest,
  onSelectTarget,
  onSelectPlant,
  onSelectUnit,
  onSelectFloor,
  onSelectPort,
  onChangeDate,
  onChangeTime,
  isModalVisible,
  increment,
  updateTaskReq
} from "../actions";
import { connect } from "react-redux";
import { SpinnerView } from "../utilities/common";

class TaskAdd extends Component {
  state = {
    description: "",
    portData: {},
    time: new Date(),
    job_number: "",
    select_ports: "Port Doors",
    selected_floors: "Floor & Ports",
    is_selected: false,
    isInventorySelected: false,
    isFloorSelected: false,
    editable: false,
  };

  componentDidMount() {
    let editable = this.props.navigation.state.params.editable
    if (this.props.navigation.state.params.editable) {
      this.setState({
        editable,
        isInventorySelected: editable,
        description: this.props.description,
        job_number: this.props.job_number
      })
      this.props.getUserApiRequest(false);
      this.props.getInventoryApiRequest(true);
    } else {
      this.props.getUserApiRequest(true);
      this.props.getInventoryApiRequest(true);
      this.props.getPlantApiRequest(true);
    }
  }

  onCrewValueChange = id => {
    this.props.onSelectTarget(id);
  };

  onPlantValueChange = id => {
    this.props.onSelectPlant(id);
    this.setState({
      selected_floors: "Floor & Ports"
    })

    if (id !== "-1") {
      this.props.getUnitApiRequest(false, id);
    }
  };
  onUnitValueChange = id => {
    this.props.onSelectUnit(id);
    this.setState({
      selected_floors: "Floor & Ports"
    })
    if (id !== "-1") {
      this.props.getFloorApiRequest(false, id);
    }
  };

  onPortValueChange = id => {
    this.props.onSelectPort(id);
  };

  _onChangeText(text, name) {
    this.setState({
      [name]: text
    });
  }


  _toggleModal = () => {
    if (this.props.floorData.length > 0) {
      this.props.isModalVisible();
    } else {
      Toasts.showToast("Selected unit have no floor");
    }
  };

  _addInventory = () => {
    this.props.navigation.navigate("InventoryList", {
      getSelectedInventory: this.getSelectedInventory
    });
  };

  getSelectedInventory = () => {
    this.setState({
      isInventorySelected: true,
    });
  };

  _addFloors = () => {
    if (this.props.floorData.length > 0) {
      this.props.navigation.navigate("FloorList", {
        getSelectedFloor: this.getSelectedFloor
      });
    } else {
      Toasts.showToast("Please select a unit or selected unit has no floor");
    }
  };

  getSelectedFloor = (selectedFloorsTitle) => {
    this.setState({
      isFloorSelected: true,
      selected_floors: `${selectedFloorsTitle} Floor Added`
    });
  };

  handleInputChange = (text, item) => {
    this.props.onChangeInventoryQuantity(text, item.id);
  };

  onDone = () => {
    let inventoryData = this.props.inventoryResponseData.filter(obj => obj.is_selected);
    if (inventoryData.length !== 0) {
      let valid = true;
      for (var i = 0; i < inventoryData.length; i++) {
        if (
          inventoryData[i].inventory_name.trim() === "" ||
          inventoryData[i].inventory_quantity === 0
        ) {
          Toasts.showToast("Inventory name or quantity is required")
          valid = false;
          break;
        }
      }
      if (this.state.isFloorSelected) {
        if (valid) {
          let params = {
            target_id: this.props.selectedTarget,
            inventory: JSON.stringify(inventoryData),
            plant_id: this.props.selectedPlant,
            unit_id: this.props.selectedUnit,
            port_data: JSON.stringify(this.props.portSelectionDone),
            schedule_date: this.props.date,
            schedule_time: this.props.time,
            job_number: this.state.job_number,
            description: this.state.description
          };
          this.props.addTask(true, params);
        }
      }
      else {
        Toasts.showToast("Please select a floor");
      }

    } else {
      Toasts.showToast("No Inventory Selected");
    }
  };

  updateTask = () => {
    let inventoryData = this.props.inventoryResponseData.filter(obj => obj.is_selected);
    if (inventoryData.length !== 0) {
      let valid = true;
      for (var i = 0; i < inventoryData.length; i++) {
        if (
          inventoryData[i].inventory_name.trim() === "" ||
          inventoryData[i].inventory_quantity === 0
        ) {
          Toasts.showToast("Inventory name or quantity is required");
          valid = false;
          break;
        }
      }
      if (valid) {
        let params = {
          task_id: this.props.task_id,
          target_id: this.props.selectedTarget,
          inventory: JSON.stringify(inventoryData),
          plant_id: this.props.selectedPlant,
          unit_id: this.props.selectedUnit,
          port_data: JSON.stringify(this.props.portSelectionDone),
          schedule_date: this.props.date,
          schedule_time: this.props.time,
          job_number: this.state.job_number,
          description: this.state.description
        };
        this.props.updateTaskReq(true, params);
      }
    } else {
      Toasts.showToast("Please select a inventory");
    }
  };

  componentDidUpdate() {
    //Tempo update
    if (this.props.error !== "") {
      Toasts.showToast(this.props.error);
    }
    // if (this.props.error !== "") {
    //   Toasts.showToast(this.props.error);
    // }
    if (this.props.message !== "") {
      Toasts.showToast(this.props.message, "success");
      if (this.state.editable) {
        this.props.navigation.navigate("SegmentScreen")
        this.props.navigation.state.params.reload();
      }
      else {
        this.props.navigation.pop();
        this.props.navigation.state.params.reload();
      }
    }
  }

  // ==============================================
  // Inventory Component Render
  // ==============================================
  renderItem = ({ item, index }) => {
    return (
      <View style={styles.inventoryRowStyle}>
        <Label style={styles.inventoryLabels}>{item.title}</Label>
        <Item regular style={styles.inventoryInputLarge}>
          <Input
            // value={(this.state.editable)? item.name : item.inventory_name}
            value={item.inventory_name}
            key={item.id}
            onChangeText={text =>
              this.props.onChangeInventoryName(text, item.id)
            }
            maxLength={14}
            autoCapitalize='characters'
          />
        </Item>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-between",
            flex: 2
          }}
        >
          <TouchableOpacity
            disabled={item.inventory_quantity === 0 || item.total_qty === 0 ? true : false}
            onPress={() => this.props.increment(-1, item.id)}
            style={{ padding: 5 }}
          >
            <Image
              source={
                item.inventory_quantity == 0 || item.total_qty === 0
                  ? images.minus_icon_red
                  : images.minus_icon
              }
              style={{ height: 15, width: 10 }}
            />
          </TouchableOpacity>
          <Item regular style={styles.inventoryInputSmall}>
            <Input
              value={`${item.inventory_quantity}`}
              // value={(this.state.editable)? `${item.total_qty}` :`${item.inventory_quantity}`}
              placeholder="0"
              keyboardType="numeric"
              onChangeText={text => this.handleInputChange(text, item)}
              maxLength={6}
            />
          </Item>
          <TouchableOpacity
            onPress={() => this.props.increment(1, item.id)}
            style={{ paddingRight: 5, paddingBottom: 5, paddingTop: 5 }}
          >
            <Image
              source={images.plus_icon}
              style={{ height: 20, width: 20 }}
            />
          </TouchableOpacity>
        </View>
      </View>
    )
  };

  render() {
    return (
      <Container>
        <Content contentContainerStyle={styles.topView}>
          <Text style={styles.titleStyle}>Person Details</Text>

          <Item regular style={{ borderRadius: 10 }}>
            <Dropdowns
              textstyle={false}
              label={this.props.selectedTarget}
              selectedValue={this.props.selectedTarget}
              onValueChange={id => this.onCrewValueChange(id)}
              listItems={this.props.crewData}
              type={"user"}
              userId={48}
              placeholder="Select Member"
            />
          </Item>
          <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          >
            <Text style={styles.titleStyle}>Inventory</Text>
            <Icon
              // name={"edit"}
              name={this.state.isInventorySelected ? "edit" : null}
              size={25}
              color="#696969"
              style={{ marginTop: 16, marginBottom: 10 }}
              onPress={() =>
                this.props.navigation.navigate("InventoryList", {
                  getSelectedInventory: this.getSelectedInventory
                })
              }
            />
          </View>

          <View style={styles.inventorySection}>
            {this.state.isInventorySelected ? (
              <FlatList
                data={this.props.inventoryResponseData.filter(item => item.is_selected)}
                renderItem={this.renderItem}
                keyExtractor={item => item.id.toString()}
              />
            ) : (
                <NormalButton
                  text="Add Inventory"
                  textColor="#fff"
                  onClick={() => this._addInventory()}
                  btnBgColor="#bfbfbf"
                />
              )}
          </View>
          <View style={{ display: this.state.editable ? "none" : null }}>
            <Text style={styles.titleStyle}>Location</Text>

            <View style={{ flex: 1, flexDirection: "column" }}>
              <View style={{ flex: 1, flexDirection: "row", margin: 5 }}>
                <Item
                  regular
                  style={{ flex: 2, borderRadius: 10, marginBottom: 10 }}
                >
                  <Dropdowns
                    textstyle={true}
                    placeholder="Plant"
                    label={this.props.selectedPlant}
                    selectedValue={this.props.selectedPlant}
                    onValueChange={(id, value) => {
                      this.onPlantValueChange(id);
                    }}
                    listItems={this.props.plantData}
                  />
                </Item>
                <Item
                  regular
                  style={{ flex: 2, borderRadius: 10, marginBottom: 10 }}
                >
                  <Dropdowns
                    enabled={this.props.unitData.length > 0 ? true : false}
                    textstyle={true}
                    placeholder="Unit"
                    label={this.props.selectedUnit}
                    selectedValue={this.props.selectedUnit}
                    onValueChange={(id, ind) => this.onUnitValueChange(id, ind)}
                    listItems={this.props.unitData}
                  />
                </Item>
              </View>

              <View style={{ flex: 1, margin: 5 }}>
                <NormalButton
                  text={this.state.selected_floors}
                  textColor="#fff"
                  onClick={() => this._addFloors()}
                  btnBgColor="#bfbfbf"
                />
              </View>
            </View>
          </View>

          <Text style={styles.titleStyle}>Schedule</Text>

          <View>
            <View style={styles.datePicker}>
              <DatePicker
                style={styles.datePickerFieldStyle}
                date={this.props.date} //initial date from state
                mode="date" //The enum of date, datetime and time
                placeholder="select date"
                // format={this.state.editable ? "DD/MM/YYYY" : "DD-MM-YYYY"}
                format="DD-MM-YYYY"
                minDate={new Date()}
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                onDateChange={date => {
                  this.props.onChangeDate(date);
                }}
                customStyles={{
                  dateIcon: {
                    position: "absolute",
                    left: 0,
                    top: 4,
                    marginLeft: 0
                  }
                }}
              />
              {/* {console.log("moment(this.props.data).isSame(new Date().toLocaleDateString(), 'day')" , moment(this.props.date).isSame(new Date().toLocaleDateString(), 'day'))} */}
              <DatePicker
                style={[styles.datePickerFieldStyle, { marginTop: 10 }]}
                date={this.props.time} //initial date from state
                mode="time" //The enum of date, datetime and time
                placeholder="select time"
                format="h:mm a"
                minDate={(moment(this.props.date).isSame(new Date().toLocaleDateString(), 'day')) ? new Date() : "01-01-1970"}
                // maxDate="01-01-2019"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                onDateChange={time => {
                  this.props.onChangeTime(time);
                }}
                customStyles={{
                  dateIcon: {
                    position: "absolute",
                    left: 0,
                    top: 4,
                    marginLeft: 0
                  }
                }}
              />
            </View>
          </View>

          <View style={{ margin: 10 }}>
            <FloatingLabelInput
              label="Job Number"
              // keyboardType="phone-pad"
              value={this.state.job_number}
              onChangeText={text => this._onChangeText(text, "job_number")}
              inactiveColor={colorsUtil.gray}
              activeColor={colorsUtil.tabSelectedTextColor}
              labelBackgroundColor={colorsUtil.white}
              textInputStyle={{ color: "black" }}
            />
          </View>

          <View style={{ margin: 10 }}>
            <FloatingLabelInput
              label="Description"
              value={this.state.description}
              onChangeText={text => this._onChangeText(text, "description")}
              inactiveColor={colorsUtil.gray}
              activeColor={colorsUtil.tabSelectedTextColor}
              labelBackgroundColor={colorsUtil.white}
              textInputStyle={{ color: "black" }}
            />
          </View>

          <NormalButton
            text={this.state.editable ? "Update" : "Done"}
            // btnBgColor={this.state.editable? "#2eb82e" : colorsUtil.buttonColorPurpule}
            textColor="#fff"
            onClick={() => this.state.editable ? this.updateTask() : this.onDone()}
            btnSyle={{ margin: 12 }}
          />
        </Content>
        {this.props.loading && <SpinnerView />}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  topView: {
    justifyContent: "space-around",
    paddingLeft: 20,
    paddingRight: 20
  },
  titleStyle: {
    marginTop: 16,
    marginBottom: 10,
    fontSize: 20,
    fontWeight: "bold"
  },
  borderedStyle: {
    borderColor: colorsUtil.borderRoundedColor,
    borderWidth: 10,
    borderRadius: 20
  },
  inventorySection: {
    flex: 1,
    flexDirection: "column",
    marginTop: 5
  },
  inventoryRowStyle: {
    flex: 1,
    flexDirection: "row",
    marginTop: 5,
    justifyContent: "space-evenly"
  },
  inventoryInputLarge: {
    flex: 2,
    height: 35,
    borderRadius: 8,
    borderColor: colorsUtil.gray,
    borderWidth: 1,
    marginRight: 7,
    padding: 5
  },
  inventoryInputSmall: {
    flex: 1,
    height: 35,
    borderRadius: 8,
    borderColor: colorsUtil.gray,
    borderWidth: 1,
    padding: 5,
    color: colorsUtil.gray
  },
  inventoryLabels: {
    flex: 2
  },
  clock_icon_style: {
    width: 10,
    height: 10,
    resizeMode: "contain"
  },
  datePicker: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    padding: 16
  },
  datePickerFieldStyle: {
    width: "100%",
    borderRadius: 10,
    borderWidth: 1,
    borderColor: colorsUtil.borderRoundedColor
  }
  // modalContent: {
  //   backgroundColor: "#F5F5F5",
  //   padding: 10,
  //   borderRadius: 4,
  //   borderColor: "rgba(0, 0, 0, 0.1)"
  // },
  // listComponent: {
  //   borderRadius: 10,
  //   marginRight: 12,
  //   marginLeft: 12,
  //   marginBottom: 5,
  //   backgroundColor: "white",
  //   padding: 10,
  //   justifyContent: "space-between",
  //   flexDirection: "row"
  // }
});

const mapStateToProp = state => {
  return {
    error: state.addTaskReducer.error,
    loading: state.addTaskReducer.loading,
    crewData: state.addTaskReducer.crewResponseData,
    inventoryData: state.addTaskReducer.selectedInventory,
    plantData: state.addTaskReducer.plantResponseData,
    unitData: state.addTaskReducer.unitResponseData,
    floorData: state.addTaskReducer.floorResponseData,
    portData: state.addTaskReducer.portResponseData,
    message: state.addTaskReducer.message,
    selectedTarget: state.addTaskReducer.selectedTarget,
    selectedPlant: state.addTaskReducer.selectedPlant,
    portSelectionDone: state.addTaskReducer.portSelectionDone,
    selectedUnit: state.addTaskReducer.selectedUnit,
    selectedFloor: state.addTaskReducer.selectedFloor,
    date: state.addTaskReducer.date,
    time: state.addTaskReducer.time,
    description: state.addTaskReducer.description,
    job_number: state.addTaskReducer.job_number,
    task_id: state.addTaskReducer.task_id,

    inventoryResponseData: state.addTaskReducer.inventoryResponseData
  };
};

export default connect(
  mapStateToProp,
  {
    getUserApiRequest,
    onChangeInventoryName,
    onChangeInventoryQuantity,
    getPlantApiRequest,
    getUnitApiRequest,
    getFloorApiRequest,
    getPortApiRequest,
    addTask,
    getInventoryApiRequest,
    onSelectTarget,
    onSelectPlant,
    onSelectUnit,
    onSelectFloor,
    onSelectPort,
    onChangeDate,
    onChangeTime,
    isModalVisible,
    increment,
    updateTaskReq
  }
)(TaskAdd);
