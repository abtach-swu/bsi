import React, { Component } from "react";
import {
  Text,
  View,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator
} from "react-native";
import { Toasts } from "../utilities/showToast";
import Icon from "react-native-vector-icons/FontAwesome";
import { getInventoryApiRequest, setSelectedInventory } from '../actions';
import { connect } from "react-redux"

let _this = null;

class InventoryList extends Component {
  state = {
    inventoryList: [],
    is_selected: false,
    loading: true,
  };
  static navigationOptions = ({ navigation }) => {
    return {
      title: "Inventory List",
      headerRight: (
        <TouchableOpacity onPress={() => _this.onDone()}>
          <Text style={{ color: "white", marginRight: 10, fontSize: 15 }}>
            Done
          </Text>
        </TouchableOpacity>
      )
    };
  };

  componentDidMount = () => {
    _this = this;
    const cloneInventoryList = JSON.parse(JSON.stringify(this.props.inventoryData));
    this.setState({
      inventoryList: cloneInventoryList,
      loading: false
    });
  };

  async onDone() {
    let inventoryArry = this.state.inventoryList.filter(item => item.is_selected);
    if (inventoryArry.length > 0) {
      await this.props.setSelectedInventory(inventoryArry, this.state.inventoryList)
      await this.props.navigation.state.params.getSelectedInventory();
      Toasts.showToast(`${inventoryArry.length} inventory added`, "success");
      this.props.navigation.pop();
      // this.props.navigation.pop();
    }
    else {
      Toasts.showToast("Please select at least one inventory");
    }
  }

  onItemSelect = item => {
    item.is_selected = !item.is_selected;
    this.setState({
      is_selected: !this.state.is_selected
    });
  };

  render() {
    return this.state.loading ? (
      <View
        style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
      >
        <Text> Loading...</Text>
        <ActivityIndicator size="large" color="purple" />
      </View>
    ) :
      (
        <View style={{ flex: 1, backgroundColor: "#F5F5F5" }}>
          <FlatList
            data={this.state.inventoryList}
            contentContainerStyle={{ marginTop: 10, paddingBottom: 20 }}
            ListEmptyComponent={() => <Text style={{ alignSelf: 'center' }}>No inventory found </Text>}
            extraData={this.state}
            keyExtractor={item => `${item.id}`}
            renderItem={({ item }) => {
              return (
                <TouchableOpacity
                  style={styles.listComponent}
                  onPress={() => {
                    this.onItemSelect(item);
                  }}
                >
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: item.is_selected ? "#696969" : "#000000"
                    }}
                  >
                    {item.title}
                  </Text>
                  <Icon
                    name={item.is_selected ? "check" : null}
                    size={15}
                    color="#696969"
                  />
                </TouchableOpacity>
              );
            }}
          />
        </View>
      );

  }
}
const mapStateToProp = state => {
  return {
    inventoryData: state.addTaskReducer.inventoryResponseData
  };
};

const styles = StyleSheet.create({
  titleStyle: {
    marginTop: 16,
    marginBottom: 10,
    fontSize: 20,
    fontWeight: "bold",
    marginLeft: 10
  },
  listComponent: {
    borderRadius: 10,
    marginRight: 12,
    marginLeft: 12,
    marginBottom: 5,
    backgroundColor: "white",
    padding: 15,
    justifyContent: "space-between",
    flexDirection: "row"
  }
});

export default connect(mapStateToProp, {
  getInventoryApiRequest,
  setSelectedInventory
})(InventoryList)
