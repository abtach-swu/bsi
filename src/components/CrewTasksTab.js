import React, { Component } from "react"
import { FlatList, View, } from "react-native"
import { Container, Text } from "native-base"
import { TaskCardItem } from "../reuseableComponent/TaskCardItem";
import { myTaskApiRequest } from "../actions";
import { Toasts } from "../utilities/showToast";
import { connect } from "react-redux";
import constant from "../HttpServiceManager/constant";
import moment from 'moment';
import styles from "../assets/styles";
// import { withNetworkConnectivity } from "react-native-offline";



class CrewTasksTab extends Component {
    state ={
        responseData : []
    }
    componentDidMount = () => {
        const { filterType ,statusType , today , upcoming , completed} = this.props;
        if(filterType === "today"){
            this.setState({
                responseData : today
            })
        }
        else if(filterType === "upcoming"){
            this.setState({
                responseData : upcoming
            })
        }
        else if(statusType === "completed"){
            this.setState({
                responseData : completed
            })
        }
    }
    
    

    componentDidUpdate() {
        if (this.props.error !== '') {
            Toasts.showToast(this.props.error)
        }
        
    }

    onItemSelected = (item) => {
        let navigation = this.props.navigation
        navigation.navigate("taskDetail",
            {
                'detail': item,
                'editable'  : false,
                completable : true,
                reload : this.props.reload,
            });
    }

    renderSeperator = () => {
        return (
            <View
                style={styles.dividerItem}
            />
        )
    }
    handleLoadMore = () => {
        if (!this.onEndReachedCalledDuringMomentum) {
            if (this.props.currentPage !== this.props.nextPage) {
                this.props.myTaskApiRequest(this.props.currentPage + 1);
            }
            this.onEndReachedCalledDuringMomentum = true;
        }
    }
    ListEmptyView = () => {
        return (
            <View style={styles.message}>
                <Text style={{ textAlign: 'center' }}>
                    {
                        this.props.refreshing ? 'Loading...' : 'No Record Found.'
                    }
                </Text>
            </View>
        );
    }
    componentWillReceiveProps(nextProps){
    const { filterType ,statusType , today , upcoming , completed} = nextProps;
        if(filterType === "today"){
            this.setState({
                responseData : today
            })
        }
        else if(filterType === "upcoming"){
            this.setState({
                responseData : upcoming
            })
        }
        else if(statusType === "completed"){
            this.setState({
                responseData : completed
            })
        }
    }


    render() {
       
        return (
            <Container style={{backgroundColor : 'rgb(245, 245, 245)'}}>
                <View>
                    <FlatList
                        contentContainerStyle={{margin : 10, paddingBottom : 20 }}
                        onRefresh={() => this.props.myTaskApiRequest(1, this.props.filterType, this.props.statusType, '',  this.props.target_id, true)}
                        refreshing={this.props.refreshing}
                        onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                        onEndReached={this.handleLoadMore}
                        onEndReachedThreshold={0.5}
                        ListEmptyComponent={this.ListEmptyView}
                        extraData={this.props}
                        data={this.state.responseData}
                        keyExtractor={(item, index) => `${item.id}`}
                        renderItem={({ item }) => (
                            <TaskCardItem
                                title={item.actor_name}
                                subtitle={item.job_number}
                                sub_subtitle={`${item.location.plant_title}/${item.location.unit_title}`}
                                detailDescription={item.description}
                                thumbnail={constant.imageBaseUrl + item.actor_image_url}
                                isCheck={item.status}
                                time={moment(moment(item.schedule_time, 'HH:mm')).format('h:mm a')}
                                onItemSelected={() => this.onItemSelected(item)}
                            />
                        )
                        }
                    />
                </View>
            </Container>
        )
    }

}
const mapStateToProp = state => {
    return {
        error: state.mytaskReducer.error,
        loading: state.mytaskReducer.loading,
        data: state.mytaskReducer.responseData,
        today: state.mytaskReducer.today,
        upcoming: state.mytaskReducer.upcoming,
        completed: state.mytaskReducer.completed,
        refreshing: state.mytaskReducer.refreshing,
        nextPage: state.mytaskReducer.nextPage,
        currentPage: state.mytaskReducer.currentPage
    };
};



export default connect(
    mapStateToProp,
    {
        myTaskApiRequest,
    }
)(CrewTasksTab);





