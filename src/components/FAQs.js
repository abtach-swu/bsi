import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Text,
  ActivityIndicator,
  FlatList,
  TouchableOpacity
} from "react-native";
import { Container, Content, Thumbnail, Title } from "native-base";
import { images } from "../assets";
import { connect } from "react-redux";
import { getFAQsRequest } from "../actions";
import Icon from "react-native-vector-icons/FontAwesome";

class FAQS extends Component {
  state = {
    display: false
  };

  componentDidMount() {
    this.props.getFAQsRequest();
  }

  render() {
    return this.props.loading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Thumbnail large source={images.app_logo} style={styles.logoStyle} />
        <Text> Loading...</Text>
        <ActivityIndicator size="large" color="purple" />
      </View>
    ) : (
      <Container style={{ backgroundColor: "#e6e6e6" }}>
        <Content>
            <View style={styles.topView}>
                <Thumbnail large source={images.app_logo} style={styles.logoStyle} />
            </View>
          <FlatList
            data={this.props.faqs}
            extraData={this.state}
            contentContainerStyle={{marginHorizontal : 20 , paddingBottom : 20}}
            renderItem={({ item }) => {
              return (
                <View style={{ backgroundColor: "white", marginTop: 10 }}>
                  <TouchableOpacity
                    style={{ paddingVertical: 20, paddingHorizontal: 20, flexDirection : 'row', justifyContent : "space-between" }}
                    onPress={() => {
                      item.isDisplay = !item.isDisplay;
                      this.setState({ display: !this.state.display });
                    }}
                  >
                    <Text>{item.question}</Text>
                    <Icon
                      name={item.isDisplay ? "chevron-up" : "chevron-down"}
                      size={15}
                      color="#696969"
                    />
                  </TouchableOpacity>
                  <View
                    style={{
                      display: item.isDisplay ? "flex" : "none",
                      paddingBottom: 10,
                      paddingHorizontal: 20
                    }}
                  >
                    <Text>{item.answer}</Text>
                  </View>
                </View>
              );
            }}
            keyExtractor={item => `${item.id}`}
          />
          
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  topView: {
    alignItems: "center"
  },
  logoStyle: {
    marginTop: 24
  }
});

const mapStateToProps = state => ({
  faqs: state.NotificationReducer.faqs,
  loading: state.NotificationReducer.loading
});

const mapDispatchToProps = {
  getFAQsRequest
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FAQS);
