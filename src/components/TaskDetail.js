import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Content,
  Text,
  Left,
  Body,
  Thumbnail,
  ListItem
} from "native-base";
import colorsUtil from "../utilities/colorsUtil";
import { NormalButton } from "../reuseableComponent";
import { images } from "../assets";
import { getUserData } from "../UserPreference";
import constant from "../HttpServiceManager/constant";
import moment from "moment";
import { onMarkTaskComplete, onClearReduxState , onOfflineMarkTaskComplete , onTaskEdit} from "../actions";
import { connect } from "react-redux";
import { Toasts } from "../utilities/showToast";


let _this = this
class TaskDetail extends Component {
  static navigationOptions = ({ navigation }) => {

    let isEditable = navigation.getParam("editable"); 
   
    return {
      title: "Details",
      headerRight: isEditable? (
        <TouchableOpacity
          onPress={() => { _this.onEditPressed(isEditable) }}
        >
          <Image
            source={images.edit_icon}
            size={10}
            style={{
              marginRight: 16,
              height: 27,
              width: 27,
              resizeMode: "contain"
            }}
          />
        </TouchableOpacity>
      ) : null
    };
  };

  state = {
    taskDetail: {},
    task_inventory : [],
    port: [],
    location: {},
    detailDescription: "",
    loggedinUserId: "",
    taskStatus: 0,
    markAsComplt: false,
    scheduleTime: "",
    calculate: false,
    completable : this.props.navigation.getParam("completable")
  };
  componentDidMount = () => {
    _this = this
    let navigation = this.props.navigation;
    let detail = navigation.getParam("detail"); //detail
    let completable = navigation.getParam("completable"); //markAsCompt
    const cloneInventory = JSON.parse(JSON.stringify(detail.inventory))
    this.props.onClearReduxState();
    
    getUserData().then(loggedinUser => {
      let markAsComplt = ((loggedinUser.id != detail.actor_id) || (this.state.completable && detail.actor_type === "supervisor"));
      this.setState({
        taskDetail: detail,
        task_inventory : cloneInventory,
        port: detail.port,
        scheduleTime: `${detail.schedule_date} ${detail.schedule_time}`,
        location: detail.location,
        loggedinUserId: loggedinUser.id,
        taskStatus: detail.status,
        markAsComplt: markAsComplt,
      });
    });
  };

  onEditPressed = (isEditable) => {
        this.props.onTaskEdit(this.state.taskDetail)
        this.props.navigation.navigate("taskAdd" , { editable : isEditable , reload :  this.props.navigation.state.params.reload} ) //editProfileStack
  }

  _markAsComplt = () => {
    if(this.state.calculate){
    let portData = this.props.selected_ports //detail
    let portDataObj = {
      task_id : this.state.taskDetail.id,
      target_id : this.state.taskDetail.target_id,
      port_data : JSON.stringify(portData)
    }
    if (portData.length > 0) {
      if(this.props.isConnected){
        this.props.onMarkTaskComplete(portDataObj);
      }else {
        let requestedAt = new Date().toLocaleDateString()
        portDataObj.requested_at = requestedAt
        this.props.onOfflineMarkTaskComplete(portDataObj)
      }
    } else {
      Toasts.showToast("Inventory Report Required");
    }
  }
  else {
    Toasts.showToast("Please calculate the inventory or used quantity is greater");
  }

  };

  _onViewMapPressed = item => {
    let location = {
      plant_title: this.state.location.plant_title,
      unit_title: this.state.location.unit_title,
      floor_title: item.floor_title
    };

    this.props.navigation.navigate("PortMap", {
      item,
      detail: this.props.navigation.getParam("detail"),
      location,
      isMapView: true,
      markAsComplt: this.state.taskStatus == 0 && this.state.markAsComplt,
      _calcStatus : this.calcStatus
    });
  };

 
  componentDidUpdate() {
    const {isConnected , error, message , navigation ,offlineMessage} = this.props
    if(!isConnected){
      if(offlineMessage !== ""){
        Toasts.showToast(offlineMessage, "success");
        navigation.state.params.reload();
        navigation.pop();
      }
    }
    else {
      if (error !== "") {
        Toasts.showToast(error);
      }
      else if(message !== "") {
        Toasts.showToast(message, "success");
        navigation.state.params.reload();
        navigation.pop();
      }
    }
  }

  calcStatus = () => {
    this.setState({
      calculate : false
    })
  }


  calcPortdata = () => {
    let calcArr = []
    this.props.selected_ports.forEach((item, index) => {
        item.data.forEach((data , index) => {
            data.inventory.forEach((inv , index) => {
              if (calcArr.length === 0){
                calcArr.push({id:inv.id, total_qty: inv.used_qty})
              }else{
                if (calcArr.findIndex(item => item.id === inv.id ) !== -1){
                  calcArr[calcArr.findIndex(item => item.id === inv.id )].total_qty += inv.used_qty
                }else{
                  calcArr.push({id:inv.id, total_qty: inv.used_qty})
                }
              }
            })          
        })
    })
    let slectdPorts = this.props.selected_ports
    if(slectdPorts.length !== 0) {
      
      this.state.task_inventory.map( (item , index) => {
        item.used_qty = calcArr[index].total_qty
      })
      this.setState({
        task_inventory : this.state.task_inventory,
        calculate : true
      } ,()=> {
      let valid = true;
      for (var i = 0 ; i < calcArr.length ; i++){
      if(this.state.task_inventory[i].used_qty > this.state.task_inventory[i].total_qty){
        valid = false
        break;
        }
      }
      if(!valid){
        Toasts.showToast("Inventory used Quantity is greater")
        this.setState({
          calculate : false
        })
      }
      })
    }
    else {
      Toasts.showToast("Please fill Inventory");
    }
  }



  
  render() {
    return this.state.port.length == 0 ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text> Loading...</Text>
        <ActivityIndicator size="large" color="purple" />
      </View>
    ) : (
      <Container>
        <Content contentContainerStyle={styles.topView}>
          <ListItem avatar noBorder>
            <Left>
              <View style={styles.shadowStyle}>

              <Image
                large
                style={styles.profileImageStyle}
                source={{
                  uri:
                  constant.imageBaseUrl +
                  this.state.taskDetail.target_image_url
                }}
                />
                </View>
            </Left>
            <Body style={{marginTop : 15 }}>
              <Text style={{ fontSize: 17, marginBottom : 4 }}>
                {this.state.taskDetail.target_name}
              </Text>
              <Text note style={{fontSize : 13}}>Assigned by : {this.state.taskDetail.actor_name}</Text>
            </Body>
          </ListItem>
          <View style={styles.bottomLine} />
          <View>
            <Text style={styles.titleStyle}>Schedule</Text>
          </View>

          <View style={{ flexDirection: "row" }}>
            <Image
              small
              source={images.clock_gray}
              style={styles.clock_icon_style}
            />
            <Text style={{ marginLeft: 15 }}>
              Assignment Date {moment(this.state.taskDetail.created_at).format("DD MMM")}
            </Text>
          </View>
          <View
            style={{
              borderColor: "#707070",
              borderWidth: 1,
              marginLeft: 10,
              width: 1,
              height: 30
            }}
          />
          <View style={{ flexDirection: "row" }}>
            <Image
              small
              source={images.clock_red}
              style={styles.clock_icon_style}
            />
            <Text style={{ marginLeft: 15, color: colorsUtil.red }}>
              Execution Date {moment(new Date(this.state.taskDetail.schedule_date)).format("DD MMM")} {moment(moment(this.state.taskDetail.schedule_time, 'HH:mm')).format('h:mm a')}
            </Text>
          </View>
          <View style={styles.bottomLine} />          
          <View>
            <Text style={styles.titleStyle}>Location</Text>
          
            <FlatList
              data={this.state.location.floor}
              extraData={this.state}
              keyExtractor={(item, index) => `${item.floor_id}`}
              renderItem={({ item }) => {
                return (
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "space-between"
                    }}
                  >
                    <View
                      style={{
                        flexDirection: "row"
                      }}
                    >
                      <Image
                        source={images.location}
                        style={{ width: 10, height: 12, marginRight: 3 }}
                      />
                      <Text note>{` ${this.state.location.plant_title}/${
                        this.state.location.unit_title
                      }/${item.floor_title}`}</Text>
                    </View>
                    <TouchableOpacity
                      style={{
                        borderWidth: 1,
                        borderColor: colorsUtil.buttonColorPurpule,
                        padding: 6,
                        borderRadius: 4,
                        marginBottom: 4
                      }}
                      onPress={() => this._onViewMapPressed(item)}
                    >
                      <Text
                        style={{
                          color: colorsUtil.textColorPurpule,
                          fontSize: 12
                        }}
                      >
                        View Map
                      </Text>
                    </TouchableOpacity>
                  </View>
                );
              }}
            />
          </View>
          <View style={styles.bottomLine} /> 
          <View style={{
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "space-between"
                    }}>
            <Text style={styles.titleStyle}>Inventory</Text>
            {(this.state.markAsComplt && this.state.taskStatus == 0) ?
            (
            <TouchableOpacity
                      style={{
                        borderWidth: 1,
                        borderColor: colorsUtil.buttonColorPurpule,
                        padding: 6,
                        borderRadius: 4,
                        marginBottom: 2
                      }}
                      onPress={() => this.calcPortdata()}
                    >
                      <Text
                        style={{
                          color: colorsUtil.textColorPurpule,
                          fontSize: 12
                        }}
                      >
                        Calculate
                      </Text>
                    </TouchableOpacity>
            )
                    :
                    null
                      }
          </View>

          <FlatList
            data={this.state.task_inventory}
            extraData={this.state}
            renderItem={({ item }) => (
              <View style={{ flexDirection: "row", marginTop: 8, justifyContent : 'space-between', flex : 1}}>
                <Text note style={{flex : 2}}>
                  {item.title}
                </Text>
                <Text note style={{flex : 2}}>
                  {item.name}
                </Text>
                <View style={{flex : 2 , alignItems : 'flex-end'}}>
                  <Text note> 
                    {item.used_qty}/
                    <Text note style={{ color: colorsUtil.red }}>
                      {item.total_qty}
                    </Text>
                  </Text>
                </View>
              </View>
            )}
            keyExtractor={(item, index) => index + "-" + item.id}
          />
          <View>
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between"
              }}
            >
              <Text style={styles.titleStyle}>Job Number</Text>
            </View>
            <View>
              <Text note>{this.state.taskDetail.job_number}</Text>
            </View>
          </View>

          <View>
            <Text style={styles.titleStyle}>Description</Text>
          </View>
          <Text note> {this.state.taskDetail.description}</Text>

          <View style={{ marginTop: 30 }} />
          {this.state.taskStatus == 0 ? (
            <NormalButton
              btnBgColor={
                this.state.markAsComplt ? colorsUtil.purple : colorsUtil.gray
              }
              showImage = {this.state.markAsComplt}
              btnImage = {images.done}
              textColor={colorsUtil.textColorWhite}
              text={this.state.markAsComplt ? "Mark as Complete" : "Pending..."}
              btnSyle={{ marginBottom: 15, height: 50 }}
              onClick={() => {
                this.state.markAsComplt
                  ? this._markAsComplt()
                  : this.props.navigation.pop();
              }}
            />
          ) : (
            <NormalButton
              showImage = {true}
              btnImage = {images.done2_icon}
              btnBgColor={'#2eb82e'}
              textColor={colorsUtil.textColorWhite}
              text={"Completed"}
              btnSyle={{ marginBottom: 15, height: 50 }}
              onClick={() => this.props.navigation.pop()}
            />
          )}
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    message: state.InventoryReducer.message,
    offlineMessage : state.mytaskReducer.message,
    error: state.InventoryReducer.error,
    port_data: state.InventoryReducer.port_data,
    selected_ports: state.InventoryReducer.selected_ports,
    isConnected : state.authReducer.isConnected

  };
};

const styles = StyleSheet.create({
  topView: {
    justifyContent: "space-around",
    paddingLeft: 20,
    paddingRight: 20
  },
  profileImageStyle: {
    borderColor: colorsUtil.borderColor,
    borderWidth: 2,
    width: 60,
    height: 60,
    borderRadius : 30
  },
  shadowStyle : {
    borderRadius: 30,

    backgroundColor : 'white',
    elevation: 1,
    shadowOpacity: 0.3,
    shadowRadius: 4,
    shadowOffset: {
      height: 0,
      width: 0
    }
  },
  titleStyle: {
    marginTop: 16,
    marginBottom: 10,
    fontSize: 20,
    fontWeight: "bold"
  },

  location_icon_style: {
    width: 20,
    height: 20,
    resizeMode: "contain"
  },

  clock_icon_style: {
    width: 20,
    height: 20,
    resizeMode: "contain"
  },
  bottomLine: {
    borderBottomWidth: 1,
    borderBottomColor: "#BFBFBF",
    marginTop: 15
  },
});

export default connect(
  mapStateToProps,
  {
    onMarkTaskComplete,
    onClearReduxState,
    onOfflineMarkTaskComplete,
    onTaskEdit
  }
)(TaskDetail);
