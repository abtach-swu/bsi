import React, { Component } from 'react';
import {
  View, TouchableHighlight, FlatList, Image} from 'react-native';
import { ListItem,Text, Left, Right, Container, Icon } from 'native-base';

import colorsUtil from "../utilities/colorsUtil";
import {NormalButton} from "../reuseableComponent";
import { Toasts } from '../utilities/showToast';
import { connect } from "react-redux";
import { onSelectPort, isModalVisible } from '../actions';



class SelectPorts extends Component {

  state = {
    dataList: [],
    isSelected:false
  }
  
  
  onItemPressed = (item)=>{
    item.isSelected = !item.isSelected
    this.setState({
      isSelected : item.isSelected
    })

  }


  onSelectButton =()=>{
  
    let items = this.state.dataList
    let selectedItems = items.filter(it=>it.isSelected === true).map(it=>it.id)

    if(selectedItems.length<1){
      Toasts.showToast("Please select at least one floor")
    } 
    else {
      let selectedItemsId =  selectedItems.toString();
      console.warn(selectedItemsId);
      this.props.onSelectPort(selectedItemsId);
    }
    console.warn(selectedItems);
  }

  componentDidMount() {
      this.setState({
          dataList : this.props.portData
      })
  }
  render() {
    return (
      <Container >
        <View style={{flex : 1}}>
           <Text>Select Ports</Text>
          <FlatList
            style={{ height: '80%' }}
            data={this.state.dataList}
            extraData={this.state}
            renderItem={
              ({ item }) =>
                <ListItem onPress={()=>this.onItemPressed(item)}>
                  <Left>
                    <Text>{item.title}</Text>
                  </Left>
                  <Right>
                  <Icon name={item.isSelected ?"checkmark":""} style={{color: colorsUtil.greenColor}}/>
                  </Right>
                </ListItem>
            } />

            <NormalButton
              btnStyle={{ height: 60, margin: 20 }}
              text="Select"
              textColor={colorsUtil.white}
              onClick={() => this.onSelectButton()}
            />
        </View>

      </Container>
    );
  }
  
}

const mapStateToProp = (state) => {
  return {
    portData: state.addTaskReducer.portResponseData,
    selectedPort: state.addTaskReducer.selectedPort,
  }
}


export default connect(
  mapStateToProp,
  {
    onSelectPort,
    isModalVisible
  }
)(SelectPorts);


