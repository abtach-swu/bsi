import React, { Component } from 'react';
import { Image , Text} from 'react-native';
import {
  Container,
  View,
} from 'native-base';
import { images } from "../assets";
import { getUserData } from '../UserPreference'
import { StackActions, NavigationActions } from 'react-navigation';
import { SpinnerView } from "../utilities/common";
import colorsUtil from '../utilities/colorsUtil';


export default class Authentication extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loadng: true
    }
    // this.checkIsLogin();
    // this.props.getNotificationList()
    this.checkIsLogin();
  }

  checkIsLogin = () => {
    getUserData().then((response) => {
      this.setState({
        loadng : false
      })
      if (response !== undefined && response !== null) {
        this._navigateTo('drawerState');
      } else {
        this._navigateTo('loginStack');
      }
    });
    
  }

  _navigateTo = (rounts) => {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: rounts }),
      ],
    });
    this.props.navigation.dispatch(resetAction);
  }
  render() {
  
    return (
      <Container style={{ backgroundColor: colorsUtil.white }}>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Image source={images.app_logo} style={{
            width: 120,
            height: 120,
          }} />
        </View>
        {this.state.loadng && <SpinnerView />}
      </Container>
    );

  }
}
