import React, { Component } from "react";
import {
  View,
  Text,
  FlatList,
  ActivityIndicator,
  TouchableOpacity,
  Image
} from "react-native";
import { Container } from "native-base";
import { connect } from "react-redux";
import { images } from "../assets";
import { MessageCardItem } from "../reuseableComponent";
import { getMessages ,_onMessageRead } from "../actions";
import moment from "moment";
import Modal from "react-native-modal";
import constant from "../HttpServiceManager/constant";
import colorsUtil from "../utilities/colorsUtil";



class InboxMessage extends Component {
  state = {
    messageData: [],
    isLoading: true,
    isRead: false,
    isModalVisible: false,
    item: {},
   
  };
  componentDidMount = () => {
    this.props.getMessages("inbox");
  };
  componentWillReceiveProps(nextProps) {
    if(nextProps.error !== ""){
      alert(nextProps.error)
      this.setState({
        isLoading : false,
        messageData : []
      })
    }
    else {
      this.setState({
        messageData: nextProps.messagesData,
        isLoading: false
      });
    }
  }
  _toggleModal = () => {
    this.setState({
      isModalVisible: !this.state.isModalVisible
    });
  };

  _onItemPressed = item => {
    if(item.is_read === 0) {
      this.props._onMessageRead(item.id)
      item.is_read = 1;
    }
    this.setState(
      {
        isRead: !this.state.isRead,
        item
      },
      () => {
        this._toggleModal();
      }
    );
  };

  render() {
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text> Loading...</Text>
        <ActivityIndicator size="large" color="purple" />
      </View>
    ) : (
      <Container>
        <FlatList
          contentContainerStyle={{ padding: 10 }}
          data={this.state.messageData}
          extraData={this.state}
          renderItem={({ item }) => {
            return (
              <View>
                <MessageCardItem
                  _isHide={item.is_read == 0 ? false : true}
                  _shadowStyle={styles.shadowStyle}
                  _imageSource={{ uri:constant.imageBaseUrl + item.sender.image_url}}
                  _imageStyle={styles.imageStyle}
                  _titleTextStyle={{ fontWeight: "bold", marginBottom: 5, color : 'black' }}
                  _messageTextStyle={{ color: "grey" }}
                  _titleText={`${item.sender.first_name} ${item.sender.last_name}`}
                  _MessageText={item.message}
                  _created_at={moment(item.created_at).format("DD-MMM")}
                  _onPressed={() => this._onItemPressed(item)}
                />
              </View>
            );
          }}
          ListEmptyComponent={() => {
            return <Text style={{alignSelf : 'center'}}> Empty !</Text>;
          }}
          ItemSeparatorComponent={() => {
            return <View style={styles.bottomLine} />;
          }}
          onRefresh={() => this.props.getMessages("inbox")}
          refreshing={this.state.isLoading}
          keyExtractor={(item, index) => `${item.id}`}
        />

        <Modal isVisible={this.state.isModalVisible} onBackdropPress={this._toggleModal}>
          <View style={styles.modalContent}>
            <View
              style={{
                justifyContent: "space-between",
                flexDirection: "row",
                backgroundColor: colorsUtil.purple,
                padding: 5,
              }}
            >
              <Text style={{ fontSize: 20 , color : 'white' }}>
                {this.state.isModalVisible
                  ? `${this.state.item.sender.first_name} ${
                      this.state.item.sender.last_name
                    }`
                  : ""}
              </Text>
              <TouchableOpacity onPress={this._toggleModal}>
                <Image
                  source={images.cancel_grey}
                  style={{ width: 15, height: 15 }}
                />
              </TouchableOpacity>
            </View>
            <Text
              style={{
                backgroundColor: "white",
                padding: 10,
                color : 'black'
              }}
            >
              {this.state.item.message}
            </Text>
          </View>
        </Modal>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    messagesData: state.MessageReducer.messageData,
    error: state.MessageReducer.error,
  };
};

const mapDispatchToProps = {
  getMessages,
  _onMessageRead
};

const styles = {
  imageStyle: {
    width: 40,
    height: 40,
    borderRadius: 20,
    
  },
  shadowStyle : {
    marginRight : 10,
    borderRadius: 30,
    backgroundColor : 'white',
    elevation: 1,
    shadowOpacity: 0.3,
    shadowRadius: 4,
    shadowOffset: {
      height: 0,
      width: 0
    }
  },
  bottomLine: {
    borderBottomWidth: 1,
    borderBottomColor: "#BFBFBF",
    marginLeft: 55,
    marginRight: 5,
    marginTop: 5
  },
  modalContent: {
    backgroundColor: "white",
    padding: 10,
    borderRadius: 4,
    // borderColor: "rgba(0, 0, 0, 0.1)"
    backgroundColor: 'rgba(52, 52, 52, 0.0)'
  }
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(InboxMessage);
