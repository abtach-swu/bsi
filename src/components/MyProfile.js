import React, { Component } from "react"
import { View, StyleSheet, Image, TouchableOpacity, StatusBar, AppState, Alert } from "react-native"
import { Container, Content, Text, Thumbnail } from "native-base";
import colorsUtil from "../utilities/colorsUtil";
import { images } from "../assets";
import { getUserData, getUserToken } from "../UserPreference";
import constant from "../HttpServiceManager/constant";
import { connect } from "react-redux";


let _this = null
class MyProfile extends Component {

    static navigationOptions = ({ navigation }) => {

        return {
            title: 'Profile',
            headerRight:
                <TouchableOpacity onPress={() => _this.onEditProfile()}>
                    <Image source={images.edit_icon}
                        size={10}
                        style={{ marginRight: 16, height: 27, width: 27, resizeMode: "contain" }}
                    />
                </TouchableOpacity>
        }

    };

    //State
    state = {
        userData: {}
    }

    //Life Cycle  Method
    componentDidMount() {
        _this = this
        getUserData().then(response => {
            this.setState({
                userData: response
            })
        })
    }

    _userUpdated = () => {
        getUserData().then(response => {
            this.setState({
                userData: response
            })
        })
        this.props.navigation.state.params.userUpdated()
    }

    onEditProfile = () => {
        if (this.props.isConnected) {
            this.props.navigation.navigate("editProfile", { userUpdated: this._userUpdated })
        } else {
            Alert.alert('Something went wrong', 'Kindly check if the device is connected to stable cellular data plan or WiFi.');
        }
    }


    render() {
        let userImage = constant.imageBaseUrl + this.state.userData.image_url
        return (
            <Container>
                <Content contentContainerStyle={styles.topView}>
                    <Thumbnail large style={styles.profileImageStyle} source={{ uri: userImage }} />

                    <Text style={styles.textPurple}>Name</Text>
                    <Text style={{ fontWeight: "bold" }}>{this.state.userData.name}</Text>

                    <Text style={styles.textPurple}>Email</Text>
                    <Text>{this.state.userData.email}</Text>

                    <Text style={styles.textPurple}>User Type</Text>
                    <Text>{this.state.userData.user_type}</Text>

                </Content>
            </Container>
        )
    }
}
const styles = StyleSheet.create({
    topView: {
        justifyContent: 'center',
        flex: 1,
        alignItems: "center",
        padding: 16
    },
    textPurple: {
        marginTop: 24,
        color: colorsUtil.textColorPurpule
    }
    ,
    profileImageStyle: {
        borderColor: colorsUtil.borderColor,
        borderWidth: 5,
        height: 100,
        width: 100,
        borderRadius: 50
    }
})

const mapStateToProps = (state) => ({
    isConnected: state.authReducer.isConnected
})

export default connect(
    mapStateToProps,
    {}
)(MyProfile);