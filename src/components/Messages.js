import React, { Component } from "react";
import { Container, Button, Segment, Text , Thumbnail} from "native-base";
import { TouchableOpacity, Alert} from "react-native";
import { images } from "../assets";
import { getMessages , _onTabSelection } from "../actions";
import { connect } from "react-redux";
import InboxMessage from "./InboxMessage";
import SendMessage from "./SendMessage";

class Messages extends Component {
  state = {
    selectedSegmentId: 1
  };
  setSelectedTab(index) {
    this.props._onTabSelection()
    this.setState({
      selectedSegmentId: index
    });
  }
  _createMessage = () => {
    if(this.props.isConnected){
      this.props.navigation.navigate("CreateMessage", { onReload : this._onReload});
    }else {
      Alert.alert('Something went wrong', 'Kindly check if the device is connected to stable cellular data plan or WiFi.');
    }
  };

  _onReload = () => {
    if(this.state.selectedSegmentId == 1 ){
      this.props.getMessages("inbox");
    }
    else {
      this.props.getMessages("sent");
    }
  }
  render() {
    return (
      <Container>
        <Segment>
          <Button
            first
            active={this.state.selectedSegmentId == 1 ? true : false}
            onPress={() => {
              this.setSelectedTab(1);
            }}
          >
            <Text>Inbox</Text>
          </Button>
          <Button
            last
            active={this.state.selectedSegmentId == 2 ? true : false}
            onPress={() => {
              this.setSelectedTab(2);
            }}
          >
            <Text>Send</Text>
          </Button>
        </Segment>
        <Container>
          {this.state.selectedSegmentId === 1 ? (
            <InboxMessage navigation={this.props.navigation} />
          ) : (
            <SendMessage navigation={this.props.navigation} />
          )}
          <TouchableOpacity
            onPress={this._createMessage}
            style={styles.createMsg}
          >
            <Thumbnail
              source={images.add_task_fb}
              style={{ width: 80, height: 80 }}
            />
          </TouchableOpacity>
        </Container>
      </Container>
    );
  }
}
const styles = {
  createMsg: {
    position: "absolute",
    bottom: 16,
    right: 16
  }
};
const mapStateToProps = state => {
  return {
    isConnected : state.authReducer.isConnected
  };
};

// export default connect({} , getMessages)(Messages)
export default connect(
  mapStateToProps,
  {getMessages,_onTabSelection}
)(Messages);

