import React, { Component } from "react"
import { FlatList, View, } from "react-native"
import { Container, Content, Text } from "native-base"
import { images } from "../assets";
import { TaskCardItem } from "../reuseableComponent/TaskCardItem";
import { myTaskApiRequest } from "../actions";
import { Toasts } from "../utilities/showToast";
import { connect } from "react-redux";
import constant from "../HttpServiceManager/constant";
import styles from "../assets/styles";
import moment from 'moment';

class SupervisorTasksTab extends Component {

   
   
    componentDidUpdate() {
        if (this.props.error !== '') {
            Toasts.showToast(this.props.error)
        } 
    }

    onItemSelected = (item) => {
        this.props.navigation.navigate("taskDetail",
            {
                'detail': item,
                'editable'  : true,
                completable : false,
                reload : this.props.reload,
            });
    }

    renderSeperator = () => {
        return (
            <View
                style={styles.dividerItem}
            />
        )
    }
    handleLoadMore = () => {
        if (!this.onEndReachedCalledDuringMomentum) {
            if (this.props.currentPage !== this.props.nextPage) {
                this.props.myTaskApiRequest(this.props.currentPage + 1);
            }
            this.onEndReachedCalledDuringMomentum = true;
        }
    }
    ListEmptyView = () => {
        return (
            <View style={styles.message}>
                <Text style={{ textAlign: 'center' }}>
                    {
                        this.props.refreshing ? 'Loading...' : 'No Record Found.'
                    }
                </Text>
            </View>
        );
    }


    render() {
        return (

            <Container style={{backgroundColor : 'rgb(245, 245, 245)'}}>
                <View>
                    <FlatList
                        contentContainerStyle={{margin : 10, paddingBottom : 20 }}
                        onRefresh={() => this.props.myTaskApiRequest(1, this.props.filterType, this.props.statusType, this.props.actor_id, '', true)}
                        refreshing={this.props.refreshing}
                        onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                        onEndReached={this.handleLoadMore}
                        onEndReachedThreshold={0.5}
                        ListEmptyComponent={this.ListEmptyView}
                        keyExtractor={(item, index) => `${item.id}`}
                        extraData={this.props.data}
                        data={this.props.data}
                        renderItem={({ item }) => (
                            <TaskCardItem
                                title={item.target_name}
                                subtitle={item.job_number}
                                sub_subtitle={`${item.location.plant_title}/${item.location.unit_title}`}
                                detailDescription={item.description}
                                thumbnail={constant.imageBaseUrl + item.target_image_url}
                                isCheck={item.status}
                                time={moment(moment(item.schedule_time, 'HH:mm')).format('h:mm a')}
                                onItemSelected={() => this.onItemSelected(item)}
                            />
                        )
                        }
                    />
                </View>
            </Container>
        )
    }

}
const mapStateToProp = state => {
    return {
        error: state.mytaskReducer.error,
        loading: state.mytaskReducer.loading,
        data: state.mytaskReducer.responseData,
        refreshing: state.mytaskReducer.refreshing,
        nextPage: state.mytaskReducer.nextPage,
        currentPage: state.mytaskReducer.currentPage
    };
};

export default connect(
    mapStateToProp,
    {
        myTaskApiRequest,
    }
)(SupervisorTasksTab);



