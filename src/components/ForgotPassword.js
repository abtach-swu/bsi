import React, { Component } from "react"
import {  StyleSheet, Text, View } from 'react-native';
import { Container, Content, Form, Thumbnail } from "native-base"
import { images } from "../assets";
import colorsUtil from "../utilities/colorsUtil";
import { InputField, NormalButton, FloatingLabelInput } from "../reuseableComponent";
import { isEmailValid } from "../utilities/validations";
import { Toasts } from "../utilities/showToast";
import { VALIDEMAIL } from "../constants/Constants";
import { connect } from "react-redux";
import { forgotPassword, emailChanged } from "../actions";
import { SpinnerView } from "../utilities/common";

class ForgotPassword extends Component {

    forgotPasswrords = () => {
        const email = this.props.email
        if (!isEmailValid(email)) {
            Toasts.showToast(VALIDEMAIL)
        } else {
            this.props.forgotPassword(email, true)
        }
    }
    componentDidUpdate() {
        if (this.props.error !== '') {
            Toasts.showToast(this.props.error)
        }
        if (this.props.message !== '') {
            Toasts.showToast(this.props.message, "success")
        }

    }

    render() {
        return (
            <Container>
                <Content contentContainerStyle={styles.topView}>
                    
                    <Thumbnail source={images.app_logo} style={{ marginBottom: 30, width: 150, height: 150 }} />
                    <Text style={styles.forgotPassword}>Forgot Password ?</Text>
                    <Form>
                        <FloatingLabelInput
                            style={{ width: 280, height: 50 }}
                            label="Email"
                            value={this.props.email}
                            //onChangeText={(text) => this._onChangeText(text, 'email')}
                            onChangeText={(text) => this.props.emailChanged(text)}
                            inactiveColor={colorsUtil.gray}
                            activeColor={colorsUtil.tabSelectedTextColor}
                            labelBackgroundColor={colorsUtil.white}
                            labelStyle={{ color: colorsUtil.textColorPurpule }}
                        />
                    </Form>
                    <View style={{ alignItems: "center" }}>
                        <NormalButton
                            btnSyle={{ width: 280, height: 50, marginTop: 25 }}
                            text="Submit" textColor={colorsUtil.white} onClick={() => this.forgotPasswrords()}
                        />
                    </View>
                    {/* <NormalButton text="Submit" onClick={()=>this.forgotPasswrord()}/> */}
                </Content>
                {this.props.loading && <SpinnerView />}
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    topView: {
        justifyContent: 'center',
        flex: 1,
        alignItems: "center",
        padding: 16,
    },
    forgotPassword: {
        marginBottom: 50,
        fontSize: 17,
        margin: 16,
        color: colorsUtil.textColorPurpule
    }
})

const mapStateToProp = (state) => {
    return {
        email: state.authReducer.email,
        password: state.authReducer.password,
        error: state.authReducer.error,
        loading: state.authReducer.loading,
        user: state.authReducer.user,
        message: state.authReducer.message
    };
};
export default connect(mapStateToProp,
    {
        emailChanged,
        forgotPassword,
    }
)(ForgotPassword);



