import React, { Component } from "react";
import { Container, Button, Segment, Text, Item } from "native-base";
import { StatusBar, Alert, ActivityIndicator, AppState, NativeModules } from "react-native";
import HomeScreen from "./HomeScreen";
import SupervisorHome from "./SupervisorHome";
import colorsUtil from "../utilities/colorsUtil";
import { NetInfo } from "react-native";
import {
  _setNetInfo,
  onMarkTaskComplete,
  _onClearPandingTask,
  _getTaskDetail,
  myTaskApiRequest
} from "../actions";
import { connect } from "react-redux";
import firebase, {
  Notification,
  NotificationOpen,
  RemoteMessage
} from "react-native-firebase";
import FirebaseNotification from "../utilities/FirebaseNotification";

const ShortcutBadger = NativeModules.ShortcutBadger;
class SegmentOutsideHeaderExample extends Component {
  constructor(props) {
    super(props);
    this.state = {
      appState: AppState.currentState,
      selectedSegmentId: 1,
      isConnected: true,
      loading: false,
      target_id: 0,
      notification_count: 0
    };
  }
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    return params;
  };

  componentDidMount() {
    FirebaseNotification.notificationPermission();


    NetInfo.addEventListener("connectionChange", this.handleConnectionChange);

    //ios Notification
    AppState.addEventListener("change", this._handleAppStateChange);

    this._initialNotification();
    this._openNotification();

  }

  getAllNotification = () => {
    firebase
      .notifications()
      .getBadge()
      .then(count => {
        console.log("count getBadge() ********** ", count);
        this.props.navigation.setParams({ count: count });
      });
    this.messageListener = firebase.messaging().onMessage((message: RemoteMessage) => {
      console.log("onMessage : ", message);
      // Process your message as required
    });
    firebase
      .notifications()
      .onNotificationDisplayed((notification: Notification) => {
        console.log("notificationDisplayedListener : ", notification);
        // Process your notification as required
        // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
      });

    firebase.notifications().onNotification((notification: Notification) => {
      console.log("onNotification  app listner : ", notification);
      if (notification && notification._data["gcm.notification.user_badge"]) {
        const count = +notification._data["gcm.notification.user_badge"];
        firebase.notifications().setBadge(count);
        console.log("gcm.notification.user_badge if working ****", count);
      } else {
        const count = +notification._data["user_badge"];
        firebase.notifications().setBadge(count);
      }
      // Process your notification as required
    });

    firebase.messaging().onMessage((message: RemoteMessage) => {
      // Process your message as required
      console.log("messageListener ***** ", message);
      // const notificationData = message._data;
      // if(message && message._data.user){
      // const user = message._data.user;
      // this.props.navigation.navigate("otherProfile", {profile:user});
      // }
    });
  }
  _initialNotification = () => {
    firebase
      .notifications()
      .getInitialNotification()
      .then((notificationOpen: NotificationOpen) => {
        // console.log("getInitialNotification  ShortcutBadger *****: ", ShortcutBadger);

        // console.log("getInitialNotification : ", notificationOpen);
        // firebase.notifications().setBadge(20);
        firebase
          .notifications()
          .getBadge()
          .then(count => {
            // console.log("count getBadgeNumber() check karou  ********** ", count);
            this.props.navigation.setParams({ count: count });
          });
        if (notificationOpen) {
          // console.log("getInitialNotification  ****** ", notificationOpen);
          // App was opened by a notification
          // App was closed
          // Get the action triggered by the notification being opened
          const action = notificationOpen.action;
          // Get information about the notification that was opened
          const notification: Notification = notificationOpen.notification;

          const notificationData = notification._data;
          // console.log("data ***** ", notification._data);
          //For android
          if (notification && notificationData.custom_data) {
            const task_id = JSON.parse(notificationData.custom_data).id;
            this.setState({
              target_id: JSON.parse(notificationData.custom_data).target_id
            })

            if (notificationData.identifier === "add_task") {
              this.props._getTaskDetail(task_id);
              this.setState({ loading: true });
            } else if (notificationData.identifier === "mark_as_complete") {
              this.props._getTaskDetail(task_id);
              this.setState({
                selectedSegmentId: 2,
                loading: true
              });
            } else if (notificationData.identifier === "update_task") {
              this.props._getTaskDetail(task_id);
              this.setState({ loading: true });
            } else if (notificationData.identifier === "add_message") {
              this.props.navigation.navigate("Messages");
            }
          }
          //For ios


          else {
            const task_id = JSON.parse(
              notificationData["gcm.notification.custom_data"]
            ).id;

            this.setState({
              target_id: JSON.parse(notificationData["gcm.notification.custom_data"]).target_id
            })

            if (
              notificationData["gcm.notification.identifier"] === "add_task"
            ) {
              this.props._getTaskDetail(task_id);
              this.setState({ loading: true });
            } else if (
              notificationData["gcm.notification.identifier"] ===
              "mark_as_complete"
            ) {
              this.props._getTaskDetail(task_id);
              this.setState({
                selectedSegmentId: 2,
                loading: true
              });
            } else if (
              notificationData["gcm.notification.identifier"] === "update_task"
            ) {
              this.props._getTaskDetail(task_id);
              this.setState({ loading: true });
            } else if (
              notificationData["gcm.notification.identifier"] === "add_message"
            ) {
              this.props.navigation.navigate("Messages");
            }
          }
        }
      });
  };
  _openNotification = () => {
    firebase
      .notifications()
      .onNotificationOpened((notificationOpen: NotificationOpen) => {
        // console.log("onNotificationOpened : ");
        firebase
          .notifications()
          .getBadge()
          .then(count => {
            // console.log("count getBadgeNumber() ********** ", count);
            this.props.navigation.setParams({ count: count });
          });
        if (notificationOpen) {
          // console.log("onNotificationOpened  ****** ", notificationOpen);
          // App was opened by a notification
          // App was running in the background
          // Get the action triggered by the notification being opened
          const action = notificationOpen.action;
          // Get information about the notification that was opened
          const notification: Notification = notificationOpen.notification;

          const notificationData = notification._data;
          // console.log("data ***** ", notification._data);
          //For android
          if (notification && notificationData.custom_data) {
            const task_id = JSON.parse(notificationData.custom_data).id;
            this.setState({
              target_id: JSON.parse(notificationData.custom_data).target_id
            })

            if (notificationData.identifier === "add_task") {
              this.props._getTaskDetail(task_id);
              this.setState({ loading: true });
            } else if (notificationData.identifier === "mark_as_complete") {
              this.props._getTaskDetail(task_id);
              this.setState({
                selectedSegmentId: 2,
                loading: true
              });
            } else if (notificationData.identifier === "update_task") {
              this.props._getTaskDetail(task_id);
              this.setState({ loading: true });
            } else if (notificationData.identifier === "add_message") {
              this.props.navigation.navigate("Messages");
            }
          }
          //For ios
          else {
            const task_id = JSON.parse(
              notificationData["gcm.notification.custom_data"]
            ).id;
            this.setState({
              target_id: JSON.parse(notificationData["gcm.notification.custom_data"]).target_id
            })

            if (
              notificationData["gcm.notification.identifier"] === "add_task"
            ) {
              this.props._getTaskDetail(task_id);
              this.setState({ loading: true });
            } else if (
              notificationData["gcm.notification.identifier"] ===
              "mark_as_complete"
            ) {
              this.props._getTaskDetail(task_id);
              this.setState({
                selectedSegmentId: 2,
                loading: true
              });
            } else if (
              notificationData["gcm.notification.identifier"] === "update_task"
            ) {
              this.props._getTaskDetail(task_id);
              this.setState({ loading: true });
            } else if (
              notificationData["gcm.notification.identifier"] === "add_message"
            ) {
              this.props.navigation.navigate("Messages");
            }
          }
        }
      });
  };

  onReload = () => {
    this.props.myTaskApiRequest(1, 'today', 'pending', '', this.state.target_id, true);
  }
  componentDidUpdate(prevProps, prevState, nextProps) {

    const count = this.props.notificationCount.length
    if (prevState.notification_count !== count) {
      this.setState({
        notification_count: count
      })
      this.props.navigation.setParams({ count: count })
    }

  }
  componentWillReceiveProps(nextProps) {

    if (this.state.isConnected) {
      if (nextProps.detail !== null) {
        this.props.navigation.navigate("taskDetail", {
          detail: nextProps.detail,
          // reload : this.props.reload
          reload: this.onReload
        });
        this.setState({ loading: false });
      }
    } else {
      this.setState({ loading: false });
      Alert.alert(
        "Something went wrong",
        "Kindly check if the device is connected to stable cellular data plan or WiFi."
      );
    }
  }

  //Tells the connection type and status
  handleConnectionChange = connectionInfo => {
    NetInfo.isConnected.fetch().then(isConnected => {
      this.props._setNetInfo(isConnected);
      this.setState({ isConnected: isConnected });
      // alert(isConnected)
      if (isConnected) {
        const endReached = this.props.pandingTask.length;
        this.props.pandingTask.map((item, ind) => {
          if (endReached === ind + 1) {
            this.props.onMarkTaskComplete(item);
            this.props._onClearPandingTask();
          } else {
            this.props.onMarkTaskComplete(item);
          }
        });
      }
    });
  };

  componentWillUnmount() {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleConnectionChange
    );
    //FIREBASE NOTI
    AppState.removeEventListener("change", this._handleAppStateChange);


  }
  _handleAppStateChange = nextAppState => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === "active"
    ) {
      console.log("App has come to the foreground! ****** ");
      firebase
        .notifications()
        .getBadge()
        .then(count => {
          console.log("background getBadge() ********** ", count);
          this.props.navigation.setParams({ count: count });
        });
    }
    this.setState({ appState: nextAppState });
  };

  setSelectedTab(index) {
    if (index === 2) {
      if (this.state.isConnected) {
        this.setState({
          selectedSegmentId: index
        });
      } else {
        Alert.alert(
          "Something went wrong",
          "Kindly check if the device is connected to stable cellular data plan or WiFi."
        );
      }
    } else {
      this.setState({
        selectedSegmentId: index
      });
    }
  }

  render() {
    return (
      <Container>
        <StatusBar
          backgroundColor={colorsUtil.purple}
          barStyle="light-content"
        />
        <Segment>
          <Button
            first
            active={this.state.selectedSegmentId === 1 ? true : false}
            onPress={() => {
              this.setSelectedTab(1);
            }}
          >
            <Text>Assigned Tasks</Text>
          </Button>
          <Button
            last
            active={this.state.selectedSegmentId === 2 ? true : false}
            onPress={() => {
              this.setSelectedTab(2);
            }}
          >
            <Text>Created Tasks</Text>
          </Button>
        </Segment>
        {this.state.loading ? (
          <Container
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Text> Loading...</Text>
            <ActivityIndicator size="large" color="purple" />
          </Container>
        ) : (
            <Container>
              {this.state.selectedSegmentId === 1 ? (
                <HomeScreen navigation={this.props.navigation} />
              ) : (
                  <SupervisorHome navigation={this.props.navigation} />
                )}
            </Container>
          )}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  pandingTask: state.mytaskReducer.pandingTask,
  detail: state.mytaskReducer.detail,
  notificationList: state.NotificationReducer.notificationList,
  notificationCount: state.NotificationReducer.notificationCount,
});

export default connect(
  mapStateToProps,
  {
    _setNetInfo,
    onMarkTaskComplete,
    _onClearPandingTask,
    _getTaskDetail,
    myTaskApiRequest
  }
)(SegmentOutsideHeaderExample);
