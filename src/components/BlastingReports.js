import React, { Component } from 'react'
import { WebView, Platform , View} from 'react-native';


class BlastingReports extends Component {
  render() {
    return (
        <WebView
          source={{ uri: 'http://360cubes.com/bsi_staging/public/user/report' }}
          // source={{uri: 'http://bsiapp.com/public/admin/report'}}
          scalesPageToFit={(Platform.OS === 'ios') ? false : true}
          automaticallyAdjustContentInsets={true}
        />
    )
  }
}

// http://bsiapp.com/public/admin/user/report

export default BlastingReports
