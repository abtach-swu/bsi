import React, { Component } from "react";
import { Text, View, ActivityIndicator } from "react-native";

class MessageDetail extends Component {
  state = {
    messageData: {},
    isLoading: true
  };

  componentWillMount = () => {
    let selectedMessageDetail = this.props.navigation.state.params
      .selectedMessageDetail;
    this.setState({
      messageData: selectedMessageDetail,
      isLoading: false
    });
  };

  render() {
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text> Loading...</Text>
        <ActivityIndicator size="large" color="purple" />
      </View>
    ) : (
      <View style={{ justifyContent : 'center' , flex : 1}}>
        <View style={{marginLeft : 20 , marginRight : 20}}>
          <Text style={styles.titleStyle}>
            Sender :{" "}
            {`${this.state.messageData.sender.first_name} ${
              this.state.messageData.sender.last_name
            }`}{" "}
          </Text>
          <Text style={styles.titleStyle}> Message :</Text>
          <Text> {this.state.messageData.message}</Text>
        </View>
      </View>
    );
  }
}

const styles = {
  titleStyle: {
    marginTop: 16,
    marginBottom: 10,
    fontSize: 20,
    fontWeight: "bold"
  }
};

export default MessageDetail;
