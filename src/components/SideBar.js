import React, { Component } from "react"
import { View, StyleSheet, FlatList, TouchableOpacity, Alert } from "react-native"
import { Container, Text, Content, ListItem, Left, Body, Thumbnail } from "native-base"
import { images } from "../assets";
import colorsUtil from "../utilities/colorsUtil";
import { StackActions, NavigationActions } from 'react-navigation';
import {  getUserData } from "../UserPreference";
import constant from "../HttpServiceManager/constant";
import { connect } from "react-redux";
import { _onLogout } from "../actions";
import { SpinnerView } from "../utilities/common";



class SideBar extends Component {
    state = {

        user: {},
        arry : [1],
        is_selected : true,
        data: [
            {
                name: "My Task",
                route: 'SegmentScreen',
                icon: images.nav_mytask,
                id : 1
            },
            {
                name: "Inbox",
                route: 'Messages',
                icon: images.message,
                id : 2
            },
            {
                name: "Blasting Report",
                route: 'blastingReport',
                icon: images.nav_blast_report,
                id : 3
            },
            {
                name: "Help & FAQ",
                route: 'fAQS',
                icon: images.nav_help_faqs,
                id : 4
            },
            {
                name: "Logout",
                route: 'loginStack',
                icon: images.nav_logout,
                id : 5
            },
        ],
        userData: {}


    }

    componentDidMount() {
        getUserData().then(user => {
            this.setState({
                userData: user,
            })
        })
    }

    userUpdated = () => {
        getUserData().then(user => {
            this.setState({
                userData: user,
            })
        })
    }

    _onPress = (item) => {
        this.props.navigation.toggleDrawer();
        this.state.arry.pop();
        this.state.arry.push(item.id);
        this.setState({
            is_selected: !this.state.is_selected
        });
    }

     navigate(route, item) {
        if (route === "loginStack") {
            if(this.props.isConnected){
                this.props._onLogout();
            }
            else {
                Alert.alert('Something went wrong', 'Kindly check if the device is connected to stable cellular data plan or WiFi.');
            }
            // this._navigateTo(route)
        } 
        else if(route === "Messages"){
            if(this.props.isConnected){
                this._onPress(item)
                this.props.navigation.navigate(route)
            }else {
                Alert.alert('Something went wrong', 'Kindly check if the device is connected to stable cellular data plan or WiFi.');
            }
        }
        else {
            this._onPress(item)
            this.props.navigation.navigate(route)
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.message === "LOGOUT"){
            this._navigateTo('loginStack')
        }
    }

    _navigateTo = (routs) => {
       
        const resetAction = StackActions.reset({
            index: 0,
            key: null,
            actions: [
                NavigationActions.navigate({ routeName: routs }),
            ],
        });
        this.props.navigation.dispatch(resetAction);
    }
    viewProfile = (item) => {
        this.props.navigation.toggleDrawer();
        this.props.navigation.navigate("viewProfile", { userUpdated : this.userUpdated})
    }


    render() {
        return (
            <Container>
                <Content>
                    <View style={{ alignItems: "center", justifyContent: "center", height: 200 }} >
                        <TouchableOpacity onPress={(item) => this.viewProfile(item)}>
                            <Thumbnail large style={{ borderColor: colorsUtil.borderColor, borderWidth: 4, }}
                                source={{ uri: constant.imageBaseUrl + this.state.userData.image_url }} />
                            <Text>{this.state.userData.name}</Text>
                            <Text note>{this.state.userData.email}</Text>
                        </TouchableOpacity>
                    </View>
                    <FlatList
                        data={this.state.data}
                        extraData={this.state}
                        keyExtractor={(item)=> `${item.id}`}
                        renderItem={({ item }) => (
                            <View  
                            style={{
                                borderRightColor : this.state.arry[0] == item.id ?  colorsUtil.purple : colorsUtil.white ,
                                backgroundColor : this.state.arry[0] == item.id ?  '#fae6ff' : colorsUtil.white ,
                                
                                borderRightWidth : 5
                                }}>
                            <ListItem thumbnail onPress={() => this.navigate(item.route , item)} noBorder>
                                <Left>
                                    <Thumbnail square source={item.icon} style={styles.iconStyle} />
                                </Left>
                                <Body>
                                    <Text note style={styles.testStyle}>{item.name}</Text>
                                </Body>
                            </ListItem>
                            </View>
                        )
                        } />
                {this.props.loadng && <SpinnerView />}
                </Content>
            </Container>
        )
    }
}
const styles = StyleSheet.create({

    iconStyle: {
        width: 26,
        height: 26
    },
    testStyle: {
        fontSize: 14
    }
})

const mapStateToProps = (state) => ({
    isConnected : state.authReducer.isConnected,
    message : state.authReducer.message,
    loading : state.authReducer.loading
  })
  
  export default connect(
    mapStateToProps,
    {
        _onLogout
    }
  )(SideBar);