import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  FlatList,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  ScrollView
} from "react-native";
import Modal from "react-native-modal";
import colorsUtil from "../utilities/colorsUtil";
import { NormalButton } from "../reuseableComponent";
import { Toasts } from "../utilities/showToast";
import { heightRatio, widthRatio } from "../utilities/utility";
import { connect } from "react-redux";
import { images } from "../assets";
import {
  onChangeInventoryUsedQuantity,
  onSavePortData,
  _portSelectionDone,
  _onSavePortInventoryReport,
  onInventoryUpdatdSaved
} from "../actions";

class PortMap extends Component {
  state = {
    inventory: [],
    port_data: [],
    selected_ports: [],
    selectedPortInventory: [],
    data: {},
    detail: {},
    location: {},
    text: "",
    floor_id: "",
    selectedPortId: "",
    isLoading: true,
    isMapView: false,
    is_selected: false,
    showUsedQty: false,
    markAsComplt: false,
    isSelectPort: false,
    isModalVisible: false,
    jugarItem: null
  };

  componentDidMount() {
    let navigation = this.props.navigation;
    let isSelectPort = navigation.getParam("isSelectPort");
    let detail = navigation.getParam("detail");
    let isMapView = navigation.getParam("isMapView");
    let markAsComplt = navigation.getParam("markAsComplt");
    let item = navigation.getParam("item"); // fromMarkAsCmplt
    let location = navigation.getParam("location"); //View map or markAsComplt


    if (markAsComplt) {
      isMapView = false;
    }
    if (isSelectPort) {
      let selectPlantTitle = this.props.plantData.find(
        it => it.id === this.props.selectedPlant
      ).title;
      let selectUnitTitle = this.props.unitData.find(
        it => it.id === this.props.selectedUnit
      ).title;
      let selectFloorTitle = this.props.navigation.getParam("item").title;
      let location = {
        unit_title: selectUnitTitle,
        plant_title: selectPlantTitle,
        floor_title: selectFloorTitle
      };
      this.setState({
        location,
        isSelectPort
      });
    } else if (isMapView) {
      let portDetail = detail.port.find(it => it.floor_id == item.floor_id);
      this.setState({
        data: portDetail.data,
        location,
        isMapView: isMapView,
        isLoading: false,
        detail
      });
    } else if (markAsComplt) {
      let task_Detail = Object.assign({}, detail);
      let portDetail = task_Detail.port.find(
        it => it.floor_id == item.floor_id
      );
      this.props.onSavePortData(
        task_Detail,
        portDetail.data,
        task_Detail.inventory,
        item.floor_id,
        location
      );
      this.setState({
        data: portDetail.data,
        location,
        floor_id: item.floor_id,
        markAsComplt,
        selected_ports: this.props.selected_ports,
        isLoading: false
      });
    }
  }
  componentWillReceiveProps(nextProp) {
    if (this.state.isSelectPort) {
      this.setState({
        data: nextProp.portData,
        isSelectPort: true,
        isLoading: false
      });
    }
  }

  _keyExtractor = (item, index) => `${item.port_id}`;

  _toggleModal = () => {
    this.setState({
      isModalVisible: !this.state.isModalVisible
    });
  };

  GetSectionListItem = pressedItem => {
    if (this.state.isSelectPort) {
      pressedItem.is_selected = !pressedItem.is_selected;
      this.setState({
        is_selected: !this.state.is_selected
      });
    }
    else if (this.state.markAsComplt) {
      if (pressedItem.is_selected) {
        this.setState(
          {
            jugarItem: pressedItem,
            selectedPortId: pressedItem.port_id
          },
          () => {
            let selectedPortObject = this.props.selected_ports.find(
              item => item.floor_id === this.state.floor_id).data.find(itm => itm.port_id == pressedItem.port_id);
            this.setState(
              {
                selectedPortObject,
                selectedPortInventory: selectedPortObject.inventory
              },
              () => {
                this._toggleModal();
              }
            );
          }
        );
      }
    }
    else if (pressedItem.is_selected && this.state.detail.status) {
      if (pressedItem.hasOwnProperty("inventory")) {
        this.setState({
          selectedPortInventory: pressedItem["inventory"],
          showUsedQty: true
        },
          () => this._toggleModal()
        )
      }
    }
  };

  _onChangeText(text, item) {
    this.setState({
      selectedPortInventory: this.state.selectedPortInventory.map(data => data.id === item.id ? { ...data, used_qty: isNaN(parseInt(text)) === true ? 0 : parseInt(text) } : data)
    })
  }

  _incrementDecrement = (val, item) => {
    this.setState({
      selectedPortInventory: this.state.selectedPortInventory.map(data => data.id === item.id ? { ...data, used_qty: data.used_qty + val } : data)
    })
  }

  _saveInventory = () => {
    this.state.jugarItem ? this.state.jugarItem.is_filled = 1 : null
    this.props.onInventoryUpdatdSaved(this.state.selectedPortInventory, this.state.selectedPortId, this.state.floor_id)
    this._toggleModal();
  };

  _onDonePortInventoryReport = () => {
    this.props._onSavePortInventoryReport(this.props.selected_ports, this.state.floor_id);
    this.props.navigation.state.params._calcStatus()
    this.props.navigation.pop()
  };

  onPortSelectionDone = () => {
    let selectedPortTitle = [];
    let items = this.state.data;

    if ("back" in items) {
      let backPortSelectedItems = items.back
        .filter(it => it.is_selected === true)
        .map(it => it.port_title);
      if (backPortSelectedItems.length > 0) {
        selectedPortTitle.push(backPortSelectedItems.toString());
      }
    }

    if ("left" in items) {
      let leftPortSelectedItems = items.left
        .filter(it => it.is_selected === true)
        .map(it => it.port_title);
      if (leftPortSelectedItems.length > 0) {
        selectedPortTitle.push(leftPortSelectedItems.toString());
      }
    }

    if ("right" in items) {
      let rightPortSelectedItems = items.right
        .filter(it => it.is_selected === true)
        .map(it => it.port_title);
      if (rightPortSelectedItems.length > 0) {
        selectedPortTitle.push(rightPortSelectedItems.toString());
      }
    }

    if ("front" in items) {
      let frontPortSelectedItems = items.front
        .filter(it => it.is_selected === true)
        .map(it => it.port_title);
      if (frontPortSelectedItems.length > 0) {
        selectedPortTitle.push(frontPortSelectedItems.toString());
      }
    }
    if (selectedPortTitle.length > 0) {
      let item = this.props.navigation.getParam("item");
      this.props._portSelectionDone(item, this.props.portData);
      this.props.navigation.pop();
      this.props.navigation.state.params.itemSelected(item, selectedPortTitle);
    } else {
      Toasts.showToast("Please select at least one Port");
    }
  };

  //=============================================================
  // Modal Flatlist Component
  //=============================================================
  renderItem = ({ item }) => {
    return (
      <View style={{ flexDirection: "row", flex: 1 }}>
        <View style={{ flex: 2 }}>
          <Text style={{ fontSize: 17 }}>{item.title}</Text>
        </View>
        <View style={{ flex: 1 }}>
          <Text style={{ fontSize: 17 }}>{item.name}</Text>
        </View>
        {this.state.showUsedQty ?
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginBottom: 10,
              flex: 1.5,
              justifyContent: "flex-end"
            }}
          >
            <Text style={{ color: "#808080" }}>{item.inventory_qty}</Text>
          </View>
          :
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginBottom: 5,
              flex: 1.5
            }}
          >
            <TouchableOpacity
              disabled={item.used_qty == 0 ? true : false}
              onPress={() => this._incrementDecrement(-1, item)}
              style={{ padding: 5 }}
            >
              <Image
                source={
                  item.used_qty == 0 ? images.minus_icon_red : images.minus_icon
                }
                style={{ height: 15, width: 10 }}
              />
            </TouchableOpacity>
            <TextInput
              value={`${item.used_qty}`}
              placeholder="123"
              style={styles.input}
              keyboardType="numeric"
              maxLength={6}
              onChangeText={text => this._onChangeText(text, item)}
            />
            <TouchableOpacity
              onPress={() => this._incrementDecrement(1, item)}
              style={{ paddingRight: 5, paddingBottom: 5, paddingTop: 5 }}
            >
              <Image
                source={images.plus_icon}
                style={{ height: 20, width: 20 }}
              />
            </TouchableOpacity>
          </View>}
      </View>
    );
  };

  render() {
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text> Loading...</Text>
        <ActivityIndicator size="large" color="purple" />
      </View>
    ) : (
        <View style={{ flex: 1 }}>
          <View style={{ flex: 0.2, justifyContent: "center" }}>
            <Text
              style={{ color: colorsUtil.purple, fontSize: 15, paddingLeft: 15 }}
            >
              {`${this.state.location.plant_title} > ${
                this.state.location.unit_title
                } > ${this.state.location.floor_title}`}
            </Text>
          </View>

          <View
            style={{
              flex: 2,
              flexDirection: "column",
              justifyContent: "space-between"
            }}
          >
            <View>
              <View
                style={{
                  backgroundColor: colorsUtil.purple,
                  alignItems: "center"
                }}
              >
                <Text style={styles.headingStyle}>Back</Text>
              </View>

              <FlatList
                data={this.state.data.back}
                horizontal
                extraData={this.state}
                contentContainerStyle={{ flex: 1, justifyContent: "center" }}
                keyExtractor={this._keyExtractor}
                renderItem={({ item }) => {
                  return (
                    <TouchableOpacity
                      onPress={() => this.GetSectionListItem(item)}
                    >
                      <View
                        style={{
                          alignItems: "center",
                          borderColor: colorsUtil.purple,
                          borderWidth: 1,
                          backgroundColor: item.is_selected
                            ? "yellow"
                            : colorsUtil.white
                        }}
                      >
                        <Text style={{ ...styles.fontStyle, textDecorationLine: item.is_filled ? "underline" : "none" }}>{item.port_title}</Text>
                      </View>
                    </TouchableOpacity>
                  );
                }}
              />
            </View>

            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                height: "55%"
              }}
            >
              <View
                style={{
                  flexDirection: "row"
                }}
              >
                <View
                  style={{
                    backgroundColor: colorsUtil.purple,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <Text
                    style={[
                      styles.headingStyle,
                      { transform: [{ rotate: "270deg" }] }
                    ]}
                  >
                    Left
                </Text>
                </View>

                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <FlatList
                    data={this.state.data.left}
                    extraData={this.state}
                    keyExtractor={this._keyExtractor}
                    renderItem={({ item }) => {
                      return (
                        <TouchableOpacity
                          onPress={() => this.GetSectionListItem(item)}
                        >
                          <View
                            style={{
                              alignItems: "center",
                              borderColor: colorsUtil.purple,
                              borderWidth: 1,
                              backgroundColor: item.is_selected
                                ? "yellow"
                                : colorsUtil.white
                            }}
                          >
                            <Text style={{ ...styles.fontStyle, textDecorationLine: item.is_filled ? "underline" : "none" }}>{item.port_title}</Text>
                          </View>
                        </TouchableOpacity>
                      );
                    }}
                  />
                </View>
              </View>
              <View style={{ justifyContent: "center", alignItems: 'center' }}>
                <Text
                  style={{
                    color: colorsUtil.purple,
                    fontSize: 25,
                    fontWeight: "bold",
                  }}
                >
                  {this.state.location.floor_title}
                </Text>
                <Text
                  style={{
                    color: colorsUtil.purple,
                    fontSize: 35,
                    fontWeight: "bold"
                  }}
                >
                  FLOOR
              </Text>
              </View>

              <View style={{ flexDirection: "row" }}>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <FlatList
                    data={this.state.data.right}
                    extraData={this.state}
                    keyExtractor={this._keyExtractor}
                    renderItem={({ item }) => {
                      return (
                        <TouchableOpacity
                          onPress={() => this.GetSectionListItem(item)}
                        >
                          <View
                            style={{
                              alignItems: "center",
                              borderColor: colorsUtil.purple,
                              borderWidth: 1,
                              backgroundColor: item.is_selected
                                ? "yellow"
                                : colorsUtil.white
                            }}
                          >
                            <Text style={{ ...styles.fontStyle, textDecorationLine: item.is_filled ? "underline" : "none" }}>{item.port_title}</Text>
                          </View>
                        </TouchableOpacity>
                      );
                    }}
                  />
                </View>
                <View
                  style={{
                    backgroundColor: colorsUtil.purple,
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <Text
                    style={[
                      styles.headingStyle,
                      { transform: [{ rotate: "270deg" }] }
                    ]}
                  >
                    Right
                </Text>
                </View>
              </View>
            </View>

            <View>
              <FlatList
                data={this.state.data.front}
                extraData={this.state}
                horizontal
                contentContainerStyle={{ flex: 1, justifyContent: "center" }}
                keyExtractor={this._keyExtractor}
                renderItem={({ item }) => {
                  return (
                    <TouchableOpacity
                      onPress={() => this.GetSectionListItem(item)}
                    >
                      <View
                        style={{
                          alignItems: "center",
                          borderColor: colorsUtil.purple,
                          borderWidth: 1,
                          backgroundColor: item.is_selected
                            ? "yellow"
                            : colorsUtil.white
                        }}
                      >
                        <Text style={{ ...styles.fontStyle, textDecorationLine: item.is_filled ? "underline" : "none" }}>{item.port_title}</Text>
                      </View>
                    </TouchableOpacity>
                  );
                }}
              />
              <View
                style={{
                  backgroundColor: colorsUtil.purple,
                  alignItems: "center"
                }}
              >
                <Text style={styles.headingStyle}>Front</Text>
              </View>
            </View>
          </View>
          <View
            style={{
              flex: 0.2,
              marginTop: 10,
              marginBottom: 10,
              marginRight: 20,
              marginLeft: 20
            }}
          >
            {this.state.isSelectPort || this.state.markAsComplt ? (
              <NormalButton
                text="Done"
                textColor={colorsUtil.white}
                btnSyle={{ height: 50 }}
                onClick={
                  this.state.isSelectPort
                    ? this.onPortSelectionDone
                    : this._onDonePortInventoryReport
                }
              />
            ) : null}
          </View>

          <Modal isVisible={this.state.isModalVisible}
            onBackdropPress={this._toggleModal}
          >
            {this.state.selectedPortInventory.length > 7 ?
              (
                <ScrollView style={styles.modalContent}>
                  <View
                    style={{
                      borderBottomWidth: 1,
                      marginBottom: 20,
                      justifyContent: "space-between",
                      flexDirection: "row",
                    }}
                  >
                    <Text style={{ fontSize: 25, color: "black" }}>Inventory</Text>
                    <TouchableOpacity onPress={this._toggleModal}>
                      <Image
                        source={images.cancel_icon}
                        style={{ width: 15, height: 15 }}
                      />
                    </TouchableOpacity>
                  </View>
                  <View>
                    <FlatList
                      data={this.state.selectedPortInventory}
                      renderItem={this.renderItem}
                      // extraData={this.state.selectedPortInventory}
                      keyExtractor={(item, index) => `${index}`}
                    />
                  </View>
                  <View style={{ marginTop: 5, marginBottom: 20 }}>
                    {!this.state.showUsedQty ?
                      <NormalButton
                        text="Save"
                        textColor={colorsUtil.white}
                        btnSyle={{ height: heightRatio(120) }}
                        onClick={this._saveInventory}
                      />
                      :
                      null
                    }
                  </View>
                </ScrollView>
              )
              :
              (
                <View style={styles.modalContent}>
                  <View
                    style={{
                      borderBottomWidth: 1,
                      marginBottom: 20,
                      justifyContent: "space-between",
                      flexDirection: "row",
                    }}
                  >
                    <Text style={{ fontSize: 25, color: "black" }}>Inventory</Text>
                    <TouchableOpacity onPress={this._toggleModal}>
                      <Image
                        source={images.cancel_icon}
                        style={{ width: 15, height: 15 }}
                      />
                    </TouchableOpacity>
                  </View>
                  <View>
                    <FlatList
                      data={this.state.selectedPortInventory}
                      renderItem={this.renderItem}
                      keyExtractor={(item, index) => `${index}`}
                    />
                  </View>
                  <View style={{ marginTop: 5 }}>
                    {!this.state.showUsedQty ?
                      <NormalButton
                        text="Save"
                        textColor={colorsUtil.white}
                        btnSyle={{ height: heightRatio(100) }}
                        onClick={this._saveInventory}
                      />
                      :
                      null
                    }
                  </View>
                </View>
              )
            }
          </Modal>
        </View>
      );
  }
}

const mapStateToProps = state => {

  return {
    portData: state.addTaskReducer.portResponseData,
    taskDetail: state.InventoryReducer.task_detail,
    selected_ports: state.InventoryReducer.selected_ports,
    task_inventory: state.InventoryReducer.task_inventory,
    plantData: state.addTaskReducer.plantResponseData,
    unitData: state.addTaskReducer.unitResponseData,
    selectedPlant: state.addTaskReducer.selectedPlant,
    selectedUnit: state.addTaskReducer.selectedUnit,
    selectedFloor: state.addTaskReducer.selectedFloor
  };
};

const styles = {
  fontStyle: {
    fontSize: 16,
    fontWeight: "bold",
    padding: 6,
    color: colorsUtil.purple,

  },
  headingStyle: {
    fontSize: 24,
    fontWeight: "bold",
    color: colorsUtil.white
  },
  modalContent: {
    backgroundColor: "white",
    padding: 10,
    borderRadius: 4,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  input: {
    flex: 1.5,
    height: 30,
    borderRadius: 2,
    borderColor: colorsUtil.purple,
    borderWidth: 1.5,
    padding: 5
  }
};

export default connect(
  mapStateToProps,
  {
    onChangeInventoryUsedQuantity,
    onSavePortData,
    _portSelectionDone,
    _onSavePortInventoryReport,
    onInventoryUpdatdSaved
  }
)(PortMap);
