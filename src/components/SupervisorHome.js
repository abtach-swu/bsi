import React, { Component } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  Alert
} from "react-native";
import {
  Container,
  Tab,
  Tabs,
  Thumbnail,
} from "native-base";
import { images } from "../assets";
import { getUserData } from "../UserPreference";
import SupervisorTasksTab from "./SupervisorTasksTab";
import { myTaskApiRequest, onNewTaskAdd } from "../actions";
import { connect } from "react-redux";
import { heightRatio, widthRatio } from "../utilities/utility";


class SupervisorHome extends Component {
  state = {
    signinUser: {},
    selectedTab: "today"
  };

  
  componentDidMount() {
    getUserData().then(user => {
        this.setState({
            signinUser: user
        })
        this.props.myTaskApiRequest(1,'today','pending',this.state.signinUser.id, '' , true);
});
}
onReload = ()=>{
  if (this.state.selectedTab.toLowerCase() === "done") {
    this.props.myTaskApiRequest(1,'','completed',this.state.signinUser.id, '' , true);
  } else if (this.state.selectedTab.toLowerCase() === "pending") {
    this.props.myTaskApiRequest(1,'','pending',this.state.signinUser.id, '' , true);
  } else {
    this.props.myTaskApiRequest(1,this.state.selectedTab.toLowerCase(),'pending',this.state.signinUser.id, '' , true);
  }
}


  taskAdd = () => {
    if(this.props.isConnected){
      this.props.onNewTaskAdd()
      this.props.navigation.navigate("taskAdd" , { reload : this.onReload});
    }else {
      Alert.alert('Something went wrong', 'Kindly check if the device is connected to stable cellular data plan or WiFi.');
    }
  };
  onTabSelected = heading => {
    this.setState({
      selectedTab: heading 
    });
    if (heading.toLowerCase() === "done") {
      this.props.myTaskApiRequest(1,'','completed',this.state.signinUser.id, '' , true);
    } else if (heading.toLowerCase() === "pending") {
      this.props.myTaskApiRequest(1,'','pending',this.state.signinUser.id, '' , true);
    } else {
      this.props.myTaskApiRequest(1,heading.toLowerCase(),'pending',this.state.signinUser.id, '' , true);
    }
  };

  render() {
    return (
      <Container>
        <Tabs
          initialPage={0}
          onChangeTab={({ ref }) => this.onTabSelected(ref.props.heading)}
        >
          <Tab heading="Today">
            <SupervisorTasksTab
              navigation={this.props.navigation}
              reload={this.onReload}
              filterType={"today"}
              statusType={"pending"}
              actor_id = {this.state.signinUser.id}
            />
          </Tab>
          <Tab heading="Upcoming">
            <SupervisorTasksTab
              navigation={this.props.navigation}
              reload={this.onReload}
              filterType={"upcoming"}
              statusType={"pending"}
              actor_id = {this.state.signinUser.id}
            />
          </Tab>
          <Tab heading="Done">
            <SupervisorTasksTab
              navigation={this.props.navigation}
              reload={this.onReload}
              statusType={"completed"}
              filterType={""}
              actor_id = {this.state.signinUser.id}
            />
          </Tab>
          <Tab heading="Pending">
            <SupervisorTasksTab
              navigation={this.props.navigation}
              reload={this.onReload}
              statusType={"pending"}
              filterType={""}
              actor_id = {this.state.signinUser.id}
            />
          </Tab>
        </Tabs>
        <TouchableOpacity onPress={this.taskAdd} style={styles.add_task}>
          <Thumbnail
            source={images.add_task_fb}
            style={{ width: widthRatio(200), height: heightRatio(200) }}
          />
        </TouchableOpacity>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  add_task: {
    position: "absolute",
    bottom: 25,
    right: 30
  }
});

const mapStateToProp = state => {
  return {
    loading: state.mytaskReducer.loading,
    data: state.mytaskReducer.responseData,
    refreshing: state.mytaskReducer.refreshing,
    nextPage: state.mytaskReducer.nextPage,
    currentPage: state.mytaskReducer.currentPage,
    isConnected : state.authReducer.isConnected
  };
};
export default connect(
  mapStateToProp,
  {
    myTaskApiRequest,
    onNewTaskAdd
  }
)(SupervisorHome);
