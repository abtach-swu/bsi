import {
    USER_LIST_SUCCESS,
    USER_LIST_FAIL,
    MESSEGE_RETRIEVED_SUCCESS,
    MESSEGE_RETRIEVED_FAIL,
    MESSAGE_SEND_SECCESS,
    MESSAGE_SEND_FAIL,
    MESSAGE_READ_SECCESS,
    MESSAGE_READ_FAIL,
} from "./types";
import HttpServiceManager from "../HttpServiceManager/HttpServiceManager";
import HttpConstant from "../HttpServiceManager/constant";


export const getAllUserList = () => {
    return dispatch => {
        HttpServiceManager.getInstance().request(HttpConstant.userList, '', "GET").then(response => {
            dispatch({
                type: USER_LIST_SUCCESS,
                payload: response.data
            })

        }).catch(error => {
            dispatch({
                type: USER_LIST_FAIL,
                payload: error
            })
        })
    }
}

export const getMessages = (type) => {
    let param = {
        type
    }
    return dispatch => {
        HttpServiceManager.getInstance().request(HttpConstant.message, param, "GET").then(response => {
            dispatch({
                type: MESSEGE_RETRIEVED_SUCCESS,
                payload: response.data
            })
        }).catch(error => {
            dispatch({
                type: MESSEGE_RETRIEVED_FAIL,
                payload: error
            })
        })
    }
}

export const _onSendMessage = (id, message) => {
    let param = {
        receiver_id: id,
        message
    }

    return dispatch => {
        dispatch({ type: 'MESSAGE_SEND' })
        HttpServiceManager.getInstance().request(HttpConstant.message, param, "POST").then(response => {
            dispatch({
                type: MESSAGE_SEND_SECCESS,
                payload: response.message
            })
        }).catch(error => {
            dispatch({
                type: MESSAGE_SEND_FAIL,
                payload: error

            })
        })
    }
}

export const _onMessageRead = (id) => {
    return dispatch => {
        HttpServiceManager.getInstance().request(`${HttpConstant.message}/${id}`, { is_read: 1 }, "PUT").then(response => {
            dispatch({
                type: MESSAGE_READ_SECCESS,
                payload: response.message
            })
        }).catch(error => {
            dispatch({
                type: MESSAGE_READ_FAIL,
                payload: error

            })
        })
    }
}

export const _onTabSelection = () => {
    return dispatch => {
        dispatch({
            type: 'CLEAR_REDUX_MESSAGE_STATE'
        })
    }
}