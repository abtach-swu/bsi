import {
    EMAIL_CHANGED_LOGIN, PASSWORD_CHANGED_LOGIN,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAIL,
    RESET_PASSWORD_SUCCESS,
    RESET_PASSWORD_FAIL,
    IS_LOGIN_LOADING
} from './types';
import constant from '../HttpServiceManager/constant';
import HttpServiceManager from '../HttpServiceManager/HttpServiceManager';
import { clearAll, setUser, setUserToken } from "../UserPreference";

export const emailChanged = (text) => {
    return {
        type: EMAIL_CHANGED_LOGIN,
        payload: text
    }
}
export const passwordChanged = (text) => {
    return {
        type: PASSWORD_CHANGED_LOGIN,
        payload: text
    }
}
//userLoginReq
export const loginUser = ({ email, password, device_token, device_type }, loading = false) => {

    return (dispatch) => {
        if (loading) {
            dispatch({
                type: IS_LOGIN_LOADING
            });
        }
        HttpServiceManager.getInstance().request(constant.login, { email, password, device_token, device_type }, 'POST')
            .then((response) => {
                setUser(response.data);
                setUserToken(response.data.token);
                dispatch({
                    type: LOGIN_USER_SUCCESS,
                    payload: response.data
                });
            }).catch((error) => {
                dispatch({
                    type: LOGIN_USER_FAIL,
                    payload: error
                });
            });
    }
}
// Forgot Password
export const forgotPassword = (email, loading = false) => {
    return (dispatch) => {
        if (loading) {
            dispatch({
                type: IS_LOGIN_LOADING
            });
        }
        HttpServiceManager.getInstance().request(constant.forgotPassword, { email }, 'POST').
            then((response) => {
                dispatch({
                    type: RESET_PASSWORD_SUCCESS,
                    payload: response.message
                });
            }).catch((error) => {
                dispatch({
                    type: RESET_PASSWORD_FAIL,
                    payload: error
                });
            });
    }
}

// Logout
export const _onLogout = () => {
    return (dispatch) => {
        dispatch({ type: "ON_LOGOUT" })
        HttpServiceManager.getInstance().request(constant.userLogout, {}, 'POST').
            then((response) => {
                clearAll()
                dispatch({
                    type: "ON_USER_LOGOUT_SUCCESS",
                    payload: response.message
                });
            }).catch((error) => {
                dispatch({
                    type: "ON_USER_LOGOUT_FAIL",
                    payload: error.message
                });
            });
    }
}

export const _setNetInfo = (isConnected) => {
    return dispatch => {
        dispatch({
            type: 'NETWORK_INFO',
            payload: isConnected
        })
    }
}

