import {
    PROFILE_SUCCESS,
    PROFILE_ERROR,
    NAME_CHANGED_PROFILE,
    IMAGE_CHANGED_PROFILE,
    IS_LOADING,

    CHANGE_PASSWORD_SUCCESS,
    CHANGE_PASSWORD_ERROR,
    CLEAR_ERROR_REDUX,

} from "./types"

import HttpServiceManager from "../HttpServiceManager/HttpServiceManager";
import HttpConstant from "../HttpServiceManager/constant";


/*
    start            Profile
*/
export const onChangeName = (name) => {
    return (dispatch) => {
        dispatch({
            type: NAME_CHANGED_PROFILE,
            payload: name
        })
    }
}
export const onChangePicture = (imagePath) => {
    return (dispatch) => {
        dispatch({
            type: IMAGE_CHANGED_PROFILE,
            payload: imagePath
        })
    }
}
export const updateProfile = (name, imageUrl, isLoading = false) => {
    const formData = new FormData();
    formData.append("name", name)
    if (imageUrl !== undefined) {
        formData.append('image_url', { uri: imageUrl, name: 'profile.jpg', type: 'image/jpeg' });
    }

    return (dispatch) => {
        if (isLoading) {
            dispatch({
                type: IS_LOADING
            })
        }

        HttpServiceManager.getInstance().requestWithImage(HttpConstant.updateProfile, formData, "POST")
            .then((response) => {
                dispatch({
                    type: PROFILE_SUCCESS,
                    payload: response
                });
            })
            .catch((error) => {
                dispatch({
                    type: PROFILE_ERROR,
                    payload: error
                })
            })
    }

}
/*
    End            Profile
*/

/*
    Start            Change Password
*/

export const clearErrors = () => {

    return (dispatch) => {
        dispatch({ type: CLEAR_ERROR_REDUX })
    }

}
export const changePasswordRequest = (old_password, new_password, loading = false) => {

    return (dispatch) => {
        if (loading) {
            dispatch({
                type: IS_LOADING
            });
        }

        HttpServiceManager.getInstance().request(HttpConstant.changePassword, { old_password, new_password }, 'POST').
            then((response) => {
                dispatch({
                    type: CHANGE_PASSWORD_SUCCESS,
                    payload: response.message
                });
            }).catch((error) => {
                dispatch({
                    type: CHANGE_PASSWORD_ERROR,
                    payload: error
                });
            });
    }
}