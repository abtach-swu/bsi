import {
    USER_LIST_SUCCESS,
} from "./types";
import HttpServiceManager from "../HttpServiceManager/HttpServiceManager";
import HttpConstant from "../HttpServiceManager/constant";


export const getNotificationList = (flag) => {
    return dispatch => {
        dispatch({ type: 'NOTIFICATION' })
        HttpServiceManager.getInstance().request(HttpConstant.notificationList, '', "GET")
            .then(response => {
                dispatch({
                    type: "NOTIFICATION_SUCCESS",
                    payload: response.data,
                    flag
                })
            }).catch(error => {
                dispatch({
                    type: "NOTIFICATION_FAIL",
                    payload: error
                })
            })
    }
}



export const _onNotificationRead = (id) => {
    return dispatch => {
        HttpServiceManager.getInstance().request(`${HttpConstant.notificationUpdate}/${id}`, { is_read: 1 }, "PUT").then(response => {
            dispatch({
                type: 'NOTIFICATION_READ_SECCESS',
                payload: response.message
            })
        }).catch(error => {
            dispatch({
                type: 'NOTIFICATION_READ_FAIL',
                payload: error

            })
        })
    }
}


export const getFAQsRequest = () => {
    return dispatch => {
        dispatch({ type: 'FAQ' })
        HttpServiceManager.getInstance().request(HttpConstant.faq, {}, "GET").then(response => {
            dispatch({
                type: 'FAQS_GET_SECCESS',
                payload: response.data
            })
        }).catch(error => {
            dispatch({
                type: 'FAQS_GET_FAIL',
                payload: error.message

            })
        })
    }
}
