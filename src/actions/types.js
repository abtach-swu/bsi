//---Login Actions
export const EMAIL_CHANGED_LOGIN = 'email_changed_login';
export const PASSWORD_CHANGED_LOGIN = 'password_changed_login';
export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER = 'login_user';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const RESET_LOGIN = 'reset_login';
export const IS_LOGIN = 'is_login';
export const IS_LOGIN_LOADING = 'is_login_loading';


//Reset password action
export const RESET_PASSWORD_SUCCESS = 'reset_password_success';
export const RESET_PASSWORD_FAIL = 'reset_password_fail';

export const IS_REFRESHING = 'is_refreshing';
export const IS_LOADING = 'is_loading';


// My Task
//Reset password action
export const MY_TASK_LOAD_MORE = 'my_task_load_more';
export const MY_TASK_SUCCESS = 'my_task_success';
export const MY_TASK_FAIL = 'my_task_fail';

// My Crew List
export const MY_USER_LIST_SUCCESS = 'my_user_list_success';
export const MY_USER_LIST_FAIL = 'my_user_list_fail';
export const INVENTORY_LIST_SUCCESS = 'inventory_list_success';
export const INVENTORY_LIST_FAIL = 'inventory_list_fail';
export const SELECTED_INVENTORY_ARRAY = 'SELECTED_INVENTORY_ARRAY';
export const PLANT_LIST_SUCCESS = 'plant_list_success';
export const PLANT_LIST_FAIL = 'plant_list_fail';
export const UNIT_LIST_SUCCESS = 'unit_list_success';
export const UNIT_LIST_FAIL = 'unit_list_fail';
export const FLOOR_LIST_SUCCESS = 'floor_list_success';
export const FLOOR_LIST_FAIL = 'floor_list_fail';
export const PORT_LIST_SUCCESS = 'port_list_success';
export const PORT_LIST_FAIL = 'port_list_fail';

export const INVENTORY_CHANGE_NAME = 'inventory_change_name';
export const INVENTORY_CHANGE_QUANTITY = 'inventory_change_quantity';
export const INVENTORY_CHANGE_INCREMENT = 'INVENTORY_CHANGE_INCREMENT';

//Task Add Events Type
export const ON_SELECT_TARGET = 'ON_SELECT_TARGET';
export const ON_SELECT_PLANT = 'ON_SELECT_PLANT';
export const ON_SELECT_UNIT = 'ON_SELECT_UNIT';
export const ON_SELECT_FLOOR = 'ON_SELECT_FLOOR';
export const ON_SELECT_PORT = 'ON_SELECT_PORT';
export const ON_SAVE_PORT = 'ON_SAVE_PORT';
export const ON_CHANGE_DATE = 'ON_CHANGE_DATE';
export const ON_CHANGE_TIME = 'ON_CHANGE_TIME';
export const ADD_TASK_SUCCESS = 'add_task_success';
export const ADD_TASK_FAIL = 'add_task_fail';

//Task Complete Events type
export const INVENTORY_CHANGE_USED_QUANTITY = 'INVENTORY_CHANGE_USED_QUANTITY';
export const ON_MARK_TASK_COMPLETE_SUCCESS = 'ON_MARK_TASK_COMPLETE_SUCCESS';
export const ON_MARK_TASK_COMPLETE_FAIL = 'ON_MARK_TASK_COMPLETE_FAIL';
export const TASK_DETAIL_SAVED = 'TASK_DETAIL_SAVED';
export const ON_SAVED_INVENTORY = 'ON_SAVED_INVENTORY';
export const SELECTED_PORT_INVENTORY_SAVED = 'SELECTED_PORT_INVENTORY_SAVED';
export const ON_SAVED_SELECTED_PORT_INVENTORY = 'ON_SAVED_SELECTED_PORT_INVENTORY';
export const ON_SAVED_INVENTORY_PORT_REPORT = 'ON_SAVED_INVENTORY_PORT_REPORT';
export const INVENTORY_CHANGE_USED_INCREMENT = 'INVENTORY_CHANGE_USED_INCREMENT';



//Profile
export const PROFILE_SUCCESS ='profile_success'
export const PROFILE_ERROR ="profile_fail"
export const NAME_CHANGED_PROFILE = 'name_changed_profile';
export const IMAGE_CHANGED_PROFILE = 'image_changed_profile';

//Change Password
export const CHANGE_PASSWORD_SUCCESS ='change_password_success'
export const CHANGE_PASSWORD_ERROR ="change_password_fail"
export const CLEAR_ERROR_REDUX = "clear_errors"


//Reports
export const REPORT_CREW_SUCCESS ="report_crew_success"
export const REPORT_PORT_SUCCESS ="report_port_success"
export const REPORT_TOTAL__SUCCESS ="report_total_success"


export const REPORT_CREW_ERROR ="report_crew_fail"
export const REPORT_PORT_ERROR ="report_port_fail"
export const REPORT_TOTAL_ERROR ="report_total_fail"


export const REPORT_LOAD_MORE = 'report_total_load_more';
export const REPORT_CLEAR_ERROR_REDUX = "report_total_errors"

//MessagesActionsTypes
export const USER_LIST_SUCCESS = "USER_LIST_SUCCESS"
export const USER_LIST_FAIL = "USER_LIST_FAIL"
export const MESSEGE_RETRIEVED_SUCCESS = "MESSEGE_RETRIEVED_SUCCESS"
export const MESSEGE_RETRIEVED_FAIL = "MESSEGE_RETRIEVED_FAIL";
export const MESSAGE_SEND_SECCESS = "MESSAGE_SEND_SECCESS";
export const MESSAGE_SEND_FAIL = "MESSAGE_SEND_FAIL";
export const MESSAGE_READ_SECCESS = "MESSAGE_READ_SECCESS";
export const MESSAGE_READ_FAIL = "MESSAGE_READ_FAIL";

