import {
  MY_USER_LIST_SUCCESS,
  MY_USER_LIST_FAIL,
  IS_LOADING,
  INVENTORY_LIST_SUCCESS,
  INVENTORY_LIST_FAIL,
  PLANT_LIST_SUCCESS,
  PLANT_LIST_FAIL,
  UNIT_LIST_SUCCESS,
  UNIT_LIST_FAIL,
  FLOOR_LIST_SUCCESS,
  FLOOR_LIST_FAIL,
  PORT_LIST_SUCCESS,
  PORT_LIST_FAIL,
  ADD_TASK_SUCCESS,
  ADD_TASK_FAIL,
  INVENTORY_CHANGE_NAME,
  INVENTORY_CHANGE_QUANTITY,
  INVENTORY_CHANGE_INCREMENT,
  ON_SELECT_TARGET,
  ON_SELECT_PLANT,
  ON_SELECT_UNIT,
  ON_SELECT_FLOOR,
  ON_SELECT_PORT,
  SELECTED_INVENTORY_ARRAY,
  ON_CHANGE_DATE,
  ON_CHANGE_TIME,
  ON_SAVE_PORT

} from "./types";
import HttpServiceManager from "../HttpServiceManager/HttpServiceManager";
import HttpConstant from "../HttpServiceManager/constant";

export const getUserApiRequest = (isLoading = false) => {

  return dispatch => {
    if (isLoading) {
      dispatch({
        type: IS_LOADING
      });
    }
    HttpServiceManager.getInstance()
      .request(HttpConstant.userList, '', "GET")
      .then(response => {
        dispatch({
          type: MY_USER_LIST_SUCCESS,
          payload: response.data
        });
      })
      .catch(error => {
        dispatch({
          type: MY_USER_LIST_FAIL,
          payload: error
        });
      });
  };
};


export const onChangeInventoryName = (text, id) => {
  return dispatch => {
    dispatch({
      type: INVENTORY_CHANGE_NAME,
      payload: text,
      id: id
    });

  }
}
export const onChangeInventoryQuantity = (text, id) => {
  return dispatch => {
    dispatch({
      type: INVENTORY_CHANGE_QUANTITY,
      payload: (text != '') ? parseInt(text) : text,
      id: id
    });
  }
}
export const increment = (text, id) => {
  return dispatch => {
    dispatch({
      type: INVENTORY_CHANGE_INCREMENT,
      payload: text,
      id: id
    });
  }
}
//on select crew
export const onSelectTarget = (id) => {
  return dispatch => {
    dispatch({
      type: ON_SELECT_TARGET,
      payload: id,

    });
  }
}

//on select Plant
export const onSelectPlant = (id) => {
  return dispatch => {
    dispatch({
      type: ON_SELECT_PLANT,
      payload: id,

    });
  }
}

//on select Unit
export const onSelectUnit = (id) => {
  return dispatch => {
    dispatch({
      type: ON_SELECT_UNIT,
      payload: id,

    });
  }
}

//on select Floor
export const onSelectFloor = (id) => {
  return dispatch => {
    dispatch({
      type: ON_SELECT_FLOOR,
      payload: id,
    });
  }
}

//on select Floor Disselect
export const onFloorDisselect = (id) => {
  return dispatch => {
    dispatch({
      type: "ON_FLOOR_DISSELECT",
      payload: id,
    });
  }
}


//on select Floor Disselect
export const _onFloorSelectionDone = (floorList) => {
  return dispatch => {
    dispatch({
      type: "ON_FLOOR_SELECTION_DONE",
      payload: floorList,
    });
  }
}

export const onFloorListComp = () => {
  return dispatch => {
    dispatch({
      type: "ON_FLOOR_LIST_COMP",
    });
  }
}

//on select port
export const onSelectPort = (id) => {
  return dispatch => {
    dispatch({
      type: ON_SELECT_PORT,
      payload: id,
    });
  }
}


//on change date
export const onChangeDate = (date) => {
  return dispatch => {
    dispatch({
      type: ON_CHANGE_DATE,
      payload: date,

    });
  }
}
//on change Time
export const onChangeTime = (time) => {
  return dispatch => {
    dispatch({
      type: ON_CHANGE_TIME,
      payload: time,

    });
  }
}


export const isModalVisible = () => {
  return dispatch => {
    dispatch({
      type: 'MODALVISIBLE',
    });
  }
}


export const setSelectedInventory = (inventoryArry, inventoryList) => {
  return dispatch => {
    dispatch({
      type: SELECTED_INVENTORY_ARRAY,
      payload: inventoryArry,
      inventoryList,
    })
  }
}

export const _portSelectionDone = (floorItem, port_data) => {
  return dispatch => {
    let obj = {
      floor_id: floorItem.id,
      data: port_data
    }
    dispatch({
      type: ON_SAVE_PORT,
      payload: obj
    });
  };
};


export const getInventoryApiRequest = (isLoading = false) => {

  return dispatch => {
    if (isLoading) {
      dispatch({
        type: IS_LOADING
      });
    }
    HttpServiceManager.getInstance()
      .request(HttpConstant.inventoryList, '', "GET")
      .then(response => {
        dispatch({
          type: INVENTORY_LIST_SUCCESS,
          payload: response.data
        });
      })
      .catch(error => {
        dispatch({
          type: INVENTORY_LIST_FAIL,
          payload: error
        });
      });
  };
};

export const getPlantApiRequest = (isLoading = false) => {
  return dispatch => {
    if (isLoading) {
      dispatch({
        type: IS_LOADING
      });
    }
    HttpServiceManager.getInstance()
      .request(HttpConstant.plantList, '', "GET")
      .then(response => {
        dispatch({
          type: PLANT_LIST_SUCCESS,
          payload: response.data
        });
      })
      .catch(error => {
        dispatch({
          type: PLANT_LIST_FAIL,
          payload: error
        });
      });
  };
};

export const getUnitApiRequest = (isLoading = false, id) => {
  return dispatch => {
    if (isLoading) {
      dispatch({
        type: IS_LOADING
      });
    }
    HttpServiceManager.getInstance()
      .request(HttpConstant.unitList + '?plant_id=' + id, '', "GET")
      .then(response => {
        dispatch({
          type: UNIT_LIST_SUCCESS,
          payload: response.data
        });
      })
      .catch(error => {
        dispatch({
          type: UNIT_LIST_FAIL,
          payload: error
        });
      });
  };
};

export const getFloorApiRequest = (isLoading = false, id) => {
  return dispatch => {
    if (isLoading) {
      dispatch({
        type: IS_LOADING
      });
    }

    HttpServiceManager.getInstance()
      .request(HttpConstant.floorList + '?unit_id=' + id, '', "GET")
      .then(response => {
        dispatch({
          type: FLOOR_LIST_SUCCESS,
          payload: response.data
        });
      })
      .catch(error => {
        dispatch({
          type: FLOOR_LIST_FAIL,
          payload: error
        });
      });
  };
};

export const getPortApiRequest = (isLoading = false, id) => {
  return dispatch => {
    if (isLoading) {
      dispatch({
        type: IS_LOADING
      });
    }
    HttpServiceManager.getInstance()
      .request(HttpConstant.portList + '?floor_id=' + id, '', "GET")
      .then(response => {
        dispatch({
          type: PORT_LIST_SUCCESS,
          payload: response.data
        });
      })
      .catch(error => {
        dispatch({
          type: PORT_LIST_FAIL,
          payload: error
        });
      });
  };
};


//AddTask Post Request
export const addTask = (isLoading = false, params) => {
  return dispatch => {
    if (isLoading) {
      dispatch({
        type: IS_LOADING
      });
    }
    HttpServiceManager.getInstance()
      .request(HttpConstant.taskAdd, params, "POST")
      .then(response => {
        dispatch({
          type: ADD_TASK_SUCCESS,
          payload: response.message
        })
      })
      .catch(error => {
        dispatch({
          type: ADD_TASK_FAIL,
          payload: error
        });
      });
  };
};

export const updateTaskReq = (isLoading = false, params) => {
  return dispatch => {
    if (isLoading) {
      dispatch({
        type: IS_LOADING
      });
    }
    HttpServiceManager.getInstance()
      .request(HttpConstant.taskUpdate + params.task_id, params, "POST")
      .then(response => {
        dispatch({
          type: ADD_TASK_SUCCESS,
          payload: response.message
        })
      })
      .catch(error => {
        dispatch({
          type: ADD_TASK_FAIL,
          payload: error
        });
      });
  };
};


export const onTaskEdit = (task) => {
  return dispatch => {
    dispatch({
      type: "TASK_EDIT",
      payload: task
    });
  }

}

export const onNewTaskAdd = () => {
  return dispatch => {
    dispatch({
      type: "NEW_TASK_ADD",
    });
  }

}