import {
  INVENTORY_CHANGE_USED_QUANTITY,
  ON_MARK_TASK_COMPLETE_SUCCESS,
  ON_MARK_TASK_COMPLETE_FAIL,
  SELECTED_PORT_INVENTORY_SAVED,
  ON_SAVED_INVENTORY,
  // ON_SAVED_SELECTED_PORT_INVENTORY,
  ON_SAVED_INVENTORY_PORT_REPORT
} from "./types";
import HttpServiceManager from "../HttpServiceManager/HttpServiceManager";
import HttpConstant from "../HttpServiceManager/constant";


export const onMarkTaskComplete = port_data => {
  return dispatch => {
    HttpServiceManager.getInstance()
      .request(HttpConstant.task_mark_as_complete, port_data, "POST")
      .then(response => {
        dispatch({
          type: ON_MARK_TASK_COMPLETE_SUCCESS,
          payload: response.message
        });
      })
      .catch(error => {
        dispatch({
          type: ON_MARK_TASK_COMPLETE_FAIL,
          payload: error
        });
      });
  };
};

export const onOfflineMarkTaskComplete = (port_data) => {
  return dispatch => {
    dispatch({
      type: 'OFFLINE_MARK_TASK_COMPLETE',
      payload: port_data
    })
  }
}


export const _onClearPandingTask = () => {
  return dispatch => {
    dispatch({
      type: 'CLEAR_PANDING_TASK',
    })
  }
}


export const onChangeInventoryUsedQuantity = (text, id, slctedPortId) => {
  return dispatch => {
    dispatch({
      type: INVENTORY_CHANGE_USED_QUANTITY,
      payload: text,
      id: id, //inventoryID
      portId: slctedPortId
    });
  };
};



export const onSavePortData = (task_detail, port, inventory, floor_id, location) => {
  return dispatch => {
    let selected_port_data = [];
    if ("back" in port) {
      port.back
        .filter(item => item.is_selected === true)
        .map(itm => {
          let obj = {
            port_id: itm.port_id,
            inventory: inventory,
            is_filled: 0
          };
          selected_port_data.push(obj);
        });
    }
    if ("left" in port) {
      port.left
        .filter(it => it.is_selected === true)
        .map(itm => {
          let obj = {
            port_id: itm.port_id,
            inventory: inventory,
            is_filled: 0
          };
          selected_port_data.push(obj);
        });
    }

    if ("right" in port) {
      port.right
        .filter(it => it.is_selected === true)
        .map(itm => {
          let obj = {
            port_id: itm.port_id,
            inventory: inventory,
            is_filled: 0
          };
          selected_port_data.push(obj);
        });
    }

    if ("front" in port) {
      port.front
        .filter(it => it.is_selected === true)
        .map(itm => {
          let obj = {
            port_id: itm.port_id,
            inventory: inventory,
            is_filled: 0
          };
          selected_port_data.push(obj);
        });
    }

    dispatch({
      type: SELECTED_PORT_INVENTORY_SAVED,
      payload: task_detail,
      task_port: port,
      selectedPortData: {
        data: selected_port_data,
        floor_id
      },
      floor_id,
      location
    });
  };
};


export const _onSavePortInventoryReport = (port_data, floor_id) => {
  return dispatch => {
    let port_data_obj = {
      floor_id,
      data: port_data
    }
    dispatch({
      type: ON_SAVED_INVENTORY_PORT_REPORT,
      payload: port_data_obj,
    })
  }
}

export const used_increment = (text, id) => {
  return dispatch => {
    dispatch({
      type: INVENTORY_CHANGE_USED_INCREMENT,
      payload: text,
      id: id
    });
  }
}


export const onInventoryUpdatdSaved = (data, id, floor_id) => {
  return dispatch => {
    dispatch({
      type: ON_SAVED_INVENTORY,
      payload: data,
      port_id: id,
      floor_id
    });
  };
};

export const onClearReduxState = () => {
  return dispatch => {
    dispatch({
      type: "CLEAR_REDUX_STATE",
    });
  };
};

