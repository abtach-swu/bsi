import {
    MY_TASK_SUCCESS,
    MY_TASK_FAIL,
    IS_LOADING,
    IS_REFRESHING,
    MY_TASK_LOAD_MORE
} from "./types";
import HttpServiceManager from "../HttpServiceManager/HttpServiceManager";
import HttpConstant from "../HttpServiceManager/constant";


export const myTaskApiRequest = (page, filter_type, status, actor_id, target_id, is_refreshing = false) => {


    // let param = {
    //     'page': page,
    //     "filter_type": filter_type,
    //     "status": status,
    //     "actor_id" : actor_id,
    //     "target_id" : target_id,
    // }


    return (dispatch) => {

        if (is_refreshing) {
            dispatch({
                type: IS_REFRESHING
            })
        }
        HttpServiceManager.getInstance().request(HttpConstant.mytask + '?page=' + page + '&filter_type=' + filter_type + '&status=' + status + '&actor_id=' + actor_id + '&target_id=' + target_id, '', "GET")
            .then((response) => {
                if (is_refreshing && target_id !== "") {
                    dispatch({
                        type: MY_TASK_SUCCESS,
                        payload: response.data,
                        status,
                        filter_type
                    });
                }
                else if (is_refreshing && actor_id !== "") {
                    dispatch({
                        type: MY_TASK_SUCCESS,
                        payload: response.data,
                    });
                }
                else {
                    dispatch({
                        type: MY_TASK_LOAD_MORE,
                        payload: response.data
                    });
                }
            })
            .catch((error) => {
                dispatch({
                    type: MY_TASK_FAIL,
                    payload: error
                })
            })
    }

}

export const _getTaskDetail = (id) => {

    return (dispatch) => {
        dispatch({ type: 'TASK_DETAIL' })
        HttpServiceManager.getInstance().request(HttpConstant.taskDetail + id, null, "GET")
            .then((response) => {
                dispatch({
                    type: 'GET_TASK_DETAIL_SUCCESS',
                    payload: response.data
                });
            })
            .catch((error) => {
                dispatch({
                    type: 'GET_TASK_DETAIL_FAIL',
                    payload: error
                })
            })
    }
}

