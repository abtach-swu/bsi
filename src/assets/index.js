export const images = {
    app_img :require("./images/app.png"),
    app_logo :require("./images/splash_logo.png"),
    drawer_icon :require("./images/menu.png"),
    done_icon :require("./images/done_enable.png"),
    done_disable :require("./images/done_disable.png"),

    dropdown:require("./images/dropdown.png"),
    icon_triangles :require("./images/icon_triangles.png"),
    location :require("./images/location.png"),
    status_task_right_image :require("./images/status_task_right_image.png"),
    status_task_gray_image:require("./images/status_task_gray_image.png"),
    arrow_next_icon :require("./images/arrow_next.png"),
    arrow_back :require("./images/arrow_back.png"),
    icon_eye : require("./images/View.png"),
    done2_icon: require("./images/done2.png"),
    done: require("./images/done.png"),
    edit_icon : require('./images/edit.png'),
    port_image : require('./images/Image.png'),
    cancel_icon : require('./images/cancel.png'),
    cancel_grey : require('./images/cancel_grey.png'),
    plus_icon : require('./images/plus.png'),
    minus_icon : require('./images/minus2.png'),
    minus_icon_red : require('./images/minus_red2.jpg'),
    job_number : require('./images/job_number.png'),
    notification : require('./images/notificatons.png'),
    search : require('./images/search.png'),
    
    // Drawer Icons
    nav_mytask:require("./images/my_task.png"),
    nav_blast_report:require("./images/blast_report.png"),
    nav_help_faqs:require("./images/help_faq.png"),
    message:require("./images/message.png"),
    nav_logout:require("./images/logout.png"),


    //Add Task
    add_task_fb:require("./images/add.png"),
    arrow_drop_down:require("./images/dropdown.png"),


    //Task Detail
    clock_gray:require("./images/clock_gray.png"),
    clock_red:require("./images/clock_red.png"),
    clock_purple:require("./images/clock_purple.png"),
}