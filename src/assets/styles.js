export default {
  container: {
    backgroundColor: "#FFF",

  },
  iconSize: {
    width: 20,
    height: 20
  },
  toggleSize: {
    width: 26,
    height: 26,
    marginLeft:25
  },
  arrorSise:{
    width: 25,
    height: 20,
    marginLeft:25
  },
  logoSize:{
    width: 150,
    height: 150,
  },
  iconRight:{
    width: 26,
    height: 26,
    marginRight:25
  },
  iconLeft: {
    width: 26,
    height: 26,
    marginLeft:25
  },

  viewMargins:{
    margin:16
  },
  message:{
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    margin: 10
}
};