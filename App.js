/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from "react";
import Setup from "./src/boot/Setup";
import { Platform, AppState } from "react-native";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import { persistStore, autoRehydrate, persistReducer } from "redux-persist";
import { PersistGate } from "redux-persist/integration/react";
import ReduxThunk from "redux-thunk";
import appReducer from "./src/reducers";
import HttpServiceManager from "./src/HttpServiceManager/HttpServiceManager";
import constant from "./src/HttpServiceManager/constant";
import logger from "redux-logger";
import autoMergeLevel2 from "redux-persist/lib/stateReconciler/autoMergeLevel2";
import storage from "redux-persist/lib/storage"; // defaults to localStorage for web and AsyncStorage for react-native
import firebase from "react-native-firebase";
import FirebaseNotification from "./src/utilities/FirebaseNotification";

import KeyboardManager from 'react-native-keyboard-manager';

const persistConfig = {
  key: "root",
  storage,
  // stateReconciler: hardSet,
  stateReconciler: autoMergeLevel2,
  // blacklist : ['addTaskReducer', 'InventoryReducer'],
  whitelist: ["mytaskReducer"]
};

const persistedReducer = persistReducer(persistConfig, appReducer);

export const store = createStore(
  persistedReducer,
  applyMiddleware(ReduxThunk, logger)
);
export const persistor = persistStore(store);

export default class App extends React.Component {
  componentDidMount() {
    if (Platform.OS === 'ios') {
      KeyboardManager.setToolbarPreviousNextButtonEnable(true);
    }
    HttpServiceManager.initialize(constant.baseUrl, {
      token: "api.Pd*!(5675"
    });
    // FirebaseNotification.notificationPermission();

    firebase
      .messaging()
      .getToken()
      .then(fcmToken => {
        if (fcmToken) {
          // user has a device token
          console.log("FCM Token  : ", fcmToken);
        } else {
          // user doesn't have a device token yet
        }
      });
  }

  componentWillUnmount() {
    AppState.removeEventListener("change", this._handleAppStateChange);
  }

  _handleAppStateChange = nextAppState => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === "active"
    ) {
      console.log("App has come to the foreground! ****** ");
      firebase
        .notifications()
        .getBadge()
        .then(count => {
          console.log("background getBadge() ********** ", count);
          this.props.navigation.setParams({ count: count });
        });
      //   this.notificationListener();
    }
    this.setState({ appState: nextAppState });
  };

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Setup />
        </PersistGate>
      </Provider>
    );
  }
}
